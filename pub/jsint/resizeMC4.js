function DoResize(event) {
    ResizeMC(event.pageX);
}

function StopResize(event) {
    $('body').off("mousemove");

    //RegisterPosition();
}

function StartResize(event) {
    $('body').mousemove(DoResize);
    $('body').mouseup(StopResize);
}

function ResizeMCInit() {
    $('#pgresizer').mousedown(StartResize);
}

function ResizeMC(x) {
    $('.filelist').css('max-width',x);   

    if ( x < 25 ) {
	$('.filelist').css('display','none');
    } else {
	$('.filelist').css('display','block');
    }   
}

function RegisterPosition() {
    var lft = $('#pgresizer').css('left');
    var jid = $('#did').val();
    Cookies.set("CSXLEFT_"+jid, lft.replace("px",""));
}


function RecallPosition() {
    var jid = $('#did').val();
    var lpos = Cookies.get("CSXLEFT_"+jid);

    if ( lpos === undefined ) {
	return;
    }
    var xpos = parseInt(lpos);
    ResizeMC(xpos);
}

