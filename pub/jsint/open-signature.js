/**
 * JS code that could be useful on all page
 */

/**
 * To fix left and right key navigation inside input and textarea.
 * Because it is trapped by dsfr tab component on .fr-tabs container
 * @see https://gouvfr.atlassian.net/servicedesk/customer/portal/1/DSFR-741
 * @see https://github.com/GouvernementFR/dsfr/issues/392 (should be fixed in dsfr-1.9.0 upcoming release)
 *
 */
const fixDsfrInputNavigationInTabComponents = () => {

	const inputElements = document.querySelectorAll('.fr-tabs input[type], .fr-tabs textarea');

	const keyCodesToFix = [
		window.dsfr.core.KeyCodes.LEFT,
		window.dsfr.core.KeyCodes.RIGHT,
		window.dsfr.core.KeyCodes.HOME,
		window.dsfr.core.KeyCodes.END
	];

	inputElements.forEach((inputElement) => {
		inputElement.addEventListener('keydown', (e) => {
			if (keyCodesToFix.includes(e.keyCode)) {
				e.stopPropagation();
			}
		});
	});
};


/**
 * add a "show password" toggle button after input[type=password] having the .gnPasswordVisibilityToggle class
 */
 const handleVisibilityToggleOnPasswordFields = () => {

	// find the password fields we want to add the functionality to
	const passwordFields = document.querySelectorAll('input[type=password].OSpasswordVisibilityToggle');

	const openedEyeClass = 'fr-icon-eye-fill';
	const closedEyeClass = 'fr-icon-eye-off-fill';

	//and leave early if nothing to do
	if (!passwordFields.length) { return; }


	//first create the event handler for toogle button click so we can bind it later (if created after, it would be undefined)
	const togglePassword = (event) => {
		event.preventDefault();
		event.stopPropagation();

		const passwordField = document.getElementById(event.target.dataset.gnTogglePassword);
		const showPassword = event.target.getAttribute('aria-pressed') !== 'true';

		passwordField.setAttribute('type', showPassword ? 'text' : 'password');
		event.target.setAttribute('aria-pressed', showPassword ? 'true' : 'false');
		event.target.title = (showPassword ? 'Cacher' : 'Montrer') + ' le mot de passe';
		event.target.firstChild.classList.remove(openedEyeClass, closedEyeClass)
		const classToAdd = showPassword ? closedEyeClass : openedEyeClass;
		event.target.firstChild.classList.add(classToAdd);
	}

	passwordFields.forEach(passwordField => {

		//creation d'un wrapper pour l'input passord et le boutton de toggle
		const passwordWrapper = document.createElement('div');
		passwordWrapper.classList.add('fr-input-wrap', 'fr-input-wrap--addon');
		passwordField.parentNode.insertBefore(passwordWrapper, passwordField);

		// move password field into wrapper
		passwordWrapper.appendChild(passwordField);

		// create visibility toggle button
		const buttonToggler = document.createElement('button');
		buttonToggler.classList.add('fr-btn', 'fr-btn--tertiary');
		buttonToggler.setAttribute('type', 'button');
		buttonToggler.setAttribute('role', 'switch');
		buttonToggler.setAttribute('aria-pressed', 'false');
		buttonToggler.dataset.gnTogglePassword = passwordField.id;
		buttonToggler.title = 'Montrer le mot de passe';
		buttonToggler.id = 'show-passord-for-field-' + passwordField.id
		buttonToggler.addEventListener('click', togglePassword);
		// if form in modal, form is reset on modal open, we need to simulate to reset toogleButton
		passwordField.closest('form').addEventListener('reset', function (e) {
			buttonToggler.setAttribute('aria-pressed', 'true');
			buttonToggler.click();
		})

		// adding fontawesome icon inside toggle button
		const buttonTogglerContent = document.createElement('i');
		buttonTogglerContent.classList.add(openedEyeClass);
		buttonTogglerContent.style = 'pointer-events: none;'; // important so that click on the icon still fires button click
		buttonToggler.appendChild(buttonTogglerContent);

		//put the toggle button after passwordField
		passwordField.after(buttonToggler)
	});
};

document.addEventListener("DOMContentLoaded", function() {
  fixDsfrInputNavigationInTabComponents();
});

