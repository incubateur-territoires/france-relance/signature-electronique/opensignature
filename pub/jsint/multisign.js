// multisign dedicated functions

function MultiSelectionToggleAll(mode) {
    $('#sidemenufile .check-'+mode).each(function() {
	$(this).prop('checked',true);
    })
    if ( mode == "sign" ) {
	altm = "refuse"
    } else {
	altm = "sign"
    }
    $('#sidemenufile .check-'+altm).each(function() {
	$(this).prop('checked',false);
    })
}

function OneItemToggle(item) {

    if(item.closest('form').hasAttribute('lock-selection')) {
        return false;
    }
    var theid = item.id;
    var sts = $('#'+theid).prop('checked');
    var rac = theid.substr(0, 1);
    var id2 = theid.substr(2);
    if ( sts ) {
	if ( rac == "R" ) {
	    var other = "S-"+id2;
	}
	if  ( rac == "S" ) {
	    var other = "R-"+id2;
	}
	$('#'+other).prop('checked', false)
    }
}

function PagedSignSelection() {
    $('#menuFileForm').submit();
}

function MultiSelectionMenuLockToggle() {
    const sidemenuContainer = document.getElementById('sidemenufile');
    sidemenuContainer.querySelectorAll('button:not(#unlockSelection)').forEach(el => el.toggleAttribute('disabled'));
    sidemenuContainer.querySelectorAll('a[aria-current').forEach(el => el.removeAttribute('aria-current'));
    sidemenuContainer.classList.toggle('selectionLocked');

    const menuFileForm = document.getElementById('menuFileForm');
    menuFileForm.toggleAttribute('lock-selection');
    menuFileForm.addEventListener('change', function(e){
        if (!this.hasAttribute('lock-selection')) {
            return ;
        }
        if(e.target.type === 'checkbox') {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    });
    menuFileForm.addEventListener('click', function(e){
         if (!this.hasAttribute('lock-selection')) {
            return;
        }
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    if(!menuFileForm.hasAttribute('lock-selection')) {
        menuFileForm.querySelector('a.fr-sidemenu__link:first-of-type').click();
    }
}

function MultiSelectionAction(action) {

    $('#typeSignAction').val(action)
    
    var elem;
   
    if ( $('#menuFileForm').is(":hidden") ) {
	elem = "listFileForm";
    } else {
	elem = "menuFileForm";
    }

    $('#'+elem).submit(function( event ) {

        // Stop form from submitting normally
        event.preventDefault();
            
        var url = $(this).attr( "action" );
        var formData = new FormData($(this)[0]);
        //formData.append("PLOUC", "plic");
            
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (returndata) {
                
                $('#page-content').html(returndata);
                fixDsfrInputNavigationInTabComponents();
            },
            error: function (returndata) {
                $('#page-content').html("<h1> POST-ERROR </h1>");
            }
        });
            
	
    });
   
    $('#'+elem).submit();    
   
}

function MultiSignStep2() {

    $('#sign_div_step1').hide();
    $('#sign_div_step2').show();
}

function MultiSignAccept() {
    if(!document.querySelector('#multisign_sign_form_step2').reportValidity()){
        return false;
    }

    var stel = $('#signtel').val();
    var snam = $('#signnam').val();
    var smel = $('#signmel').val();
    
    if ( stel == "" || snam == "" || smel == "" ) {
        $('#sign_div_err1').show();
        return;
    } else {
        $('#sign_div_err1').hide();
    }

    var url="/mgt/MultiSignSMS/"+stel+"/"+snam;

    $.get(url).done(function(data) {

        if (data.indexOf("ERROR") != -1 ) {
            $('#sign_div_oki').hide();
            $('#sign_div_ref').hide();
            $('#sign_div_err').show();
        }
        if (data.indexOf("OK") != -1 ) {
            $('#sign_div_oki').show();
            $('#sign_div_step2').hide();
            $('#sign_div_ref').hide();
            $('#sign_div_err').hide();
        }
        
    }).fail(function(data) {
        $('#sign_div_oki').hide();
        $('#sign_div_ref').hide();
        $('#sign_div_err').show();
    })
    

}

function MultiSignRefuse() {
    $('#sign_div_ref').show();
    $('#sign_div_oki').hide();
    $('#sign_div_err').hide();
}

function MultiSignAcceptFinal(action) {

    if(!document.querySelector('#multisign_sign_form_step3').reportValidity()){
        return false;
    }

    $('#multisign_action').val(action);
    var code = $('#SIGNCODE').val();
    $('#multisign_code').val(code);

    // fetch multiple varialble  ( because responsiveness )
    var stel = $('#signtel').val();
    var snam = $('#signnam').val();
    var smel = $('#signmel').val();
    if ($('#signimg').is(":checked")) {
	var simg = "img";
    } else {
	var simg = "def";
    }

    $('#multisign_name').val(snam);
    $('#multisign_phone').val(stel);
    $('#multisign_mail').val(smel);
    $('#multisign_sigimg').val(simg);

    var messg = $('#signcom').val();     // manage message also in case of both sign and refuse
    $('#multisign_messg').val(messg);

    MultiSignActionFinal()
}

function MultiSignRefuseFinal(action) {

    if(!document.querySelector('#multisign_form').reportValidity()){
        return false;
    }
    $('#multisign_action').val(action);
    var messg = $('#signcom').val();
    $('#multisign_messg').val(messg);
    var name = $('#signnam').val();
    $('#multisign_name').val(name);
    var mail = $('#signmel').val();
    $('#multisign_mail').val(mail);
    
    //$('#multisign_form').submit();
    MultiSignActionFinal()
}

function MultiSignActionFinal() {

    $('#multisign_form').one('submit',function( event ) {

	// Stop form from submitting normally
	event.preventDefault();
	    
	var url = $(this).attr( "action" );
	var formData = new FormData($(this)[0]);

    let signcodeField = document.querySelector('input[name=SIGNCODE]');
    if (signcodeField) {
        signcodeField.classList.remove('fr-input--error');
        let signcodeFieldInputGroup = signcodeField.closest('.fr-input-group');
        signcodeFieldInputGroup.classList.remove('fr-input-group--error');
        document.getElementById('text-input-error-SIGNCODE')?.remove();
    }
	    
	$.ajax({
	    url: url,
	    type: 'POST',
	    data: formData,
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function (data, textStatus, jqXHR) {
            if(jqXHR.responseJSON !== undefined && signcodeField) {
                signcodeField.classList.add('fr-input--error');
                let signcodeFieldInputGroup = signcodeField.closest('.fr-input-group');
                signcodeFieldInputGroup.classList.add('fr-input-group--error');
                let pError = document.createElement("p");
                pError.id = 'text-input-error-SIGNCODE'
                pError.classList.add('fr-error-text');
                pError.textContent = data.message;
                signcodeField.after(pError);
            } else {
                $('#page-content').html(data);
                $('#unlockSelection').remove();
            }
	    },
	    error: function (returndata) {
		$('#page-content').html("<h1> POST-ERROR </h1>");
	    }
	});
	
    });
   
    $('#multisign_form').submit();
}
