function ProofFilter() {
    var filt_ref = $("#searchRef").val().toLowerCase();
    var filt_dat = $("#searchDat").val().toLowerCase();
    var filt_uzr = $("#searchUzr").val().toLowerCase();

    if (typeof filt_ref == 'undefined') {
	filt_ref = ""
    }
    if (typeof filt_dat == 'undefined') {
	filt_dat = ""
    }
    if (typeof filt_uzr == 'undefined') {
	filt_uzr = ""
    }

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    var xurl = "FilterProofSave?PR="+filt_ref+"&PD="+filt_dat+"&PU="+filt_uzr;
    xmlhttp.open("GET",xurl,true);
    xmlhttp.send();

    var cnt1sel = 0;
    var cnt1tot = 0;

    $('#prooftable').find('.datarow').each(function(){
	var thetr = $(this);
 
	var theref = thetr.find('.pref').text().toLowerCase();
	var thedat = thetr.find('.pdat').text().toLowerCase();
	var theuzr = thetr.find('.puzr').text().toLowerCase();

	var score=0
	if ( filt_ref == "" || theref.indexOf(filt_ref) != -1 ) {
	    score = score + 1;
	}
	if ( filt_dat == "" || thedat.indexOf(filt_dat) != -1 ) {
	    score = score + 1;
	}
	if ( filt_uzr == "" || theuzr.indexOf(filt_uzr) != -1 ) {
	    score = score + 1;
	}

	cnt1tot += 1;
	if ( score == 3 ) {
	    thetr.show();
	    cnt1sel += 1;
	} else {
	    thetr.hide();
	}

    })

    if (  cnt1sel == cnt1tot ) {
	$("#ProofCnt").text(cnt1sel);
    } else {
	$("#ProofCnt").text(cnt1sel+"/"+cnt1tot);
    }

}

