function changeContent(url) {
  //alert('in change content');

  $.get(url).done(function(data) {
    $('#page-content').html(data);
  }).fail(function(data) {
    $('#page-content').html("<h1> ERROR </h1>");
  })

    /* Permet de selectionner le fichier "en cours de visualisation" dans le side menu */
    $('.documents-list .fr-sidemenu__item--active').removeClass('fr-sidemenu__item--active');
    $(".documents-list a[aria-current='page']").removeAttr('aria-current');
    var $viewedFile =  $(".documents-list a[href*='"+url+"'");
    if($viewedFile.length) {
        $viewedFile.attr('aria-current', 'page').parents('.fr-sidemenu__item').addClass('fr-sidemenu__item--active');
    }
}

function adjustContentLeft() {
   // fetch pgcont margin left 
   // to eventually inject in conthead left css
   var pl = $('#page-content').css('margin-left');
   var i1 = pl.indexOf("px");
   var pn = parseInt(pl.substr(0,i1))
   $('#conthead').css('left',pn+5);   
   $('#prvbutn').css('left',pn+25);   
}


function contentRename() {
    $('#menuFileAction').val("REN");
    $('#listFileAction').val("REN");
    contentAction();
}

function contentDelete() {
    $('#menuFileAction').val("DEL");
    $('#listFileAction').val("DEL");
    contentAction();
}

function contentGetZip() {
    $('#menuFileAction').val("ZIP");
    $('#listFileAction').val("ZIP");
    contentAction();
}

var MenuFileCountSelected = 0

function menuItemClicked(theobj) {
    if ( $(theobj).is(":checked") ) {
	MenuFileCountSelected ++
    } else {
	MenuFileCountSelected --
    }
    if ( MenuFileCountSelected > 0 ) {
	$('#menuFileRenameButton').show();
	$('#menuFileDeleteButton').show();
    } else {
	$('#menuFileRenameButton').hide();
	$('#menuFileDeleteButton').hide();
    }
}

function menuItemToggleAll() {
  //var gsts = $('#togall').prop('checked');
   $('.fr-sidemenu__list input').each(function() {
     var sts = $(this).prop('checked');
     if ( sts ) {
       $(this).prop('checked',false);
       MenuFileCountSelected --
     } else {
       $(this).prop('checked',true);
       MenuFileCountSelected ++
     }
   })
   if ( MenuFileCountSelected > 0 ) {
	$('#menuFileRenameButton').show();
	$('#menuFileDeleteButton').show();
   } else {
	$('#menuFileRenameButton').hide();
	$('#menuFileDeleteButton').hide();
   }
}

function contentAction() {

    var elem;
   
    if ( $('#menuFileForm').is(":hidden") ) {
	elem = "listFileForm";
    } else {
	elem = "menuFileForm";
    }

    $('#'+elem).submit(function( event ) {

	// Stop form from submitting normally
	event.preventDefault();
	    
	var url = $(this).attr( "action" );
	var formData = new FormData($(this)[0]);
	//formData.append("PLOUC", "plic");
	    
	$.ajax({
	    url: url,
	    type: 'POST',
	    data: formData,
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function (returndata) {
		$('#page-content').html(returndata);
	    },
	    error: function (returndata) {
		$('#page-content').html("<h1> POST-ERROR </h1>");
	    }
	});
	
    });
   
    $('#'+elem).submit();    
   
}

function contentMailSend() {
    //alert("Sending Mail ");

    $('#melShareForm').submit(function( event ) {
 
	// Stop form from submitting normally
	event.preventDefault();
 
	var url = $(this).attr( "action" );
	var formData = new FormData($(this)[0]);

	$.ajax({
	    url: url,
	    type: 'POST',
	    data: formData,
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function (returndata) {
		$('#pgcont').html(returndata);
	    },
	    error: function (returndata) {
		$('#pgcont').html("<h1> MAIL-SEND-ERROR </h1>");
	    }
	});
	
    });

    $('#melShareForm').submit();
    $('#melShareForm').hide();
    $('#melShareSuccess').show();
}

function imageFitCalculation(iw, ih) {
    var ww = $(window).width();
    var wh = $(window).height();
    var pl = $('#conthead').css('left');
    var i1 = pl.indexOf("px");
    var cn = parseInt(pl.substr(0,i1))
    var w2w = ww - cn - 5;
    var w2h = wh - 125;
    var ri = iw / ih;
    var rw = w2w / w2h;
    
    if ( ri > rw ) {
	$('#imgfit').width(w2w);
    } else {
	$('#imgfit').height(w2h);
	var i2w = Math.floor(w2h / ih * iw);
	var m2l = (w2w - i2w) / 2;
	var m3l = Math.floor(m2l);
	$('#imgfit').css("margin-left", m3l+"px");
	
    }
}

function changeMenuFile(url) {

  $.get(url).done(function(data) {
    $('#sidemenufile').html(data);
  }).fail(function(data) {
    $('#sidemenufile').html("<h1> ERROR </h1>");
  })
}

function update4FileChange(url1, url2) {

    changeMenuFile(url1);
    changeContent(url2);

}

