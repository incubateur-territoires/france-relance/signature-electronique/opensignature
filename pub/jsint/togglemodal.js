function DeleteModal(did,dnam) {
    $('#md_tit_dnam').text(dnam);
    $('#md_imp_did').val(did);
}

// used by bootstap not yet dsfr adm/DosList !!
function DeleteModalBS(did,dnam) {
    $('#md_tit_dnam').text(dnam);
    $('#md_imp_did').val(did);
    $('#ModalConfDel').modal();
}

function DetachModal(did,dnam) {
    $('#mdt_tit_dnam').text(dnam);
    $('#mdt_imp_did').val(did);
}

function ModUserModal(uid,umel,unam,ugvn,usts,utyp,uend) {
    $('#mum_imp_uid').val(uid);
    $('#mum_imp_mel').val(umel);
    $('#mum_imp_nam').val(unam);
    $('#mum_imp_gvn').val(ugvn);
    if ( utyp == "ldap" ) {
	$('#mum_imp_typ1').prop("checked", true);
	$('#mum_div_pwd').hide();
    } else {
	$('#mum_imp_typ2').prop("checked", true);
	$('#mum_div_pwd').show();
    }
    $('#mum_sel_sts').val(usts);
    $('#mum_imp_end').val(uend);
    $('#ModalUserModify').modal();
}

function DelUserModal(uid,unam) {
    $('#mud_tit_unam').text(unam);
    $('#mud_imp_uid').val(uid);
    $('#ModalUserDelete').modal();
}


function MgtCreateModal(url) {

    $.get(url).done(function(data) {
	$('#mgtcremodal').html(data);
    }).fail(function(data) {
	$('#mgtcremodal').html("<h1> ERROR </h1>");
    })
}

function MgtCreateDismiss(url) {

    $.get(url).done(function(data) {
	$('#mgtuncreate').html(data);
    }).fail(function(data) {
	$('#mgtuncreate').html("<h1> ERROR </h1>");
    })
    $('#ModalCreate').modal('hide');
}

function PayModal() {
    $('#ModalAccount').modal('hide');
    $('#ModalPay').modal();
}

function SigReqModal() {
    $('#ModalSigReq').modal();
}

function AttachModal(did,dnam) {
    //$('#ModalAttach').modal();
}

function ViewSignersList(did,dnam) {
    const modal = document.querySelector('#signers-list-modal');
    modal.querySelector('.folder-title').textContent = dnam;


    let fd = new FormData();
    fd.append('DID', did);

    const signersContainer = modal.querySelector('#signersListPlaceholder');

    // clean existing signersList
    signersContainer.innerHTML = '';

    $.ajax({
	    url: '/doc/GetSignersList/' + did,
	    type: 'POST',
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
        dataType: 'json',
	    success: function (signers) {
        
            const signersFormatterList = signers.map(signer => {
                let str = [];
                if ( ['delayed', 'done'].includes(signer?.status)) {
                    str.push(`<span class="fr-badge os-signature-status fr-badge--sm fr-badge--success" aria-hidden="true" title="Signature effectuée"></span>`);
                }
                if (signer?.status === 'refused'){
                    str.push(`<span class="fr-badge os-signature-status fr-badge--sm fr-badge--error" aria-hidden="true" title="Signature refusée"></span>`);
                }
                if(!str.length) {
                    str.push(
                        `<span class="fr-badge os-signature-status fr-badge--sm fr-badge--info fr-badge--no-icon" aria-hidden="true" title="Signature en attente">
                            <span class="fr-icon--xs fr-icon-question-mark" aria-hidden="true"></span>
                        </span>`
                    );
                }
                str.push(`&nbsp;<span class="" style="white-space: nowrap;">${signer?.mail}</span>`);
                return str.join('');
            });
            signersContainer.innerHTML = signersFormatterList.length>0?signersFormatterList.join('<br>') : 'Aucun signataire.';
        },
	    error: function (returndata) {
            signersContainer.innerHTML = '<p>Une erreur est survenue lors de la récupération des signataires du dossier.</p>';
	    }
	});
}
