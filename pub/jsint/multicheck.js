function ToggleCheck(id) {
    $('#'+id).find('.datarow').each(function(){
        var thetr = $(this);
        if ( thetr.is(":visible") ) {
            var thechk = thetr.find('.selchk');
            var schk = thechk.prop('checked', function (i, value) { return !value;} );
        }
    })
    $('#masterCheck').prop('checked', false);
}

function CheckOrUncheckAll(id) {
    
    const mastercheckStatus = $('#'+id).find('#masterCheck').prop('checked');

    $('#'+id).find('.datarow').each(function(){
        var thetr = $(this);
        if ( thetr.is(":visible") ) {
            var thechk = thetr.find('.selchk');
            var schk = thechk.prop('checked', mastercheckStatus);
        }
    })
    //$('#masterCheck').prop('checked', !mastercheckStatus);
}


function GetCheckedList(id) {
    var clist = "";
    $('#'+id).find('.datarow').each(function(){
        var thetr = $(this);
        var thechk = thetr.find('.selchk');
        if ( thechk.is(':checked') ) {
            clist += thetr.find('.did').text() + " ";
        }
    })
    return(clist);      
}

function SigReqMultiple() {
    var cl = GetCheckedList('dostable');
    if ( cl.length === 0 ) {
        const emptySelectionModal = document.getElementById('empty-selection-modal');
        window.dsfr(emptySelectionModal).modal.disclose();
    } else {
        $('#did_imp_multi').val(cl);
        const signatureRequestModal = document.getElementById('signature-request-modal');
        window.dsfr(signatureRequestModal).modal.disclose();
	}
}

function MultiDetachModal() {
    var cl = GetCheckedList('dostable');
    if ( cl.length === 0 ) {
        const emptySelectionModal = document.getElementById('empty-selection-modal');
        window.dsfr(emptySelectionModal).modal.disclose();
        //$('#ModalEmptySel').modal();
    } else {
	    $('#did_imp_multi_detach').val(cl);
        const multiDetachModal = document.getElementById('multi-detach-modal');
        window.dsfr(multiDetachModal).modal.disclose();
	    //$('#ModalConfMultiDetach').modal();
    }
}

function MultiDeleteModal() {
    var cl = GetCheckedList('dostable');
    if ( cl.length === 0 ) {
        const emptySelectionModal = document.getElementById('empty-selection-modal');
        window.dsfr(emptySelectionModal).modal.disclose();
        //$('#ModalEmptySel').modal();
    } else {
	    $('#did_imp_multi_delete').val(cl);
        const multiDeleteModal = document.getElementById('multi-delete-modal');
        window.dsfr(multiDeleteModal).modal.disclose();
	    //$('#ModalConfMultiDel').modal();
    }
}

function MultiDownloadModal() {
    var cl = GetCheckedList('dostable');
    if ( cl.length === 0 ) {
        const emptySelectionModal = document.getElementById('empty-selection-modal');
        window.dsfr(emptySelectionModal).modal.disclose();
        //$('#ModalEmptySel').modal();
    } else {
	    $('#did_imp_multi_download').val(cl);
        const multiDownloadModal = document.getElementById('multi-download-modal');
        window.dsfr(multiDownloadModal).modal.disclose();
	const handleWindowBlurOnDownloadDialog = (event) => {
            window.removeEventListener('blur', handleWindowBlurOnDownloadDialog);
            window.dsfr(multiDownloadModal).modal.conceal();
        }
        window.addEventListener('blur', handleWindowBlurOnDownloadDialog);	
	    //$('#ModalConfMultiDel').modal();
    }
}

