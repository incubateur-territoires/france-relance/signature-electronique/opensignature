function GlobalFilter() {

		//test if field exists, otherwise .toLowerCase applies on null and break search	
		if ($("#searchDoc").length) {
			var filt_doc = $("#searchDoc").val().toLowerCase();
		}
		if ($("#searchMod").length) {
			var filt_mod = $("#searchMod").val().toLowerCase();
		}
		
		if ($("#searchEOL").length) {
			var filt_eol = $("#searchEOL").val().toLowerCase();
		}

		if ($("#searchSTS").length) {
			var filt_sts = $("#searchSTS").val().toLowerCase();
		}
	

    if (typeof filt_doc == 'undefined') {
	filt_doc = ""
    }
    if (typeof filt_mod == 'undefined') {
	filt_mod = ""
    }
    if (typeof filt_eol == 'undefined') {
	filt_eol = ""
    }
    if (typeof filt_sts == 'undefined') {
	filt_sts = "---"
    }

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    var xurl = "FilterSave?FD="+filt_doc+"&FM="+filt_mod+"&FE="+filt_eol+"&FS="+filt_sts;
    xmlhttp.open("GET",xurl,true);
    xmlhttp.send();

    var cnt1sel = 0;
    var cnt1tot = 0;

    $('#dostable').find('.datarow').each(function(){
	var thetr = $(this);
/* 
	var thedoc = thetr.find('.dnam').text().toLowerCase();
	var themod = thetr.find('.dmod').text().toLowerCase();
	var theeol = thetr.find('.deol').text().toLowerCase();
	var thests = thetr.find('.dsts').text().toLowerCase();
*/
	/* fix \n and spaces in strings, that broke search */
	var thedoc = thetr.find('.dnam').text().toLowerCase().trim();
	var themod = thetr.find('.dmod').text().toLowerCase().trim();
	var theeol = thetr.find('.deol').text().toLowerCase().trim();
	var thests = thetr.find('.dsts').text().toLowerCase().trim();

	var score=0
	if ( filt_doc == "" || thedoc.indexOf(filt_doc) != -1 ) {
	    score = score + 1;
	}
	if ( filt_mod == "" || themod.indexOf(filt_mod) != -1 ) {
	    score = score + 1;
	}
	if ( filt_eol == "" || theeol.indexOf(filt_eol) != -1 ) {
	    score = score + 1;
	}
	if ( filt_sts == "---" || thests == filt_sts ) {
	    score = score + 1;
	}
	// specific case visaall  signall
	if ( filt_sts == "visaall" && thests.substring(0,4) == "visa" ) {
	    score = score + 1;
	}
	if ( filt_sts == "signall" && thests.substring(0,4) == "zign" ) {
	    score = score + 1;
	}

	cnt1tot += 1;
	if ( score == 4 ) {
	    thetr.show();
	    cnt1sel += 1;
	} else {
	    thetr.hide();
	}

    })

    if (  cnt1sel == cnt1tot ) {
	$("#DosCnt").text(cnt1sel);
    } else {
	$("#DosCnt").text(cnt1sel+"/"+cnt1tot);
    }

}

function GlobalUzFilter() {
    var filt_mel = $("#searchMel").val().toLowerCase();
    var filt_nam = $("#searchNam").val().toLowerCase();
    var filt_cre = $("#searchCre").val().toLowerCase();
    var filt_pay = $("#searchPay").val().toLowerCase();

    if (typeof filt_mel == 'undefined') {
	filt_mel = ""
    }
    if (typeof filt_nam == 'undefined') {
	filt_nam = ""
    }
    if (typeof filt_cre == 'undefined') {
	filt_cre = ""
    }
    if (typeof filt_pay == 'undefined') {
	filt_pay = ""
    }

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    var xurl = "FilturSave?F1="+filt_mel+"&F2="+filt_nam+"&F3="+filt_cre+"&F4="+filt_pay;
    xmlhttp.open("GET",xurl,true);
    xmlhttp.send();

    var cnt2sel = 0;
    var cnt2tot = 0;

    $('#uzrtable').find('.datarow').each(function(){
	var thetr = $(this);
 
	var themel = thetr.find('.umel').text().toLowerCase();
	var thenam = thetr.find('.unam').text().toLowerCase();
	var thecre = thetr.find('.ucre').text().toLowerCase();
	var thepay = thetr.find('.upay').text().toLowerCase();

	var score=0
	if ( filt_mel == "" || themel.indexOf(filt_mel) != -1 ) {
	    score = score + 1;
	}
	if ( filt_nam == "" || thenam.indexOf(filt_nam) != -1 ) {
	    score = score + 1;
	}
	if ( filt_cre == "" || thecre.indexOf(filt_cre) != -1 ) {
	    score = score + 1;
	}
	if ( filt_pay == "" || thepay.indexOf(filt_pay) != -1 ) {
	    score = score + 1;
	}

	cnt2tot += 1;
	if ( score == 4 ) {
	    thetr.show();
	    cnt2sel += 1;
	} else {
	    thetr.hide();
	}

    })

    if (  cnt2sel == cnt2tot ) {
	$("#UzrCnt").text(cnt2sel);
    } else {
	$("#UzrCnt").text(cnt2sel+"/"+cnt2tot);
    }

}

