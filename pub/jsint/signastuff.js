function SignStep2(did) {
    var jralcnt = $('#jralcnt').val();
    var jralsel = 0;

    for (let i = 0; i < jralcnt; i++) {
		if ( $('#jral0_'+i).is(':checked') ) {
			jralsel += 1;
		}
    }

    if ( jralsel == 0 ) {
		$('#sign_div_err2').show();
		return;
    } else {
		$('#sign_div_err2').hide();
		$('#sign_div_stp1').hide();
		$('#sign_div_stp2').show();
    }    
}

function SignStep2Back(did) {
	$('#sign_div_stp1').show();
	$('#sign_div_stp2').hide();
}

function SignAccept(did) {

    if(!document.querySelector('#sign_div_stp2 form').reportValidity()) {
	return false;
    }

    $('#sign_div_stp2').hide();

    var stel = $('#signtel').val();
    var snam = $('#signnam').val();
    var smel = $('#signmel').val();
   
    if ( stel == "" || snam == "" || smel == "" ) {
	$('#sign_div_stp2').show();
	$('#sign_div_err1').show();
	
	return;
    } else {
	$('#sign_div_err1').hide();
    }

    // report variable to next step form
    $('#signtel2').val(stel);
    $('#signnam2').val(snam);
    $('#signmel2').val(smel);
    
    var url="/doc/SignSMS/"+did+"/"+stel+"/"+smel+"/"+snam;

    $.get(url).done(function(data) {
		if (data.indexOf("ERROR") != -1 ) {
			$('#sign_div_oki').hide();
		        $('#sign_div_ref').hide();
		        $('#sign_div_err_msg').text(data.substring(6));
			$('#sign_div_err').show();
		}
		if (data.indexOf("OK") != -1 ) {
			$('#sign_div_oki').show();
			$('#sign_div_ref').hide();
			$('#sign_div_err').hide();
		}
	
    }).fail(function(data) {
		$('#sign_div_oki').hide();
		$('#sign_div_ref').hide();
		$('#sign_div_err').show();
    }) 
}

function SignRefuse(did) {
    $('#sign_div_ref').show();
    $('#sign_div_oki').hide();
    $('#sign_div_err').hide();
}

function SignAccept1Step(did) {
    var type = $('#sigreqtyp').val();
    var mel = $('#SIGNMEL').val();

    if ( type == "manual" ) {
		var tel = $('#SIGNTEL').val();
		if ( tel == "" ) {
			$('#sign_div_oki').hide();
			$('#sign_div_ref').hide();
			$('#sign_div_err').hide();
			$('#sign_div_notel').show();
			return;
		}
		var concatargs = "manual|"+mel+"|"+tel;
    }

    if ( type == "referenced" ) {
		var concatargs = "referenced|"+mel+"|NA";
    }

    var url1 = "/doc/Sign1Step2/"+did+"/"+concatargs;

    $.get(url1).done(function(data) {
	if (data.indexOf("ERROR") != -1 ) {
	    $('#sign_div_oki').hide();
	    $('#sign_div_ref').hide();
 	    $('#sign_div_err').show();
	    $('#sign_div_notel').hide();
	}
	
	if (data.indexOf("OK") != -1 ) {
	    var url2="/doc/SignSMS/"+did;

	    $.get(url2).done(function(data) {
		if (data.indexOf("ERROR") != -1 ) {
		    $('#sign_div_oki').hide();
		    $('#sign_div_ref').hide();
 		    $('#sign_div_err').show();
		    $('#sign_div_notel').hide();
		}

		if (data.indexOf("OK") != -1 ) {
		    $('#sign_div_oki').show();
		    $('#sign_div_ref').hide();
 		    $('#sign_div_err').hide();
		    $('#sign_div_ok').hide();
		    $('#sign_div_notel').hide();
		}
		
	    }).fail(function(data) {
		$('#sign_div_oki').hide();
		$('#sign_div_ref').hide();
 		$('#sign_div_err').show();
		$('#sign_div_notel').hide();
	    })
	}
	
    }).fail(function(data) {
		$('#sign_div_oki').hide();
		$('#sign_div_ref').hide();
		$('#sign_div_err').show();
		$('#sign_div_notel').hide();
    })
}

function SubmitSignRequest() {
    $('#SignReqFrm').submit(function( event ) {
 
	// Stop form from submitting normally
	event.preventDefault();
	
	var url = $(this).attr( "action" );
	var formData = new FormData($(this)[0]);
	
	$.ajax({
	    url: url,
	    type: 'POST',
	    data: formData,
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function (returndata) {
		$('#sgnrs_div_askform').hide();
		$('#signreqdone').show();
		// permet de scroller la page afin de rendre visible le message.

		document.getElementById('signreqdone').scrollIntoView();
	    },
	    error: function (returndata) {
		$('#signreqdone').html("<h1> SEND-SIGN-REQUEST-ERROR </h1>");
	    }
	});
	
    });
    
    $('#SignReqFrm').submit();    
}

function SendSignRequestSimple() {
	if(!document.querySelector('#SignReqFrm').reportValidity()) {
		return false;
	}

    let hasSignersValues = $('[name="SIGNMEL[]"]:valid').length;
    if ( hasSignersValues ) {
		$('#sgnrs_div_oki').hide();
		$('#sgnrs_div_nok').show();
		$('#sgnrs_div_err').hide();
		$('#sgnrs_but_srch').removeClass("btn-primary");
		$('#sgnrs_but_srch').removeClass("btn-default");
		$('#sigreqtyp').val("manual");
		SubmitSignRequest();
    }
}

function SignAcceptFinal(did) {
    var jralcnt = $('#jralcnt').val();

    for (let i = 0; i < jralcnt; i++) {
		if ( $('#jral0_'+i).is(':checked') ) {
			$('#jral1_'+i).val(1);
		}
    }
    
    $('#sign_accept').submit();
}

function SignAuto2(did) {
    var stel = $('#signtel').val();
    var snam = $('#signnam').val();
    var smel = $('#signmel').val();

    if ( stel == "" || snam == "" || smel == ""  ) {
		$('#sign_div_err1').show();
		return;
    } else {
		$('#sign_div_err1').hide();
		$('#sign_div_b1').hide();
    }

    var url="/doc/SignCreateSMS/"+did+"/"+stel+"/"+smel+"/"+snam;

    $.get(url).done(function(data) {
	if (data.indexOf("ERROR") != -1 ) {
	    $('#sign_div_oki').hide();
	    $('#sign_div_ref').hide();
 	    $('#sign_div_err').show();
	}
	if (data.indexOf("OK") != -1 ) {
		$('#sign_div_step1').hide();
	    $('#sign_div_oki').show();
	    $('#sign_div_ref').hide();
 	    $('#sign_div_err').hide();
	}
	
    }).fail(function(data) {
		$('#sign_div_oki').hide();
		$('#sign_div_ref').hide();
		$('#sign_div_err').show();
    })
}

function SignAutoBack() {
	$('#sign_div_step1').show();
	$('#sign_div_oki').hide();
}

function SignAcceptFinalBack() {
	$('#sign_div_stp2').show();
	$('#sign_div_oki').hide();
}

function SignAutoFinal() {
    var stel = $('#signtel').val();
    var snam = $('#signnam').val();
    var smel = $('#signmel').val();
    if ($('#signimg').is(":checked")) {
	var simg = "img";
    } else {
	var simg = "def";
    }

    $('#signtel2').val(stel);
    $('#signnam2').val(snam);
    $('#signmel2').val(smel);
    $('#signimg2').val(simg);

    $('#sign_auto').submit();
}
