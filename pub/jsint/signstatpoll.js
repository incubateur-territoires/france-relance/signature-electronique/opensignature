// global variables for polling sign status

var SignPollCnt = 0;
var SignPollUrl = "";

function SignStatusPoll() {

    $.get(SignPollUrl).done(function(data) {
	if ( data.status == "insign" ) {
	    SignPollCnt += 1;
	    setTimeout(SignStatusPoll, SignPollCnt*5000);
	} 
	if ( data.status == "signdone" ) {
	    $('#bak2dos').submit();
	}
    }).fail(function(data) {
	$('#result').append("!!ERROR!!\n");
    })
    SignPollCnt += 1;
    setTimeout(SignStatusPoll, SignPollCnt*5000);
}

function SignStatusInit(url) {
    SignPollUrl = url;
    SigPollCnt = 0;
}
