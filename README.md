# OpenSignature #

* plateforme de signature electronique de documents
* ne nécessite pas de compte utilisateur pour le signataire
* un compte utilisteur est en revanche nécessaire pour les demandeurs
* signataire identifié par code secret SMS

## Utilisation ##

* créer un dossier ( briefcase ) 
* uploader des fichiers dedans
* faire une demande de signature en fournissant l'adresse mail du signataire
* ce dernier reçoit un mail
* il peut signer en fournissant son N° de téléphone et reçoit un code secret par SMS valable 15 minutes
* la plateforme dispose d'une API pour pouvoir être utilisée depuis une autre application

## Les fichiers ##

* les fichiers PDF sont signés *inline* dans l'onglet signature prévu à cet effet
* il est possible d'empiler plusieurs signatures dans un fichier PDF
* les autres fichiers (non PDF) sont signés avec un fichier signature détaché au format CMS (*Cryptographic Message Syntax*)
* il y a un fichier CMS par signataire


## Les pré-requis techniques ##

* serveur linux (ubuntu 1804/2004/2204) avec : bash, incrontab, coreutils, qpdf, ghostscript, jq
* apache (2.4) avec module : rewrite, dav, dav-fs, php, ssl  
* php ( > 7.2 ) avec module : json, redis, curl
  * augmenter au maximum ( 2047M ) les config php __post_max_size__ et __upload_max_filesize__
* redis server
* openssl CLI
* un jre  ( openjdk convient tres bien , ça doit marcher avec n'importe quelle version, openjdk v11 = OK ) 
* 2 certificats électroniques :
  * 1 pour la signature des fichiers, format p12, compatible EIDAS , reconnu par acrobat reader
  * 1 pour horodatage du dossier de preuve, format pem

> techniquement des certificats auto-signés peuvent suffire, mais ne seront pas reconnus par les lecteurs PDF

* un compte OVH d'émission de SMS   ( pour un autre fournisseur il faudra recoder l'API )
* un serveur smtp configuré pour l'envoi de mail.
* utilitaire mailsend  ( https://github.com/muquit/mailsend/ ) ( a compiler et installer dans le PATH du serveur )


## Installation ##

* créer un virtual-host apache avec http+https activé sur une url choisie, et la config directory suivante :
```
<Directory /repertoire/de/linstallation >
   Options +FollowSymLinks +Indexes<br>
   AllowOverride All<br>
    Require all granted<br>
</Directory>
```
* et
```
<Directory /repertoire/de/linstallation/data/ >
   DirectoryIndex disabled<br>
   Dav On<br>
</Directory>
```

* récupérer les sources du projet OpenSignature sur gitlab.girondenumerique.fr,  
* copier/dézipper/installer l'aborescence à la racine du __/repertoire/de/linstallation__ choisi
* dans le repertoire __./app/config__,  dupliquer et adapter les fichiers xxxx_sample.xxx selon votre environnement.
* assurez vous que les symlinks dans __./config__ pointent bien vers vos fichiers de config ( vers les _sample par default ),  vous pouvez utiliser le script __./config/make_sample_links__ pour refaire les liens
* vous devez impérativement :
  * spécifier le numéro de base redis que vous allez utiliser  dans __./config/Config.php__  ( remplacer la valeur 9999 mise par défault )
  * spécifier les chemins de vos certificats dans __./config/SignXXXConfig.php__
* spécifier votre serveur smtp dans __./config/MailConfig.sh__    si vous laissez le mode DEBUG,  les mails seront écrits dans un fichier /tmp et donc le compte smtp n'est pas nécessaire
* renseigner votre compte SMS OVH  dans __./config/Config.php__ , identiquement si vous laissez la classe Sms_Dumb (default) les SMS seront écrit dans /tmp et le compte ovh n'est pas nécessaire

* pointer votre navigateur vers l'url choisie, une page de vérification de divers prérequis techniques vous sera présentée, tant que tous les prérequis ne seront pas atteints.

* le compte utilisateur ( admin) créé par défaut est :
> login = admin@some.where
> passwd = admin007
* vous pouvez utilisez l'application  ( lien administration dans le footer ) pour créer les autres utilisateurs


## spécificités ##

* par défaut l'application utilise des comptes locaux, ( dans la base redis ), il est possible de relier l'application à un SSO openid-connect,  renseigner les paramètres SSO dans __./config/Config.php__

* les images (logos) utilisées pour la signature des pdf peuvent être changées  dans __./config/Config.php__ et __./config/SignPadesConfig.php__
