#!/usr/bin/php
<?php
// =============
// some init 
// =============

$topdir = dirname(dirname(__DIR__));

echo $topdir;

if ( ! file_exists($topdir."/config/Config.php") ) {
  echo "Can't find application config file - aborting !\n";
  exit(3);
}

chdir($topdir);

include $topdir."/config/Config.php";

// =================
// do it 
// =================

$gconf = new Config;
$cfil = $gconf->CacheDir."/fulldoslist.data";
$serdata = file_get_contents($cfil);

// unserialise now
$dflist = unserialize($serdata);

print_r($dflist);
