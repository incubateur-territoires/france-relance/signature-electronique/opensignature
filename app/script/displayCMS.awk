BEGIN {
    STATUS=""
}
/  contentType:/ {
    match($0, " *contentType: (.*)", rr)
    printf("= CMS Infos:\n")
    printf("== typeContenu: %s\n",rr[1])
}
/ *digestAlgorithms:$/ {
    STATUS="digestAlgorithms"
}
/ *certificates:$/ {
    STATUS="certificates"
    printf("= certificat:\n")
}
/ *key: $/ {
    STATUS="certificates/key"
}
/ *sig_alg: $/ {
    STATUS="certificates/sig_alg"
}
/ *crls:$/ {
    STATUS="crls"
}
/ *signerInfos:$/ {
    STATUS="signerInfos"
    printf("= infosSignature:\n")
}
/ *algorithm: .*/ {
    match($0, " *algorithm: (.*)", rr)

    switch (STATUS) {
	case "digestAlgorithms":
	    printf("== algoCondensat: %s\n",rr[1])
	    break
	case "certificates":
	    printf("== algoSignature: %s\n",rr[1])
	    break
	case "signerInfos/digestAlgorithm":
	    printf("== algoCondensat: %s\n",rr[1])
	    break
	case "signerInfos":
	    printf("== algoSignature: %s\n",rr[1])
	    break
    }
}
/ *issuer: .*/ {
    match($0, " *issuer: (.*)", rr)
    if ( STATUS == "certificates" ) {
	printf("== émetteur: %s\n",rr[1])
    }
}
/ *serialNumber: .*/ {
    match($0, " *serialNumber: (.*)", rr)
    if ( STATUS == "certificates" ) {
	printf("== numSerie: %s\n",rr[1])
    }
}
/ *notBefore: .*/ {
    match($0, " *notBefore: (.*)", rr)
    if ( STATUS = "certificates" ) {
	printf("== validité pasAvant: %s\n",rr[1])
    }
}
/ *notAfter: .*/ {
    match($0, " *notAfter: (.*)", rr)
    if ( STATUS == "certificates" ) {
	printf("== validité pasAprès: %s\n",rr[1])
    }
}
/ *subject: .*/ {
    match($0, " *subject: (.*)", rr)
    if ( STATUS == "certificates" ) {
	printf("== sujet: %s\n",rr[1])
    }
}
/ *UTCTIME:.*/ {
    match($0, " *UTCTIME:(.*)", rr)
    if ( STATUS == "signerInfos" ) {
	printf("== dateSignature: %s\n",rr[1])
    }
}
/ *digestAlgorithm: $/ {
    STATUS="signerInfos/digestAlgorithm"
}
/ *signedAttrs:$/ {
    STATUS="signerInfos"
}
END {
}
