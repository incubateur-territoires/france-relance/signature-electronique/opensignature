<?php

class Config {

  // init config
  function __construct() { 

    // ============================
    // CUSTOMIZABLE VARIABLES
    // ============================

    $this->name = "OpenSignature";

    // application topdir & topurl
    //     TopAppDir should match the PATH where you installed this software
    //               or let automaticaly guess based on __DIR__ predefined constant
    //     TopAppUrl should be empty on a virtual host,
    //               use the subdirectory of the site otherwise
    $this->TopAppDir = dirname(dirname(__DIR__));
    $this->TopAppUrl = "";

    // redis : self explanatory
    $this->redis_server = "localhost";
    $this->redis_port = 6379;
    //$this->redis_socket = "/path/to/redis.sock";
    $this->redis_base = 9999;     // choose a free redis base number here

    // should we use JQuery File Upload ? 
    // better to say yes (1), unless you use very old browser !
    $this->JQFileUpload = 1;

    // spool directory
    // used by batch script for thumbnail generation
    // DO NOT forget to initialise incrontab for this directory (see app/script/initincrontab)
    // if you modify this value, change the script accordingly 
    $this->upspool = $this->TopAppDir."/tmp/spool";
   
    //  sisgnspool directory
    // used by batch script for signature , different form thumbnail spool by security
    // DO NOT forget to initialise incrontab for this directory (see app/script/initincrontab)
    $this->signspool = $this->TopAppDir."/tmp/spoolsign";
   
    // mail sending configuration located in /config/MailConfig.sh file 
    
    //  default logo : 2 sizes
    $this->SignLogoSmall = "/pub/img/signatelec_h80.png";
    $this->SignLogoLarge = "/pub/img/signatelec_w200.png";

    // Auto Create Account ( only for SSO  based accounts )
    $this->AutoCreateAccount = 1;

    // autoindex file
    $this->AutoIndex = "autoindex.html";

    // default folder duration before automatic destruction  ( in seconds )
    $this->folderDuration = 15811200;     // 6 months

    // signature engine (by file extension)
    $this->SignEngine = Array(
        "pdf" => "PADES",
        "DEFAULT" => "CMS"
    );
    // needed because we do not have $_SERVER variable during batch signature
    $this->SignPlatformUrl = "https://open-signature.some.where";
    // logo used in pdf signature slip  ( bordereau )  // image path must be relative to /pub/img
    $this->SignPlatformLogo = "signatelec_h80.png";   
    
    // sms class and param
    //$this->SmsClass = "Sms_OVH";
    $this->SmsClass = "Sms_Dumb";
    $this->SmsParam = array();
    // DUMB sms param
    $this->SmsParam['dumb'] = "fakeSMS"; 
    // OVH account
    //$this->SmsParam['posturl'] = "/sms/sms-YYYYYYY-1/jobs";              // POST url
    //$this->SmsParam['appkey'] = "XXXXXXXXXXXXXXXXXXX";                      // Application Key
    //$this->SmsParam['secret'] = "YYYYYYYYYYYYYYYYYYYYYYYYYYYY";      // Application Secret
    //$this->SmsParam['endpoint'] = "ovh-eu";                              // Endpoint of API OVH Europe
    //$this->SmsParam['consumerkey'] = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"; // Consumer Key

    // SSO 
    $this->sso = "none";         // values = none | openidc
    //$this->ssourl = "https://sso.some.where/realms/XXXXX";
    //$this->ssoid = "ClientIDHere";
    //$this->ssosecret = "ClientSecretHere";
  
    // LOOL & WOPI
    $this->loolurl = "";
    $this->wopitpu = "";

    // GAAPSE API for user signature info
    $this->gapiurl = "";
    $this->gapikey = "";
    
    // internal TSA CA, ( only used for old signature proof, generated without AC )
    $this->internalTsaCA = "/path/to/timestamp/certif/timestamp_ac.pem";
    
    // ============================
    // LESS CUSTOMIZABLE VARIABLES
    // ============================

    // inner subdirectories
    // modify only if you have changed the software layout
    $this->DataDir = "/data";
    $this->AbsDataDir = $this->TopAppDir.$this->DataDir;
    $this->CacheDir = $this->TopAppDir."/tmp/cache";

    // Signature Proof Storage
    $this->ProofDir = $this->TopAppDir."/proof";

    // URL generation method
    // simple URLs, pretiest, but require apache mod-rewrite  ( see .htaccess )
    $this->csxDocClass = "/doc";
    $this->csxMgmtClass = "/mgt";
    $this->csxAdmClass = "/adm";
    // long URLs, use it if apache mod-rewrite not available for your system
    // $this->csxDocClass = "/wbx.php/Doc";
    // $this->csxMgmtClass = "/wbx.php/Mgmt";
    // $this->csxAdmClass = "/wbx.php/Admin";

  }
    
}

