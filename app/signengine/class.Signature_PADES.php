<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/config/SignPadesConfig.php";

class Signature_PADES {

    function __construct() {

        $this->conf = new SignPadesConfig;
 
        $topdir = dirname(dirname(__DIR__));  // can we reuse the one out of the class ?
        $this->theconf = $topdir."/config/SignPadesConfig.php";
 
    }

    // ####################################
    // API
    
    // input: array =
    //   fpath = file path to sign
    //   namid = name or ID of signer  
    //   mail  = mail of signer  
    //   phone = phone of signer  
    //   proof  = proof reference
    //   sigimg = path of signature image ( or null )
    // output: array =
    //  status = status of operation ( ok | err )
    //  result = file path of signed file
    //  messg = error message , eventually
    function SignOneFile( $argsign ) {

        $tmp = print_r($argsign, 1);
        $this->Log($tmp);

        if ( empty($argsign['fpath']) or empty($argsign['namid']) or empty($argsign['mail']) or ! file_exists($argsign['fpath']) ) {
            $retval = array('status'=>'err', 'result'=>"", 'messg'=>"name/mail/file_2_sign mandatory");
            return($retval);
        }
        
        // signature file name
        $fresult = substr($argsign['fpath'], 0, -4)."-sig.pdf";

        // do the sign
        if ( isset($argsign['sigimg']) ) {
            $action = sprintf("%s -c %s -f \"%s\" -u \"%s\" -r %s -s %s -o \"%s\"", $this->conf->signpades, $this->theconf, $argsign['fpath'], $argsign['namid'], $argsign['proof'], $argsign['sigimg'], $fresult );
        } else {
            $action = sprintf("%s -c %s -f \"%s\" -u \"%s\" -r %s -o \"%s\"", $this->conf->signpades, $this->theconf, $argsign['fpath'], $argsign['namid'], $argsign['proof'], $fresult );
        }

        $this->Log($action);
        system($action);
    
        $retval = array('status'=>'ok', 'result'=>$fresult, 'messg'=>"");
        return($retval);
        
    }

    private function Log($msg) {
        if ( isset($this->conf->logfile) ) {
            $fd = fopen($this->conf->logfile, "a+");
            fwrite($fd, $msg);
            fwrite($fd, "\n");
            fclose($fd);
        }
    }

}

