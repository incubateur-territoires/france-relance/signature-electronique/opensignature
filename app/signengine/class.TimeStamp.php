<?php

// ==============================================================
// generic timestamper sign proof class
//
//
// according to the configuration file (/config/SignTsaConfig.php)
// it may use :
// - internal openSSL key/certificats
// - one the freely available webservice
//
// d.roche@girondenumerique.fr
//
// ==============================================================

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/config/SignTsaConfig.php";

class TimeStamp {

    function __construct() {

        $this->conf = new SignTsaConfig;
       
    }

    // ####################################
    // API
    

    // input: 
    //  fpath = file path to timestamp
    //  fresult = file path timestamp result - not mandatory
    // output: array =
    //  status = status of operation ( ok | err )
    //  result = file path of timestamped file
    //  messg = error message , eventually
    function TSOneFile( $fpath, $fresult=null ) {

        if ( empty($fpath) or ! file_exists($fpath) ) {
            $retval = array('status'=>'err', 'result'=>"", 'messg'=>"file to timestamp not found");
            return($retval);
        }

        // generate the output name if not given
        if (is_null($fresult)) {
            $fresult = $fpath.".".date('U').".tsa";
        }

        switch($this->conf->default['type']) {
        case "internal":
            $retval = $this->internalTS($fpath,$fresult);
            $retval['result'] = $fresult;
            break;
            
        case "remote":
            $retval = $this->webServiceTS($fpath,$fresult);
            $retval['result'] = $fresult;
            break;
            
        default:
            $retval = array('status'=>'err', 'result'=>"", 'messg'=>"configuration invalid");
            break;
        }

        return($retval);
         
    }

    
    // input: 
    //  fpath = file path to timestamp
    //  fresult = file path result
    // output: array =
    //  status = status of operation ( ok | err )
    //  messg = error message , eventually
    private function internalTS( $fpath, $fresult ) {

        // USE SHA265  instead of SHA1
        // use php instead of openssl to get the hash of the file, same result !
        $hash = hash_file("sha256", $fpath);

        // create request
        $tmpfil1 = tempnam("/tmp", "tsgn");
        $action = sprintf("%s ts -query -sha256 -digest %s -cert -out %s", $this->conf->openssl, $hash, $tmpfil1);
        $this->Log($action);
        system($action);

        // create respons directly in tsa
        $action = sprintf("%s ts -reply -queryfile %s -signer %s  -inkey %s -config %s -token_out > %s 2>/dev/null", $this->conf->openssl, $tmpfil1, $this->conf->default['tscrt'], $this->conf->default['tskey'], $this->conf->default['sslcnf'], $fresult);
        $this->Log($action);
        system($action);

        // clean temp files
        unlink($tmpfil1);

        $ret = array('status'=>'ok', 'messg'=>"");
        return($ret);
         
    }

    // input: 
    //  fpath = file path to timestamp
    //  fresult = file path result
    // output: array =
    //  status = status of operation ( ok | err )
    //  messg = error message , eventually
    private function webServiceTS( $fpath, $fresult ) {

        // create request
        $tmpfil1 = tempnam("/tmp", "tsws");
        $action1 = sprintf("%s ts -query -data %s -no_nonce -sha512 -cert -out %s",$this->conf->openssl, $fpath, $tmpfil1);
        $this->Log($action1);
        system($action1);

        // call the WS for the respons
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->conf->default['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_PUT ,true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   // PUT then POST = cheat curl to avoid multipart form , awfull !
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/timestamp-query") );
        curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($tmpfil1) );
        $resp = curl_exec($ch);
        if (curl_errno($ch)) {
            $ret = array('status'=>'err', 'messg'=>curl_error($ch));
        } else {
            // write the result file
            $tmpfil2 = tempnam("/tmp", "tsws");
            $fd = fopen($tmpfil2, "w");
            fwrite($fd, $resp);
            fclose($fd);
            // transform tsr into tsa
            $action2 = sprintf("%s ts -reply -in %s -token_out -out %s",$this->conf->openssl, $tmpfil2, $fresult);
            $this->Log($action2);
            system($action2);
           
            $ret = array('status'=>'ok', 'messg'=>"");
        }
        curl_close($ch);

        // clean temp files
        unlink($tmpfil1);
        unlink($tmpfil2);

        return($ret);
         
    }

    // ####################################
    // get CA  used for proof ...

    function getCA() {

        $cafile = $this->conf->default['rootca'];
        return($cafile);
    }
    

    // ####################################
    // utilities

    private function Log($msg) {
        if ( isset($this->logfile) ) {
            $fd = fopen($this->logfile, "a+");
            fwrite($fd, $msg);
            fwrite($fd, "\n");
            fclose($fd);
        }
    }

}

