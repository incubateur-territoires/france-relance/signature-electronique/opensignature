<?php

// ======================================================
// generic info getters for various signature engines
//
// ======================================================

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/config/SignCmsConfig.php";
include_once $topdir."/config/SignTsaConfig.php";



class InfoSignature {

    function __construct() {

        $this->cms_conf = new SignCmsConfig;
        $this->tsa_conf = new SignTsaConfig; 
        
        // no need for Pades config ( this is the same as CMS )
    }

    // ####################################
    // API
    
    function GetCA() {
        $data = file_get_contents($this->cms_conf->rootca);
        return($data);
    }
    
    function GetCRT() {
        $data = file_get_contents($this->cms_conf->pubcert);
        return($data);
    }
    
    function GetTSC() {
        $data = file_get_contents($this->tsa_conf->tscert);
        return($data);
    }
    
    function GetCRL() {
        $data = file_get_contents($this->cms_conf->crlsign);
        return($data);
    }
    
    function GetGIT() {
        $data = file_get_contents($this->tsa_conf->gitversion);
        return($data);
    }

}

