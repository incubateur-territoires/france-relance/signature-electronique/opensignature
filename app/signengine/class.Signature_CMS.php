<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/config/SignCmsConfig.php";

class Signature_CMS {

    function __construct() {

        $this->conf = new SignCmsConfig;

    }
    
    // ####################################
    // API

    // input: array =
    //   fpath = file path to sign
    //   namid = name or ID of signer  
    //   mail  = mail of signer  
    //   phone = phone of signer  
    //   proof  = proof reference     ( optional )
    // output: array =
    //  status = status of operation ( ok | err )
    //  result = file path of signed file
    //  messg = error message , eventually
    function SignOneFile( $argsign ) {

        $tmp = print_r($argsign, 1);
        $this->Log($tmp);

        if ( empty($argsign['fpath']) or empty($argsign['namid']) or empty($argsign['mail']) or ! file_exists($argsign['fpath']) ) {
            $retval = array('status'=>'err', 'result'=>"", 'messg'=>"name/mail/file_2_sign mandatory");
            return($retval);
        }

        // extract passwd from file
        $passwd = trim(file_get_contents($this->conf->defpass));

        if ( $this->conf->legacyp12 ) {
            $legacy = "-legacy";
        } else {
            $legacy = "";
        }
        
        // extract key from p12
        $tmpfil1 = tempnam("/tmp", "sgn");
        $action = sprintf("%s pkcs12 -in %s -password pass:%s %s -nocerts -out %s -nodes", $this->conf->openssl, $this->conf->defcert,$passwd,$legacy,$tmpfil1);
        $this->Log($action);
        system($action);
        
         // extract cert from p12
        $tmpfil2 = tempnam("/tmp", "sgn");
        $action = sprintf("%s pkcs12 -in %s -password pass:%s %s -clcerts -nokeys -out %s -nodes", $this->conf->openssl, $this->conf->defcert,$passwd,$legacy,$tmpfil2);
        $this->Log($action);
        system($action);
        
        // cms header
        $topmsg = "# Signataire: ".$argsign['namid'].", identifié(e) par code SMS\n";
        if ( isset($argsign['proof']) ) {
            $topmsg .= "# ReferencePreuve: ".$argsign['proof']."\n";
        }

        // signature file name
        //$mlb = str_replace("@","_",str_replace(".","_",$argsign['mail']));
        $mlb = str_replace("@",".",$argsign['mail']);
        $fresult = $argsign['fpath'].".".$mlb.".cms"; 

        // do the sign
        $action = sprintf("echo '%s' > '%s'; %s cms -sign -md sha512 -binary -in '%s' -signer %s -inkey %s -outform PEM >> '%s'",$topmsg, $fresult, $this->conf->openssl, $argsign['fpath'], $tmpfil2, $tmpfil1, $fresult);
        $this->Log($action);
        system($action);

        // clean temp files
        unlink($tmpfil1);
        unlink($tmpfil2);

        $retval = array('status'=>'ok', 'result'=>$fresult, 'messg'=>"");
        return($retval);
    }

    // ####################################
    // utilities

    private function Log($msg) {
        if ( isset($this->conf->logfile) ) {
            $fd = fopen($this->conf->logfile, "a+");
            fwrite($fd, $msg);
            fwrite($fd, "\n");
            fclose($fd);
        }
    }

}

