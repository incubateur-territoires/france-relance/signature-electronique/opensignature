<?php
/**
 * First, download the latest release of PHP wrapper on github
 * And include this script into the folder with extracted files
 */
$topdir = dirname(dirname(__DIR__));
require $topdir."/lib/ovhsmsapi/autoload.php";
use \Ovh\Api;


class Sms_OVH {

    var $logfile;
    var $ovh;
    
    function __construct($param) {
        $this->logfile = "/tmp/smssign.log";
        //$this->logfile = null;

        $this->ovh = new Api( $param['appkey'],       // Application Key
                              $param['secret'],       // Application Secret
                              $param['endpoint'],     // Endpoint of API OVH Europe
                              $param['consumerkey']); // Consumer Key
        
        $this->operator = "OVH";
        $this->posturl =  $param['posturl'];

    }

    function SendSMS( $phone, $message ) {

        $tmp =  date('U')."|".$phone."|".$message;
        $this->Log($tmp);

        // ovh wants international phone number
        if ( $phone[0] == '0' ) {
            $realphone = "+33".substr($phone,1);
        } else {
            $realphone = $phone;
        }
        
        $result = $this->ovh->post($this->posturl, array(
            'charset' => 'UTF-8', // The sms coding (type: sms.CharsetEnum)
            'class' => 'phoneDisplay', // The sms class (type: sms.ClassEnum)
            'coding' => '7bit', // The sms coding (type: sms.CodingEnum)
            'message' => $message, // Required: The sms message (type: string)
            'noStopClause' => false, // Do not display STOP clause in the message, this requires that this is not an advertising message (type: boolean)
            'priority' => 'medium', // The priority of the message (type: sms.PriorityEnum)
            'receivers' => [ $realphone ], // The receivers list (type: string[])
            'sender' => '', // The sender (type: string)
            'senderForResponse' => true, // Set the flag to send a special sms which can be reply by the receiver (smsResponse). (type: boolean)
            'validityPeriod' => '2880', // The maximum time -in minute(s)- before the message is dropped (type: long)
        ));

        $res2 = print_r($result, 1);
        $this->Log($res2);

        // assume always OK ,  can we test an error return  ?  ( totalCreditsRemoved != 1 ) maybe ??
        $retval = Array(
            'time' => date("Y/m/d-H:i:s"),
            'tel' => $phone,
            'operator' => $this->operator) ;
        return($retval);
         
       
    }
 
    function Log($msg) {
        if ( isset($this->logfile) ) {
            $fd = fopen($this->logfile, "a+");
            fwrite($fd, $msg);
            fwrite($fd, "\n");
            fclose($fd);
        }
    }

}

