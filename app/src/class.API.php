<?php

//=====================================================
// PODOC API
// intended to be used with pastell connector
//
// adapted to podoc3 20190725
// authenticated for external usage
//
//=====================================================


$topdir = dirname(dirname(__DIR__));
include_once $topdir."/app/src/class.Data.php";
include_once $topdir."/app/src/class.Folder.php";
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.URL.php";

class API {

  //===============================================
  // framework
  //===============================================

  var $gconf;

  function __construct($conf) { 
    $this->gconf = $conf;
    $this->data = new Data($conf);
   }

  //===============================================
  // web app
  //===============================================

  public function Ping($vars) {

      $this->AskAuth();

      $this->Headers();
      echo "\"pong\"\n";
  }


  public function Create($vars) {

      // args = TITLE ( not mandatory )

      $this->AskAuth();

      $fobj = $this->data->PreCreateDos();

      if ( is_null($fobj) ) {
          $err= Array("ERROR" => "Pre-Create failure, contact administrator !");
          $this->Headers();
          echo json_encode($err);
          return;
      }

      $v2 = Array();
      if ( ! empty($vars["TITLE"]) ) {
          $v2['DOSNAM'] = $vars["TITLE"];
      } else {
          $v2['DOSNAM'] = "API".date("_Ymd_His");
      }

      // should we have an API datelim ???? 
      $datinf = $this->data->GetDateInfo('none');
      $v2['DOSLIM'] = date("Y-m-d", $datinf['datelim']);
      
      $this->data->UpdateDos($fobj, $v2);

      // prepare data for json return
      $rinfo = array();
      $rinfo['did'] = $fobj->did;
      $rinfo['title'] = $fobj->title;
      $rinfo['endoflife'] = $fobj->endoflife;
      
      $this->Headers();
      echo json_encode($rinfo);
      return;
  }

  public function CreateWithExternID($vars) {

      // arg1 = APPNUM ( external application number )
      // arg2 = XAID ( external application ID )
      // arg3 = TITLE ( not mandatory )

      $this->AskAuth();

      $fobj = $this->data->PreCreateDos();

      if ( is_null($fobj) ) {
          $err= Array("ERROR" => "Pre-Create failure, contact administrator !");
          $this->Headers();
          echo json_encode($err);
          return;
      }

      $v2 = Array();
      if ( ! empty($vars["TITLE"]) ) {
          $v2['DOSNAM'] = $vars["TITLE"];
      } else {
          $v2['DOSNAM'] = "API".date("_Ymd_His");
      }

      // associate external ID
      $xkey = sprintf("@%s@%s", $vars['APPNUM'], $vars['XAID']);
      $this->data->ExternalAppMappingAdd($xkey, $fobj->did);
      
      // set XAID in briefcase comment
      $v2['DOSCOM'] = sprintf("XAID=%s\n", $xkey);

      // should we have an API datelim ???? 
      $datinf = $this->data->GetDateInfo('none');
      $v2['DOSLIM'] = date("Y-m-d", $datinf['datelim']);
      
      $this->data->UpdateDos($fobj, $v2);

      // prepare data for json return
      $rinfo = array();
      $rinfo['did'] = $fobj->did;
      $rinfo['title'] = $fobj->title;
      $rinfo['endoflife'] = $fobj->endoflife;
      
      $this->Headers();
      echo json_encode($partial);
      return;
  }

  public function Info($vars) {

      // args = DID ( mandatory )

      $this->AskAuth();

      $fobj = Folder::getFromDid($vars['DID'],$this->gconf,1);
      if ( is_null($fobj) ) {
          $err= Array("ERROR" => "Bad or Obsolete Doc ID !");
          $this->Headers();
          echo json_encode($err);
          return;
      }

      // in API mode present only restricted set of info
      $info2 = Array();
      $info2['did'] = $fobj->did;
      $info2['title'] = $fobj->title;
      $info2['modified'] = $fobj->modified;
      $info2['endoflife'] = $fobj->endoflife;
      $info2['filelist'] = $fobj->filelist;

      // advanced dates for parapheur simulation 
      $info2['advdat_creat'] = $fobj->cretime;
      $info2['advdat_mod'] = $fobj->modtime;
      $info2['advdat_sigsts'] = null;       // still usefull ?
      $info2['advdat_blog'] = null;         // still usefull ?

      // lastblog if exists
      if ( $fobj->hasblog and $fobj->blogcnt >= 1 ) {

          $blgs = $fobj->fetchBlog();
          $blg1z = current($blgs);
          
          $info2['lastblgr'] = trim($blg1z['signature']);
          $tmp = trim($blg1z['comment']);
          // suppress eventually the fontawesome
          if ( preg_match('_<i class=.*</i>&nbsp;(.*)_', $tmp, $ar) ) {
              $info2['lastblog'] = $ar[1];
          } else {
              $info2['lastblog'] = $tmp;
          }
      }
      
      // calculate the status
      $status = $fobj->signstatus;
      if ( $fobj->signpartial ) {
          $status = $status." partial";
      }
      $info2['status'] = $status;
      
      $this->Headers();
      echo json_encode($info2);
      return;

  }

  public function AddFile($vars) {

      // POST method required
      // args = DID ( mandatory )

      // JQFU mode :
      // files posted in the 'files' variables ( file upload )
      // may  be tested with :
      // curl -F files[]=@/path/to/file http://podoc.girnum.fr/api/AddFile/xxxxxxxxxxxxxxxxxxxxxxxxxxx
      // but need to rewrite the data->AddJQFUFiles to accept multiple !!!
      
      // STD mode :
      // files posted in the 'nf1, nf2, ... nf8' variables ( file upload )
      // may  be tested with :
      // curl -F nf1=@/path/to/file -F nf2=@/path/to/file ... http://podoc.girnum.fr/api/AddFile/xxxxxxxxxxxxxxxxxxxxxxxxxxx
      
      $this->AskAuth();
      
      $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
      if ( is_null($fobj) ) {
          $err= Array("ERROR" => "Bad or Obsolete Doc ID !");
          $this->Headers();
          echo json_encode($err);
          return;
      }
    
      //$nbs = $fobj->AddJQFUFiles($vars);   // JQFU mode
      $nbs = $fobj->AddClassicUploadFiles($vars);    // STD mode 

      $this->Headers();
      $res = Array("status"=>"OK", "files uploaded"=>$nbs);
      echo json_encode($res);
      return;


  }

  public function DelFile($vars) {

      // arg1 = DID ( mandatory )
      // arg2 = FILE ( file_name )

      $this->AskAuth();
      
      $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
      if ( is_null($fobj) ) {
          $err= Array("ERROR" => "Bad or Obsolete Doc ID !");
          $this->Headers();
          echo json_encode($err);
          return;
      }

      $list = Array($vars['FILE']);
      $fobj->deleteFiles($list);  

      $this->Headers();
      $res = Array("status"=>"OK", "files deleted"=>1);    // DelDosFiles does not return the number of deleted files !!
      echo json_encode($res);
      return;
     

  }

  public function Del($vars) {

      // arg1 = DID ( mandatory )

      $this->AskAuth();
      
      $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
      if ( is_null($fobj) ) {
          $err= Array("ERROR" => "Bad or Obsolete Doc ID !");
          $this->Headers();
          echo json_encode($err);
          return;
      }

      $this->data->DeleteDosByObj($fobj);
      
      $this->Headers();
      $res = Array("status"=>"OK", "briefcase deleted"=>1);   
      echo json_encode($res);
      return;
     

  }

  public function FetchAll($vars) {

      // arg1 = DID ( mandatory )
      // do not return json status, return a zip for all files

      $this->AskAuth();
      
      $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
      if ( is_null($fobj) ) {
           $err= Array("ERROR" => "Bad or Obsolete Doc ID !");
          $this->Headers();
          echo json_encode($err);
          return;
      }

      $fobj->GenerateAndSendZip();

  }

  public function FetchFile($vars) {

      // arg1 = DID ( mandatory )
      // arg2 = FILE ( file_name, mandatory )
      // do not return json status, return the file content

      $this->AskAuth();
      
      $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
      if ( is_null($fobj) ) {
           $err= Array("ERROR" => "Bad or Obsolete Doc ID !");
          $this->Headers();
          echo json_encode($err);
          return;
      }
      
      $file = $vars['FILE'];
      $filpath = $this->gconf->TopAppDir.$this->gconf->DataDir."/".$fobj->rdir."/".$file;

      // force download

      header("MIME-Version: 1.0");
      header("Expires: Sat, 01 Jan 2000 05:00:00 GMT");        // date in the past
      header("Last-Modified:".date("D, d M Y H:i:s")." GMT");  // always modified
      header("Cache-Control: no-cache, must-revalidate");      // HTTP/1.1
      header("Pragma: no-cache");                              // HTTP/1.0
      header("Content-type: application/download");   
      header("Content-Disposition: attachment; filename=\"$file\"");
      header("Content-Description: File"); 

      $fn=fopen($filpath , "r"); 
      return fpassthru($fn); 

  }

  public function Ask4Sign($vars) {

      // arg1 = DID ( mandatory )
      // arg2 = SIGNOR ( mail address of the signator,  mandatory )
      // arg3 = ASKER ( mail address of the asker,   not mandatory )

      $this->AskAuth();
      
      $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
      if ( is_null($fobj) ) {
          $err= Array("ERROR" => "Bad or Obsolete Doc ID !");
          $this->Headers();
          echo json_encode($err);
          return;
      }

      //  duplication of the class.Doc.php/SignRequest2 function,  should be refactored !!
      
      // signerlist for the moment separated by , will be changed later.....
      $signerlist = explode(',', $vars['SIGNOR']);

      $params = array(
          "signercount" => count($signerlist),
          "signasker" => trim($vars['ASKER'])
      );
      // and trigger the signature request 
      $this->data->SignRequest($fobj, $params, $signerlist);
      foreach($signerlist as $signer) {
          $this->data->DosAttach4SignVisa($fobj->did, $signer);
      }

      // inits  
      $urls = URL::GetURLByFolder($this->gconf, $fobj);
      $tpl = new Savant3();
      
      // blog
      $tmp1 = sprintf("Demande de signature pour %s", $vars['SIGNOR']);
      $fobj->addBlog($params['signasker'],$tmp1);
    
      // send the mail
      $tpl->assign("URL", $urls);
      $tpl->assign("DOSINFO", $fobj->_all_ );
      $tpl->assign("SIGNASKR", $params['signasker'] );
      $tpl->assign("SIGNCOMM", "" );            // no comment with API
      $tpl->assign("APPNAM", $this->gconf->name );
      
      $tmpfname = tempnam("/tmp", "CSXsign");
      $fm = fopen($tmpfname, "a");
      fwrite($fm,$tpl->fetch("tpl/doc/mail_sign.html"));
      fclose($fm);

      $msubj = "Demande de signature : ".$fobj->title;
      
      foreach ( $signerlist as $signer) {  // should be detached ??
          $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$signer." \"".$msubj."\" ".$tmpfname;
          system(escapeshellcmd($launch));
      }

      // -----
      
      $this->Headers();
      $res = Array("status"=>"OK", "message"=>"signature asked");   
      echo json_encode($res);
      return;
  }

  //===============================================
  // online help
  //===============================================

  public function Help($vars) {

      $this->AskAuth();

      $urls = URL::GetURLSimple($this->gconf);

      $tpl = new Savant3();
      $tpl->assign("TITLE", $this->gconf->name);
      $tpl->assign("APIBAZ", $urls->GetApiBaseUrl());
      $tpl->assign("FAVICO", $this->gconf->favico);
     
      $tpl->display("tpl/adm/api_dsfr.html");
  }

  //===============================================
  // Authentication Stuff
  // API always use http authentication
  // even if SSO is configured !!
  // because SSO make the API usage more difficult
  //
  // this implies that the user used for API
  // has a locally defined password  !!!
  //===============================================
    
  private function AskAuth() {
      // force std (http) auth even if SSO configured
      $this->AskAuthStd();
  }

  private function AskAuthStd(){

      $realm = "API:".$this->gconf->name;
   
      // demande d'identification
      if ( !isset($_SERVER['PHP_AUTH_USER']) ) {
          $this->Authenticate($realm);
      } else {
          @session_start();
          if ( $_SERVER['PHP_AUTH_USER'] == @$_SESSION['LOGOFF'] ) {
              unset($_SESSION['AUTHENTICATED']);
              unset($_SESSION['LOGOFF']);
              $this->Authenticate($realm);
          }
          $uok = $this->data->VerifUser($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
          if ( ! $uok ) {
              $this->Authenticate($realm);
          }
          $_SESSION['AUTHENTICATED']=$_SERVER['PHP_AUTH_USER'];
      }
  }
  
  private function Authenticate($realm){
      Header("status: 401 Unauthorized"); 
      Header("WWW-Authenticate: Basic realm=\"$realm\" ");
      Header("HTTP/1.0 401 Unauthorized");
      
      // display if authentication canceled
      $this->Headers();
      $res = Array("status"=>"ERROR", "message"=>"unauthenticated !");   
      echo json_encode($res);

      exit;
  }

  //===============================================
  // utilities
  //===============================================

  private function Headers() {
      header("Content-Type: application/json;");
  }

  //===============================================
  // end
  //===============================================

}

