<?php

function AutoCreateAccount($mail, $conf) {

    // parse the mail
    if ( preg_match('/(.*)@(.*)\.(.*)/', $mail, $rm) != 1 ) {
        // echo "user no match mail\n";
        return(NULL);
    }
    $login = $rm[1];
    $domain = $rm[2];
    $tld = $rm[3];
    
    $ldpserv = $conf->ldap['server'];
    
    // connnect to ldap
    $ldpdn = sprintf($conf->ldap['reader'],$domain,$tld);
    
    $ldpconn = @ldap_connect($ldpserv, $conf->ldap['port']);
    // force LDAPv3 ( for openldap )
    ldap_set_option($ldpconn, LDAP_OPT_PROTOCOL_VERSION, 3);
    if ( ! @ldap_bind($ldpconn, $ldpdn, $conf->ldap['passwd']) ) {
        // echo "ldap connection failed\n";
        return(NULL);
    }
    
    // search ldap for user
    $ldpbase = sprintf($conf->ldap['base'],$domain,$tld);
    $ldpfilt = sprintf($conf->ldap['filter'],$login);
    $ldpattr = array("dn","sn","givenname");
    
    $sr = ldap_search($ldpconn, $ldpbase, $ldpfilt, $ldpattr); 
    if ( $sr == false ) {
        // echo "ldap search failed\n";
        return(NULL);
    }
    
    $info = ldap_get_entries($ldpconn, $sr);
    if ( $info == false ) {
        // echo "ldap search result failed\n";
        return(NULL);
    }
    
    if ( ! (isset($info['count']) and $info['count'] == 1) ) {
        // echo "ldap found nothing\n";
        return(NULL);
    }
    
    $nom = $info[0]['sn'][0];
    $prenom = $info[0]['givenname'][0];
    $passtag = "LDAP:".$ldpserv."::uid=%s,".$conf->ldap['base'];
    $defstatus = "premium";   // default autocreated user status

    //create user in redis
    $redis = new Redis(); 
    if ( $redis->connect($conf->redis_server, $conf->redis_port) ) {
        $redis->select($conf->redis_base);
    } else {
        return(NULL);
    }

    $nextuid = $redis->hincrby("AutoIncrement", "user", 1);
    if ( empty($nextuid) ) {
        return(NULL);
    }
    $udata = array();
    $udata['id'] = $nextuid;
    $udata['name'] = $nom;
    $udata['gvname'] = $prenom;
    $udata['password'] = $passtag;
    $udata['status'] = $defstatus;
    $udata['credate'] = date("Y-m-d H:i:s");
   
    $redis->hmset("user:".$mail, $udata);
    $redis->hset("Uid2Mel", $nextuid, $mail);

    return(array("ID"=>$nextuid,"PASSTAG"=>$passtag, "STATUS"=>$defstatus, "MAIL" => $mail));

}


