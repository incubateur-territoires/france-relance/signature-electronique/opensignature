<?php

$topdir = dirname(dirname(__DIR__));
require_once($topdir."/app/signengine/class.Signature_CMS.php");
require_once($topdir."/app/signengine/class.Signature_PADES.php");

//****************************************************
//  Signature Wrapper with dedicated standalone engine
//
//  can manage multiple sign engine default to CMS
//
//****************************************************

class SignatureWrapper {

    function __construct($engine = "CMS") {

        //$this->logfile = "/tmp/signwrapper.log";
        $this->logfile = null;

        $this->Init($engine);
    }

    function Init($engine) {

        switch($engine) {
        case "CMS":
            $this->engine = new Signature_CMS(); 
            $this->suffix = "cms";
            break;
        case "PADES":
            $this->engine = new Signature_PADES(); 
            $this->suffix = ".sig.pdf";
        }
    }


    // input : array =
    //   fpath = file path to sign
    //   namid = name or ID of signer  
    //   mail  = mail of signer  
    //   phone = phone of signer  
    //   proof  = proof reference     ( optional )
    //   crtfile = certificat info    ( no longer used )
    //   crtpass = certificat passwd  ( no longer used )
    // output: array =
    //  status = status of operation ( ok | err )
    //  result = file path of signed file
    //  messg = error message , eventually
    // 
    function SignOneFile( $signinfo ) {

        return($this->engine->SignOneFile($signinfo));
        
    }
 
    private function Log($msg) {
        if ( isset($this->logfile) ) {
            $fd = fopen($this->logfile, "a+");
            fwrite($fd, $msg);
            fwrite($fd, "\n");
            fclose($fd);
        }
    }

}

