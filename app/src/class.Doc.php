<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.Data.php";
include_once $topdir."/app/src/class.Folder.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.PluginLib.php";
include_once $topdir."/app/src/class.Debug.php";
include_once $topdir."/app/src/class.Contacts.php";
include_once $topdir."/app/src/class.Signature.php";

class Doc {

  //===============================================
  // framework
  //===============================================

  var $public_functions;
  var $gconf;
  
  protected $contacts;

  function __construct($conf) { 
    $this->gconf = $conf;
    $this->data = new Data($conf);
    $this->contacts = new Contacts($conf);
    $this->debug = new Debug();
  }

  //===============================================
  // web app
  //===============================================

  function ExamData($vars) {


    $signerlist = explode("\r\n", $vars['SIGNMEL']);
          
    echo "<pre>\n";
    foreach( $signerlist as $id => $val) {
        printf("%s|%s|\n",$id,$val);
    }
    echo "</pre>\n";
    exit(0);

    //$this->debug->Debug1("ExamData");
    //exit(0);

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf, 1);
    $sobjs = $dobj->getSignatureObjects();

    //$info =  $this->data->fetchUserSignInfo("d.roche@girondenumerique.fr");

    //$li1 = $this->data->GetDosListByUser("d.roche@girondenumerique.fr");
    //$u1 = $this->data->UserInfo("d.roche@girondenumerique.fr");
    //$ulf = $this->data->GetUserListFull();
    //$xap = $this->data->GetXappList();
  

    //echo $urls->GetBrowser();
    //exit(0);

    echo "<pre>\n";
    //print_r($_SERVER);
    //print_r($vars);
    foreach($sobjs as $sobj ) {
        echo "====".$sobj->mail."\n";
        $s0 = $sobj->status;
        print_r($s0);
    }
    //print_r($ulf);
    //print_r($this->gconf);
    echo "</pre>\n";
    exit(0);
    //echo "<pre>".$_SERVER['PHP_AUTH_USER']."</pre>\n";

    //exit(0);

    //echo "status = ".$this->data->CurrentUserStatus();
    //exit(0);
   
    @session_start();
    //$this->debug->Debug2("DOSINFO", $dosinf);
    //$this->debug->Debug2("COOKIE", $_COOKIE);
    $this->debug->Debug2("SESSION", $_SESSION);
    return;

    $tpl = new Savant3();

    //$urls = URL::GetURLByInfo($this->gconf, $dosinf);

    //$tpl->assign("MSG", "deb url func");
    //$tpl->assign("URL", $urls);
   
    $tpl->display("tpl/doc/debug3.html");
 

  }

  function Display($vars) {
    $tpl = new Savant3();

    $tpl->assign("APPNAM", $this->gconf->name);

    if ( empty($vars['DID']) ) {
      $tpl->assign("MSG", "Veuillez fournir un identifiant de porte-documents !");
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/doc/error2.html");
      return;
    }

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf, 1);

    if ( $dobj == null ) {
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/doc/invalid.html");
      return;

    } 

    $ra = $this->Authenticate($dobj);

    $tpl->assign("DID", $dobj->did);

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    // for menu we need content display parameters and the lang
    $urls->InitContentDisplayParameters();
    
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $dobj->_all_);
    
    $sobjs = $dobj->getSignatureObjects();
    $tpl->assign("SIGNATAIRES", $sobjs);


    switch($ra) {
    case ACCES_NO:
      exit(0);
      break;
    case ACCES_RW:
      $tpl->assign("MODACC", 'RW');
      break;
    case ACCES_RO:
      $tpl->assign("MODACC", 'RO');
      break;
    }
    $tpl->assign("ATTACHARG", $dobj->did."/".$ra );

    // guess what page to display
    // if PAGE arg given , display it
    // if AutoIndex file is present display it
    // if no file -> add
    // if message -> blog  ?? no longer ? 
    if ( ! isset($vars['PAGE']) ) {
        $nbfil = count($dobj->filelist);
        if ( $nbfil == 0 ) {
            $tpl->assign("CONTURL", $urls->GetDosMethod("AddFile1"));
        } else {
            // back to thumbs display !
            //if ( @$dosinf['signature'] == 1 ) {
            //    $tpl->assign("CONTURL", $urls->GetDosMethod("SignAction"));
            //} else {
            //    $tpl->assign("CONTURL", $urls->GetDosMethod("SignRequest"));
            //}
            $firstFile = $dobj->resetFileList();
            $tpl->assign("CONTURL",  $urls->GetDosData($firstFile));
            //$tpl->assign("CONTURL", $urls->GetDosMethod("DispThumbs"));
        }
    } else {
        $tpl->assign("CONTURL",  $urls->GetDosMethod($vars['PAGE']));
    }

    @session_start();
    if ( isset($_SESSION['AUTHENTICATED']) ) {
        $tpl->assign("BAKICOURL", $urls->GetMgmtMethod('DosList') );
        $tpl->assign("DISPLOGOFF", 1 );
        $tpl->assign("DISPACCOUNT", 1 );
        $uinfo = $this->data->UserInfo($_SESSION['AUTHENTICATED']);
        $tpl->assign("UINFO", $uinfo );
        
    } else {
        $tpl->assign("BAKICOURL", $urls->GetMgmtMethod('Index') );
        $tpl->assign("DISPLOGOFF", 0 );
        $tpl->assign("DISPACCOUNT", 0 );
    }
    
    
    $tpl->display("tpl/doc/signature-page.html");
        
  }

  function DispPlugin($vars) {
    $tpl = new Savant3();

    //$this->debug->Debug2("DISPPLUGIN", $vars);
    //return;
 
    $dobj = Folder::getFromDid($vars['DID'], $this->gconf, 1);
    $ra = $this->AuthenticateInner($dobj);

    $file = $vars['FILENAM'];

    $plugnam = $vars['PLUGIN'];
    $classplug = $this->gconf->TopAppDir."/app/plugins/".$plugnam."/class.".$plugnam.".php";
    if ( file_exists($classplug) ) {

      $pluglib = new PluginLib($dobj, $file, $this->gconf );

      include_once $classplug;
      $obj = new $plugnam($pluglib);
    
      $obj->Display();

      unset($classplug);
      unset($plugnam);
      unset($obj);

    } else {

      print ("plugin $classplug cannot be found\n");

    } 
  }

  // is this still used ?
  function DispList($vars) {
    $tpl = new Savant3();

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf, 1);
    $ra = $this->AuthenticateInner($dobj);

    //$this->debug->Debug2("DOSINFO", $dosinf);
    //return;

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    // for thumbs we need content display parameters
    $urls->InitContentDisplayParameters();

    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $dobj->_all_);

    $tpl->display("tpl/doc/part_listfil.html");
  }

  function DispParam($vars) {
    $tpl = new Savant3();

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $dobj->_all_);

    switch($ra) {
    case ACCES_RW:
      $utyp = $this->data->CurrentUserStatus();
      switch($utyp) {
      case 'none':
      case 'std':
	$datesel = "no";
	break;
      case 'premium':
      case 'admin':
	$datesel = "yes";
	break;
      }
      if ( isset($dosinf['passadm']) ) {
	$tpl->assign("FIGACTION", "defige" );
      } else {
	$tpl->assign("FIGACTION", "fige" );
      }
      $tpl->assign("DATESEL", $datesel );
      $tpl->display("tpl/doc/content/settings-writable_content.html");
      break;
    case ACCES_RO:
      $tpl->display("tpl/doc/content/settings-read_content.html");
      break;
    }

  }

  function DispShare($vars) {
    $tpl = new Savant3();

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $dobj->_all_);

    // generate the mailto url :
    $subj = "Un dossier de Signature pour vous.";
    $body = $tpl->fetch("tpl/doc/mailto_share.txt");
    $m2url = $urls->GetMailto($subj,$body);

    $tpl->assign("MAILTO", $m2url);

    if ( isset($this->gconf->xidkiv) ) {
        $tpl->assign("DOAR", true);
    }

    $this->Authenticate($dobj);       // is it usefull ? probably not
    $tpl->display("tpl/doc/content/share_content.html");
  }

  //  normaly no longer used 
  function DispAttach($vars) {
    
    //$this->debug->Debug1("DispAttach");
    $tpl = new Savant3();

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);
 
    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $dobj->_all_);
    $tpl->assign("MODE", $ra);

    $tpl->display("tpl/doc/part_attach.html");
  }

  function DispDav($vars) {
    
    //$this->debug->Debug1("DispDav");
    $tpl = new Savant3();

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);
 
    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $dobj->_all_);

    $tpl->display("tpl/doc/content/direct-access_content.html");
  }

  function AddFile1($vars) {

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $urls = URL::GetURLByDID($this->gconf, $vars['DID'] );
    $tpl->assign("URL", $urls);
    $tpl->assign("DID", $vars['DID']);

    if ( isset($vars['mode']) ) {
        switch($vars['mode']) {
        case "classic":
            $tpl->display("tpl/doc/part_addfil_simple.html");
            break;
        case "jqfu":
            $tpl->display("tpl/doc/part_addfil_jqfu.html");
            break;
        }
    } elseif ( $this->gconf->JQFileUpload ) {
        $tpl->display("tpl/doc/part_addfil_jqfu.html");
    } else {
        $tpl->display("tpl/doc/part_addfil_simple.html");
    }
    
  }

 function AddFile2($vars) {

    //$this->debug->Debug1("addfile2");
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $res = $fobj->AddClassicUploadFiles($vars);
 
    $urls = URL::GetURLByFolder($this->gconf, $fobj);

    $url = $urls->GetDosMethod('Display');
    header("Location: ".$url);
  }

  function AddFileJQFU($vars) {
      
    //$this->debug->DebugToFile("/tmp/jqfu_deb.log", $vars);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $ret = $fobj->AddJQFUFiles($vars);
    
    if ( $ret == null ) {
      return;
    }

    // jquery file upload need a json return 
    header('Content-type: application/json');
    $tpl = new Savant3();
    $tpl->assign("NAME", $ret['name']);
    $tpl->assign("TYPE", $ret['type']);
    $tpl->assign("SIZE", $ret['size']);
    $tpl->display("tpl/doc/jqfu_ret.json");

  }

  function ActionOnFiles($vars) {
    //$this->debug->Debug2("ActionOnFile", $vars);
    
    switch($vars['ACTION']) {
    case "DEL":
      $this->DelFile1($vars);
      break;
    case "REN":
      $this->RenFile1($vars);
      break;
    case "ZIP":
      $this->GetZip1($vars);
      break;
    default:
      echo "Unknown Action : Should not happen !";
      return;
    }
  }

  // called by ActionOnFiles
  function DelFile1($vars) {

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DID", $vars['DID']);

    if ( isset($vars["CFI"]) ) {
      $tpl->assign("DLIST", $vars["CFI"]);
      $tpl->display("tpl/doc/content/delete-documents_content.html");
    } else {
      $tpl->assign("MSG", "Veuillez sélectionner le ou les documents à supprimer.");
      $tpl->display("tpl/doc/content/error_content.html");
    }

  }

  function DelFile2($vars) {

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $fobj->deleteFiles($vars['LDL']);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $url = $urls->GetDosMethod('Display');
    header("Location: ".$url);
  }

  // called by ActionOnFiles
  function RenFile1($vars) {

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DID", $vars['DID']);

    if ( isset($vars["CFI"]) ) {
      $tpl->assign("RLIST", $vars["CFI"]);
      $tpl->display("tpl/doc/content/rename-documents_content.html");
    } else {
      $tpl->assign("MSG", "Veuillez sélectionner le ou les documents à renommer.");
      $tpl->display("tpl/doc/content/error_content.html");
    }

  }
  function RenFile2($vars) {

    //$this->debug->Debug2("RenFile2", $vars);
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $fobj->renameFiles($vars['ORN'], $vars['NRN']);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $url = $urls->GetDosMethod('Display');
    header("Location: ".$url);
  }


  function Update($vars) {

    //$this->debug->Debug2("update", $vars);
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $this->data->UpdateDos($fobj, $vars);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $url = $urls->GetDosMethod('Display');
    header("Location: ".$url);
 
  }

  function Delete($vars) {

    //$this->debug->Debug2("update", $vars);
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $this->data->DeleteDosByObj($fobj);
 
    $urls = URL::GetURLSimple($this->gconf);
    $url = $urls->GetMgmtMethod('Index');
    header("Location: ".$url);
 
  }

  function SetAuth($vars) {

    //$this->debug->Debug2("setauth", $vars);
    //exit(0);

    @session_start();
    $appnam = $this->gconf->name;
    $_SESSION[$appnam.'_'.$vars['DID']] = $vars['PASSWD']; 
 
    $urls = URL::GetURLByDID($this->gconf, $vars['DID']);
    $url = $urls->GetDosMethod('Display' );
  
    header("Location: ".$url);
 
  }

  function PassAdm($vars) {

    //$this->debug->Debug2("passadm", $vars);
    //exit(0);

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);

    if ( $vars['PASSADM'] == $dobj->passadm ) {
      // dont unlock here !
      //$this->data->UnlockDos($dosinf['did']);
      @session_start();
      $appnam = $this->gconf->name;
      $_SESSION[$appnam.'ADM_'.$vars['DID']] = $vars['PASSADM']; 

      $urls = URL::GetURLByFolder($this->gconf, $dobj);
      $url = $urls->GetDosMethod('Display');
      header("Location: ".$url);

    } else {
      $tpl = new Savant3();

      $urls = URL::GetURLByFolder($this->gconf, $dobj);
      $tpl->assign("URL", $urls);
      $tpl->assign("MSG", "Identifiant Administrateur incorrect !");
      $tpl->assign("RURL", $urls->GetDosMethod('Display') );
      $tpl->display("tpl/doc/error2.html");
    }

  }


  function Fige($vars) {

    //$this->debug->Debug1("Fige");
    //exit(0);

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);

    $dobj->lock($vars['PASSADM']);

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $url = $urls->GetDosMethod('Display', "PAGE=DispParam");
    header("Location: ".$url);

  }

  function Defige($vars) {

    //$this->debug->Debug1("Defige");
    //exit(0);

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);

    $dobj->unLock();

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $url = $urls->GetDosMethod('Display', "PAGE=DispParam");
    header("Location: ".$url);

  }

  // called by ActionOnFiles
  function GetZip1($vars) {

    $tpl = new Savant3();

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DID", $vars['DID']);

    if ( isset($vars["CFI"]) ) {
      $tpl->assign("ZLIST", $vars["CFI"]);
    } else {
      $tpl->assign("ZLIST",Array());
    }
    $tpl->display("tpl/doc/content/multiple-download_content.html");

  }

  function GetZip($vars) {

    //$this->debug->Debug2("getzip", $vars);
    //exit(0);

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf, 1);
    $ra = $this->AuthenticateInner($dobj);

    $cnt = !empty($vars['LDL']) ? count($vars['LDL']) : 0;
    if ( $cnt == 0 ) {
        // all files, but verify if just 1
        if ( count($dobj->filelist) == 1 ) {
            // force download
            $pseudovar = Array('FILENAM' => $dobj->filelist[0],
                               'DID' => $dobj->did );
            $this->Download($pseudovar);
        } else {
            $dobj->GenerateAndSendZip();
        }
    } elseif ( $cnt == 1 )  {
        // force download
        $pseudovar = Array('FILENAM' => $vars['LDL'][0],
                           'DID' => $dobj->did );
        $this->Download($pseudovar);
    } else {
        $f2z = "\"".implode("\" \"", $vars['LDL'])."\"";
        $dobj->GenerateAndSendZip($f2z);
    }
  }

  function DispBlog($vars) {

    $tpl = new Savant3();

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->Authenticate($dobj);

    $blogdata = $dobj->fetchBlog();

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $dobj->_all_);
    $tpl->assign("BLOGDATA", $blogdata);
    
    $tpl->assign("ROMODE", 1);

    // forum is writable , whatever folder status !
    $tpl->display("tpl/doc/content/commentaries_content.html");

  }

  function SendMail($vars) {
    //$this->debug->Debug2("SendMail", $vars);
    //exit(0);

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->Authenticate($dobj);

    // multiline mail processing
    $mlm = explode("\n", $vars["SNDRMEL"]);

    //$this->debug->Debug2("MLM", $mlm);
    //exit(0);

    $tpl = new Savant3();
    $urls = URL::GetURLByFolder($this->gconf, $dobj);

    $cntmel = 0;
    foreach($mlm as $ind => $melto) {

        $melto2 = trim($melto);
        
        if ( preg_match("/.*@.*\..*/", $melto2) ) {

            $tpl->assign("URL", $urls);
            $tpl->assign("FROM", $vars["SNDRNAM"] );
            $tpl->assign("MESSG", $vars["SNDRMSG"] );
            $tpl->assign("DOSINFO", $dobj->_all_ );
            $tpl->assign("APPNAM", $this->gconf->name );
            
            $tmpfname = tempnam("/tmp", "CSXmel");
            $fm = fopen($tmpfname, "a");
            if ( $vars['SNDAR'] == "on" ) {
                $tpl->assign("XID", $this->data->encode_xid($melto2) );
                fwrite($fm,$tpl->fetch("tpl/doc/mail_share_ar.html"));
            } else {
                fwrite($fm,$tpl->fetch("tpl/doc/mail_share.html"));
            }
            fclose($fm);
            
            $launch=$this->gconf->TopAppDir."/app/script/melsnd ".$melto2." \"Dossier de Signature\" ".$tmpfname;
            system(escapeshellcmd($launch));
            $cntmel += 1;
        }
    }
        
    $tpl2 = new Savant3();
    $tpl2->assign("URL", $urls);
    $tpl2->assign("MSG", sprintf("%d mail(s) de partage ont été envoyé(s) !",$cntmel));
    if ( $cntmel >= 1 ) {
        $tpl2->display("tpl/doc/part_success1.html");
    } else {
        $tpl2->display("tpl/doc/content/error_content.html");
    }
  }

  //===============================================
  // display files and external url
  //===============================================

  function GetData($vars) {


    // A quoi sert cette fonction ??

    //  $this->debug->Debug2("GetData", $vars);

    header('Content-type: application/json');

    $tpl = new Savant3();
    $tpl->display("tpl/doc/jqfu_ret.json");


    exit(0);
  }


  function GetSignersList($vars) {

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);

    $sobjs = $dobj->getSignatureObjects();

    $signers = [];
    foreach($sobjs as $sobj) {

      $signer = [
        'mail' => $sobj->mail,
        'status' => $sobj->status,
      ];
     
      $signers[] = (object) $signer;
    }
    header('Content-type: application/json');
    echo json_encode($signers);
    return;
  }

  function Download($vars) {

    //$this->debug->Debug2("Download", $vars);
    //exit(0);

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($dobj);

    $file = $vars['FILENAM'];

    //$this->debug->Debug2("Download", $dosinf);
    //exit(0);

    $filpath = $this->gconf->TopAppDir.$this->gconf->DataDir."/".$dobj->rdir."/".$file;

    //echo $filpath;
    //exit(0)

    // force download

    header("MIME-Version: 1.0");
    header("Expires: Sat, 01 Jan 2000 05:00:00 GMT");        // date in the past
    header("Last-Modified:".date("D, d M Y H:i:s")." GMT");  // always modified
    header("Cache-Control: no-cache, must-revalidate");      // HTTP/1.1
    header("Pragma: no-cache");                              // HTTP/1.0
    header("Content-type: application/download");   
    header("Content-Disposition: attachment; filename=\"$file\"");
    header("Content-Description: File"); 

    $fn=fopen($filpath , "r"); 
    return fpassthru($fn); 

    exit(0);
  }

  function ExternUrl($vars) {
    //$this->debug->Debug2("ExternUrl", $vars);
    //exit(0);

    $tpl = new Savant3();

    $tpl->assign("XU", base64_decode($vars['XU']));
   
    $tpl->display("tpl/doc/part_xu.html");
 
  }

  function GetQR($vars) {
    //  png generation with qrencode

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    
    $sharurl = $urls->GetMinimumURL();
    
    header("MIME-Version: 1.0");
    header("Expires: Sat, 01 Jan 2000 05:00:00 GMT");        // date in the past
    header("Last-Modified:".date("D, d M Y H:i:s")." GMT");  // always modified
    header("Cache-Control: no-cache, must-revalidate");      // HTTP/1.1
    header("Pragma: no-cache");                              // HTTP/1.0
    header("Content-type: image/png");   

    $fn=popen("qrencode -lH -o - ".$sharurl , "r"); 
    return fpassthru($fn); 
  }

  function GetStatus($vars) {
    //  json status of the briefcase
    //  may be used for polling ...  ( delayed sign )

    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);
    header("Content-type: application/json");   

    $sts = array();

    if ( is_null($dobj) ) {
        echo json_encode($sts);
        return;
    }

    $sts['status'] = "std";
    
    if ( @$dobj->signature == true) {
        $sts['status'] = "insign";
    } elseif ( @$dobj->status == "done") {
        $sts['status'] = "signdone";
    };
    
    echo json_encode($sts);
    
  }

  //===============================================
  // signature stuff
  //===============================================

  function SignRequest($vars) {
    
    //$this->debug->Debug1("SignRequest");
    //exit(0);

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);
 
    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $fobj->_all_);

    @session_start();
    if ( empty($_SESSION['AUTHENTICATED']) ) {
        $tpl->assign("ASKER", "");
        $tpl->assign("UCONTACTS", []);
    } else {
        $tpl->assign("ASKER", $_SESSION['AUTHENTICATED']);
        $contacts = $this->contacts->getUserContacts($_SESSION['AUTHENTICATED']);
        $tpl->assign("UCONTACTS", $contacts);
    }

    $tpl->assign("FUNCSIGNREQ", "SendSignRequestSimple()");
    
    $tpl->display("tpl/doc/content/signature-request_content.html");
  }

  function SignRequest2($vars) {
    
    //$this->debug->Debug1("SignRequest2");
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);
 
    // $vars['SIGNMEL'] is now an array of emails  (<input name="SIGNMEL[]" />)
    $signerlist = $vars['SIGNMEL'];

    // signature request parameters 
    $params = array(
        "signercount" => count($signerlist),
        "signasker" => trim($vars['SIGNASKR']),
        "signacomm" => trim($vars['SIGNCOMM'])
    );
    // and trigger the signature request 
    $this->data->SignRequest($fobj, $params, $signerlist);

    foreach($signerlist as $signer) {
        $this->data->DosAttach4SignVisa($fobj->did, $signer);
    }
    
    
    // inits  
    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl = new Savant3();

    // blog
    $tmp1 = sprintf("Demande de signature pour %s", implode(',', $signerlist));
    $fobj->addBlog($params['signasker'],$tmp1);
                    
    // send all the mails,
	$tpl->assign("URL", $urls);
	$tpl->assign("DOSINFO", $fobj->_all_ );
	$tpl->assign("SIGNASKR", $params['signasker'] );
	$tpl->assign("SIGNCOMM", $params['signacomm'] );
	$tpl->assign("APPNAM", $this->gconf->name );

    $tmpfname = tempnam("/tmp", "CSXsign");
    $fm = fopen($tmpfname, "a");
    fwrite($fm,$tpl->fetch("tpl/doc/mail_sign.html"));
    fclose($fm);
    
    $msubj = "Demande de signature : ".$fobj->title;

    foreach ( $signerlist as $signer) {  // should be detached ??
        $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$signer." \"".$msubj."\" ".$tmpfname;
        system(escapeshellcmd($launch));
    }

    // at the end remove the mail file....
    unlink($tmpfname);
    
    // Add contact to user contactList if needed  , DAN probably useless at this point 
    // $this->contacts->addContactIfNeeded($vars);

    // nothing to return for now 
  }

  function SignAction($vars) {
    
    //$this->debug->Debug2("signaction",$vars);
    //exit(0);

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf, 2);
    $ra = $this->AuthenticateInner($fobj);
 
    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $fobj->_all_);

    if ( $fobj->status == "delayed" ) {
        $tpl->display("tpl/doc/part_signdelayed.html");
    } else {
        @session_start();
        if ( isset($_SESSION['AUTHENTICATED']) ) {
            $uinfo = $this->data->UserInfo($_SESSION['AUTHENTICATED'], true);
            $tpl->assign("OWNNAME", $uinfo['gvname']." ".$uinfo['name'] );
            $tpl->assign("OWNMAIL", $uinfo['mail']);
            if ( isset($uinfo['signphone']) ) {
                $tpl->assign("OWNPHONE", $uinfo['signphone'] );
                $tpl->assign("SIGNINFOK", 1);
            } else {
                $tpl->assign("OWNPHONE", "" );
                $tpl->assign("SIGNINFOK", 0);
            }
            if ( isset($uinfo['imageSignature']) and $uinfo['imageSignature'] != "" ) {
                $idata = "data:".$uinfo['mimetype'].";base64,".$uinfo['imageSignature'];
                $tpl->assign("SIGNIMAGE", $idata);
            } else {
                $tpl->assign("SIGNIMAGE", "");
            }
            
        } else {
            $tpl->assign("OWNNAME", "" );
            $tpl->assign("OWNMAIL", "" );
            $tpl->assign("OWNPHONE", "");
            $tpl->assign("SIGNINFOK", 0);
            $tpl->assign("SIGNIMAGE", "");
         }

        if ( $vars['ACTION'] == 'sign' ) {
            $tpl->display("tpl/doc/content/signature-accept_content.html");
        } elseif ( $vars['ACTION'] == 'refuse' ) {
            $tpl->display("tpl/doc/content/signature-refuse_content.html");
        } else {
            $this->debug->Debug1("should not happen");
       }
    }
  }

  function SignAbort($vars) {
    
    //$this->debug->Debug2("signaction",$vars);
    //exit(0);

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $fobj->_all_);

    $tpl->display("tpl/doc/content/signature-abort_content.html");
  }

  function SignAbort2($vars) {
    
    //$this->debug->Debug1("SignAbort");
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    if ( $ra == 0 ) {
        $tpl = new Savant3();
        $tpl->assign("MSG", "Porte-documents invalide !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    //  $fobj->SignatureAbort();  // no longer used,  but keep the code, just in case
    // replaced by folder delete and mail sent

    // send abort mail to all signer
    $sobjs = $fobj->getSignatureObjects();
    $tpl = new Savant3();
    $urls = URL::GetURLByFolder($this->gconf, $fobj);

    // send all the mails,
	$tpl->assign("URL", $urls);
	$tpl->assign("DOSINFO", $fobj->_all_ );
	$tpl->assign("APPNAM", $this->gconf->name );

    $tmpfname = tempnam("/tmp", "CSXremail");
    $fm = fopen($tmpfname, "a");
    fwrite($fm,$tpl->fetch("tpl/doc/mail_abort.html"));
    fclose($fm);
    
    $msubj = "Annulation de dossier signature : ".$fobj->title;

    foreach ( $sobjs as $sobj) {  // should be detached ??
        if ( ! $sobj->codeverified ) {     # status not relevant,  may be delayed
            $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$sobj->mail." \"".$msubj."\" ".$tmpfname;
            system(escapeshellcmd($launch));
        }
    }

    // at the end remove the mail file....
    unlink($tmpfname);

    // and finaly delete 
    $this->data->DeleteDosByObj($fobj);
    
    $urls = URL::GetURLSimple($this->gconf);
    $url = $urls->GetMgmtMethod('DosList');
    header("Location: ".$url);
  }

  function SignRemail($vars) {
    
    //$this->debug->Debug2("SignRemail",$vars);
    //exit(0);

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $fobj->_all_);

    $tpl->display("tpl/doc/content/signature-remail_content.html");
  }
    
  function SignRemail2($vars) {
    
    //$this->debug->Debug1("SignRemail2");
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);

    if ( $ra == 0 ) {
        $tpl = new Savant3();
        $tpl->assign("MSG", "Porte-documents invalide !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    $sobjs = $fobj->getSignatureObjects();
    $tpl = new Savant3();
    $urls = URL::GetURLByFolder($this->gconf, $fobj);

    // send all the mails,
	$tpl->assign("URL", $urls);
	$tpl->assign("DOSINFO", $fobj->_all_ );
	$tpl->assign("APPNAM", $this->gconf->name );

    $tmpfname = tempnam("/tmp", "CSXremail");
    $fm = fopen($tmpfname, "a");
    fwrite($fm,$tpl->fetch("tpl/doc/remail_sign.html"));
    fclose($fm);
    
    $msubj = "Rappel : Demande de signature : ".$fobj->title;

    foreach ( $sobjs as $sobj) {  // should be detached ??
        if ( ! $sobj->codeverified ) {     # status not relevant,  may be delayed
            $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$sobj->mail." \"".$msubj."\" ".$tmpfname;
            system(escapeshellcmd($launch));
        }
    }

    // at the end remove the mail file....
    unlink($tmpfname);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $url = $urls->GetDosMethod('Display');
    header("Location: ".$url);
  }

  function SignSMS($vars) {

    // called by javascript only, do not return html
    // messages after ERROR will be displayed in the browser, keep it in french !
      
    //$this->debug->Debug2("SignSMS", $vars);
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    if ( is_null($fobj) ) {
        echo "ERROR ID de dossier invalide";
        return;
    }

    $ra = $this->Authenticate($fobj);
    if ( $ra == 0 ) {
        echo "ERROR permissions insuffisantes, ceci ne devrait pas arriver";
        return;
    }

    $sigphone = $vars['TEL'];
    $sigmail = $vars['MEL'];
    $signame = $vars['NAM'];
    if ( empty($sigphone) or empty($sigmail) ) {
        echo "ERROR mail et/ou téléphone invalides";
        return;
    }
    // register given name and phone for later use if unauthenticated user
    $tmpdata = array("name"=>$signame, "phone"=>$sigphone);

    $sobj = Signature::getExisting($sigmail, $fobj, $this->gconf);
    if ( is_null($sobj) ) {
        echo "ERROR signataire inconnu ou non demandé";
        return;
    }
    $sobj->update($tmpdata); 
    
    $res = $sobj->SignSMS();
    if ( $res != 0 ) {
        echo "ERROR echec d'envoi du SMS";
    } else {
        echo "OK sms registered";
    }
  }

  function SignCreateSMS($vars) {

      // only used by SignAuto form,
      // as we already know the signer info ( signer = asker )
      // pre-create the signature data struct before sending the SMS

      $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
      if ( is_null($fobj) ) {
          echo "ERROR invalid ID";
          return;
      }

      $ra = $this->Authenticate($fobj);
      if ( $ra == 0 ) {
          echo "ERROR no rights, should not happen";
          return;
      }

      $tmpdata = array(
          "mail"=>$vars['MEL'],
          "phone"=>$vars['TEL'],
          "name"=>$vars['NAM']
      );
     
      Signature::create($tmpdata['mail'], $fobj, $this->gconf, $tmpdata);

      $this->SignSMS($vars);
 
  }

  function SignAccept($vars) {
    
    //$this->debug->Debug2("SignAccept", $vars);
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf, 1);
    $ra = $this->AuthenticateInner($fobj);
    if ( $ra == 0 ) {
        $tpl = new Savant3();
        $tpl->assign("MSG", "Porte-documents invalide !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    $signer = $vars['SIGNMEL'];
    if ( ! isset($signer) ) {                // should not happend
        $tpl = new Savant3();
        $tpl->assign("MSG", "Mail Signataire manquant !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    // fetch the signature object
    $sobj = Signature::getExisting($signer, $fobj, $this->gconf);
    if ( is_null($sobj) ) {
        $tpl = new Savant3();
        $tpl->assign("MSG", "Mauvaise adresse Mail Signataire !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    // extract the JRAL list
    $jral = Array();
    for ( $i=0; $i<count($vars['JRALN']); $i++ ) {
        if ( $vars['JRALV'][$i] == 1 ) {
            $jral[] = $vars['JRALN'][$i];
        }
    }

    // adjust the signature info  
    $params2 = array(
        "action" => "sign",
        "phone" => trim($vars['SIGNTEL']),
        "name" => trim($vars['SIGNNAM']),
        "signlist" => $jral,
        "refuselist" => array()
    );
    $sobj->update($params2);
    
    // update also the signature image
    $usigninfo = $this->data->fetchUserSignInfo($signer);
    $sobj->initSignImage($vars['SIGNIMG'], $usigninfo);
    
    $ret = $sobj->signatureTrigger($vars['SIGNCODE']);
    
    if ( $ret == "delayed" ) {
        $urls = URL::GetURLByFolder($this->gconf, $fobj);
        $url = $urls->GetDosMethod('Display',"PAGE=SignDelayed");
        header("Location: ".$url);
    } elseif ( $ret == "multiple" ) {
        $urls = URL::GetURLByFolder($this->gconf, $fobj);
        $url = $urls->GetDosMethod('Display',"PAGE=SignMultiple");
        header("Location: ".$url);
    } else {
        $urls = URL::GetURLByFolder($this->gconf, $fobj);
        $url = $urls->GetDosMethod('Display',"PAGE=DispBlog");
        header("Location: ".$url);
    }
  }

  function SignRefuse($vars) {
    
    //$this->debug->Debug1("SignRefuse");
    //exit(0);

      $fobj = Folder::getFromDid($vars['DID'], $this->gconf, 1);
    $ra = $this->AuthenticateInner($fobj);
    if ( $ra == 0 ) {
        $tpl = new Savant3();
        $tpl->assign("MSG", "Porte-documents invalide !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    $signer = $vars['SIGNMEL'];
    if ( ! isset($signer) ) {                // should not happend
        $tpl = new Savant3();
        $tpl->assign("MSG", "Mail Signataire manquant !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    // fetch the signature object
    $sobj = Signature::getExisting($signer, $fobj, $this->gconf);
    if ( is_null($sobj) ) {
        $tpl = new Savant3();
        $tpl->assign("MSG", "Mauvaise adresse Mail Signataire !");
        $urls = URL::GetURLSimple($this->gconf);
        $tpl->assign("URL", $urls);
        $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
        $tpl->display("tpl/doc/error2.html");
        return;
    }

    // adjust the signature info  
    $params2 = array(
        "action" => "refuse",
        "name" => trim($vars['SIGNNAM']),
        "comment" => $vars['SIGNCOM'],
        "signlist" => array(),
        "refuselist" => $fobj->filelist
    );
    $sobj->update($params2);

    $sobj->signatureTrigger(null);

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $url = $urls->GetDosMethod('Display',"PAGE=DispBlog");
    header("Location: ".$url);

  }

  function SignDelayed($vars) {
    
    //$this->debug->Debug1("SignDelayed");
    //exit(0);

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);
 
    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $fobj->_all_);

    $tpl->display("tpl/doc/part_signdelayed.html");

  }

  function SignMultiple($vars) {
    
    //$this->debug->Debug1("SignMultiple");
    //exit(0);

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);
 
    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $fobj->_all_);

    $tpl->display("tpl/doc/part_signmultiple.html");

  }

  function SignAuto($vars) {         //  to sign the folder for yourself,  no question asked !
    
    //$this->debug->Debug1("SignAUto");
    //exit(0);

    $tpl = new Savant3();

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    $ra = $this->AuthenticateInner($fobj);
 
    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $tpl->assign("URL", $urls);
    $tpl->assign("DOSINFO", $fobj->_all_);

    @session_start();
    if ( isset($_SESSION['AUTHENTICATED']) ) {
        $uinfo = $this->data->UserInfo($_SESSION['AUTHENTICATED'], true);
        $tpl->assign("OWNNAME", $uinfo['gvname']." ".$uinfo['name'] );
        $tpl->assign("OWNMAIL", $uinfo['mail'] );
        if ( isset($uinfo['signphone']) ) {
            $tpl->assign("OWNPHONE", $uinfo['signphone'] );
            $tpl->assign("SIGNINFOK", 1);
        } else {
            $tpl->assign("OWNPHONE", "" );
            $tpl->assign("SIGNINFOK", 0);
        }
        if ( isset($uinfo['imageSignature']) and $uinfo['imageSignature'] != "" ) {
            $idata = "data:".$uinfo['mimetype'].";base64,".$uinfo['imageSignature'];
            $tpl->assign("SIGNIMAGE", $idata);
        } else {
            $tpl->assign("SIGNIMAGE", "");
        }
        
    } else {
        $tpl->assign("OWNNAME", "" );
        $tpl->assign("OWNMAIL", "" );
        $tpl->assign("OWNPHONE", "" );
        $tpl->assign("SIGNINFOK", 0);
        $tpl->assign("SIGNIMAGE", "");
   }
 
    $tpl->display("tpl/doc/content/auto-sign_content.html");

  }

  function SignAuto2($vars) {        
    
    //$this->debug->Debug1("SignAuto2");
    //exit(0);

    // fetch folder with file to sign list 
    $fobj = Folder::getFromDid($vars['DID'], $this->gconf, 2);

    if ( is_null($fobj) ) {
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/doc/invalid.html");
      return;
    }

    // signauto has only one signer !
    $signerlist = array($vars['SIGNMEL']);

    // prepare signature request parameters ( signasker = signer )
    $params = array(
        "signercount" => 1,
        "signasker" => trim($vars['SIGNMEL'])
    );
    // and trigger the signature request 
    $this->data->SignRequest($fobj, $params, $signerlist);

    // adjust the signature info  ( retreive the signature created by SignRequest )
    // for autosign,  no file-list was asked, register the real list ( already fetched )
    $sobj = Signature::getExisting($vars['SIGNMEL'], $fobj, $this->gconf);
    $params2 = array(
        "action" => "autosign",
        "phone" => trim($vars['SIGNTEL']),
        "name" => trim($vars['SIGNNAM']),
        "signlist" => $fobj->filelist,
        "refuselist" => array()
    );
    if ( is_null($sobj) ) {
        die("ERREUR TYPE : shae1lee4TieV6diechaey5koh7Sheth, veuillez prévenir Gironde Numerique");
    } else {
        $sobj->update($params2);
    }
       
    // update also the signature image
    $usigninfo = $this->data->fetchUserSignInfo($vars['SIGNMEL']);
    $sobj->initSignImage($vars['SIGNIMG'], $usigninfo);
    
    $ret = $sobj->signatureTrigger($vars['SIGNCODE']);
    
    if ( $ret == "delayed" ) {
        $urls = URL::GetURLByFolder($this->gconf, $fobj);
        $url = $urls->GetDosMethod('Display',"PAGE=SignDelayed");
        header("Location: ".$url);
    } else {
        $urls = URL::GetURLByFolder($this->gconf, $fobj);
        $url = $urls->GetDosMethod('Display',"PAGE=DispBlog");
        header("Location: ".$url);
    }

    
  }
    
  //===============================================
  // tools
  //===============================================

  private function Authenticate($foldobj) {

    $ra = $this->data->CheckAuth($foldobj);

    if ( $ra == ACCES_NO ) {

      $tpl = new Savant3();

      $urls = URL::GetURLByFolder($this->gconf, $foldobj);
      $tpl->assign("URL", $urls);
      $tpl->assign("DID", $foldobj->did);
      $tpl->assign("APPNAM", $this->gconf->name);
      $tpl->assign("FAVICO", $this->gconf->favico);

      $tpl->display("tpl/doc/askpass.html");

      exit(0);
    }

    return($ra);

  }    

  private function AuthenticateInner($foldobj) {

    $ra = $this->data->CheckAuth($foldobj);

    if ( $ra == ACCES_NO ) {

      $tpl = new Savant3();

      $urls = URL::GetURLByFolder($this->gconf, $foldobj);
      $tpl->assign("URL", $urls);
      $tpl->assign("DID", $foldobj->did);

      $tpl->display("tpl/doc/part_auth.html");
      
      exit(0);
    }

    return($ra);

  }    

  //===============================================
  // end
  //===============================================

}

