<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/lib//Savant3/Savant3.php";
include_once $topdir."/app/src/class.Data.php";
include_once $topdir."/app/src/class.Folder.php";
include_once $topdir."/app/src/class.User.php";
include_once $topdir."/app/src/class.Contacts.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.Debug.php";
include_once $topdir."/app/src/lib.LowLevelUtils.php";
include_once $topdir."/app/src/class.Signature.php";

class Mgmt {

  //===============================================
  // framework
  //===============================================

  var $public_functions;
  var $gconf;
  protected $contacts;

  function __construct($conf) { 
    $this->gconf = $conf;
    $this->data = new Data($conf);
    $this->contacts = new Contacts($conf);
    $this->debug = new Debug();
    $this->topdir = dirname(dirname(__DIR__));

    if ( $this->gconf->sso == "openidc" ) {
        include_once $this->topdir."/app/src/class.OpenIDcClient.php";
        $this->ssoc = new OpenIDcClient($this->gconf, $this->data);
    }
  }

  //===============================================
  // web app
  //===============================================

  function Index($vars) {

      $this->HomePage(0);    // no longer redirecting to DosList 
  }

  function Apropos($vars) {

      $this->HomePage(0);
  }

  private function HomePage($delay) {


      $tpl = new Savant3();
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("APPNAM", $this->gconf->name);
      $tpl->assign("DELAY", $delay);

      $install_status = $this->data->VerifyInstall();
      if ( $install_status['global'] ) {
          $tpl->display("tpl/mgmt/index_sig.html");
      } else {
          $tpl->assign("INSTALLSTATUS", $install_status);
          $tpl->display("tpl/mgmt/no_installed.html");
      }
  }

  function MgtCreate($vars) {

    $this->AskAuth();

    $tpl = new Savant3();
    $fobj = $this->data->PreCreateDos();

    if ( is_null($fobj) ) {
      $tpl->assign("MSG", "Pre-Create failure, contact administrator !");
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/doc/error2.html");
      return;
    }

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);
    $urls = URL::GetURLByFolder($this->gconf, $fobj);

    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);
    $tpl->assign("DID", $fobj->did);
    $tpl->assign("DOSINF", $fobj->_all_);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);

    $utyp = $this->data->CurrentUserStatus();
    $datinf = $this->data->GetDateInfo($utyp);

    $tpl->assign("DATESEL", $datinf['datesel'] );

    $tpl->display("tpl/mgmt/part_mgtcreate.html");

  }
  
  function CreateJQFU($vars) {

    //$this->debug->DebugToFile("/tmp/cre_jqfu_deb.log", $vars);
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    if ( ! is_null($fobj) ) {

      $ret = $fobj->AddJQFUFiles($vars);
    
      $tmp = $vars['files'];
      $finfo['name'] = $tmp['name'][0];
      $finfo['type'] = $tmp['type'][0];
      $finfo['tmp_name'] = $tmp['tmp_name'][0];
      $finfo['error'] = $tmp['error'][0];
      $finfo['size'] = $tmp['size'][0];

      // jquery file upload need a json return 
      header('Content-type: application/json');
      $tpl = new Savant3();
      $tpl->assign("NAME", $finfo['name']);
      $tpl->assign("TYPE", $finfo['type']);
      $tpl->assign("SIZE", $finfo['size']);
      $tpl->display("tpl/doc/jqfu_ret.json");
    }

  }

  function FinishCreate($vars) {

    //$this->debug->Debug1("FinishCreate");
    //exit(0);

    if ( ! isset($vars['DID']) ) {
      $tpl = new Savant3();
      $tpl->assign("MSG", "Post-Create failure, DID missing, contact administrator !");
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/doc/error2.html");
      return;
    }

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);

    $uinfo = $this->data->UserInfo(@$_SERVER['PHP_AUTH_USER']);

    // prepare data
    $finaldata = array(
        "title" => @$vars['DOSNAM'],
        "comment" => @$vars['DOSCOM'],
        "passwd" => @$vars['DOSPSW'],
        "endoflife" => @$vars['DOSLIM']
    );
    $fobj->update($finaldata);
    
    if ( isset($vars['RUSERI']) and isset($vars['RUSERM']) ) {
      // created by registered user -> attach it writer mode
      $this->data->DosAttach($fobj->did, $vars['RUSERI'], 'writer');
    }

    // after created -> display new doc

    // set auth before redirecting to the doc
    $this->data->UpdateAuth($fobj->did, $fobj->passwd); 
    
    // fetch full info to see if files present
    $fobj->getFiles(1);
      
    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $gourl = $urls->GetDosMethod('Display');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);

  }

  function UnCreate($vars) {

    if ( isset($vars['DID']) and $vars['DID'] != "" ) {
      $this->data->DeleteDosByDid($vars['DID']);
    }
    
    $urls = URL::GetURLSimple($this->gconf);
    $url = $urls->GetMgmtMethod('Index' );
  
    header("Location: ".$url);
  }

  function UnCreateMgt($vars) {

    if ( isset($vars['DID']) and $vars['DID'] != "" ) {
      $this->data->DeleteDosByDid($vars['DID']);
    }
    echo "DONE";
  }

  function UnCreatePgt($vars) {

    if ( isset($vars['DID']) and $vars['DID'] != "" ) {
      $this->data->DeleteDosByDid($vars['DID']);
    }
    
    $urls = URL::GetURLSimple($this->gconf);
    $url = $urls->GetMgmtMethod('DosList' );
  
    header("Location: ".$url);
  }

  function DosList($vars) {

    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);

    $dcnt = $this->data->GetDosSignStatusCountByUser($uinfo['mail']);

    if ( $uinfo['status'] == "admin" ) {
        header("Location: DosAll");
    } else {
        if ( isset($dcnt['asked']) and $dcnt['asked'] >= 1 ) {
            header("Location: Doc2Sign");
        } else {
            header("Location: DosWork");
        }
    }        

  }

  function DosAll($vars) {

    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);

    // echo "<pre>\n";
    // print_r($uinfo);
    // echo "</pre>\n";
    // exit(0);

    // management RAZ the std/sign mode
    @session_start();
    unset($_SESSION['MODE']);
   
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $listtype = "all";
    
    $dlist = $this->data->GetDosListByUser($uinfo['mail'],$listtype);
    
    $dcnt = $this->data->GetDosSignStatusCountByUser($uinfo['mail']);
    $tpl->assign("DCNT", $dcnt);

    //echo "<pre>\n";
    //print_r($dlist);
    //echo "</pre>\n";
    //exit(0);
   
    $tpl->assign("TABALL", 1);      // force display off tab "All"
    $tpl->assign("DLIST", $dlist);
    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);
    $tpl->assign('SFILTR', $this->GetSavedFilters() );
    $tmp = $this->GetSavedSort();
    $tpl->assign('SVSORT', $tmp['MD'] );

    $utyp = $this->data->CurrentUserStatus();
    $datinf = $this->data->GetDateInfo($utyp);

    $tpl->assign("EOL", date("Y-m-d", $datinf['datelim']) );
    $tpl->assign("DATESEL", $datinf['datesel'] );
    $tpl->assign("ASKER", $_SERVER['PHP_AUTH_USER'] );

    if ( @$uinfo['listmax'] != 0 ) {
        $tpl->assign("MAXLIST", 1 );
    } else {
        $tpl->assign("MAXLIST", 0 );
    }      
    
    $tpl->display("tpl/mgmt/doslist_sig.html");
  }

  function DosWork($vars) {

    $this->AskAuth();
    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);
    $contacts = $this->contacts->getUserContacts($_SERVER['PHP_AUTH_USER']);

    //echo "<pre>\n";
    //print_r($uinfo);
    //echo "</pre>\n";
    //exit(0);
    
    // management RAZ the std/sign mode
    @session_start();
    unset($_SESSION['MODE']);
   
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $listtype = "all";
    
    $dlist = $this->data->GetDosListByUserBySignStatus($uinfo['mail'], array("draft"));

    $dcnt = $this->data->GetDosSignStatusCountByUser($uinfo['mail']);
    $tpl->assign("DCNT", $dcnt);
    //echo "<pre>\n";
    //print_r($dlist);
    //echo "</pre>\n";
    //exit(0);

    $tpl->assign("NOTYPFILT", 1);

    $tpl->assign("DLIST", $dlist);
    if ( $uinfo['status'] == "admin" ) {
        $tpl->assign("TABALL", 1);
    } else {
        $tpl->assign("TABALL", 0);
    }

    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);
    $tpl->assign('SFILTR', $this->GetSavedFilters() );
    $tmp = $this->GetSavedSort();
    $tpl->assign('SVSORT', $tmp['MD'] );

    $utyp = $this->data->CurrentUserStatus();
    $datinf = $this->data->GetDateInfo($utyp);

    $tpl->assign("EOL", date("Y-m-d", $datinf['datelim']) );
    $tpl->assign("DATESEL", $datinf['datesel'] );
    $tpl->assign("ASKER", $_SERVER['PHP_AUTH_USER'] );
    $tpl->assign("UCONTACTS", $contacts);

    if ( @$uinfo['listmax'] != 0 ) {
        $tpl->assign("MAXLIST", 1 );
    } else {
        $tpl->assign("MAXLIST", 0 );
    }      
    
    //$tpl->display("tpl/mgmt/doswork_sig.html");
    $tpl->display("tpl/mgmt/doswork_sig_dsfr.html");
  
  }

  function DosPending($vars) {

    $this->AskAuth();
    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);

    //echo "<pre>\n";
    //print_r($uinfo);
    //echo "</pre>\n";
    //exit(0);
    
    // management RAZ the std/sign mode
    @session_start();
    unset($_SESSION['MODE']);
   
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $listtype = "all";
    
    $dlist = $this->data->GetDosListByUserBySignStatus($uinfo['mail'], array("asked-pending"));

    $dcnt = $this->data->GetDosSignStatusCountByUser($uinfo['mail']);
    $tpl->assign("DCNT", $dcnt);
    //echo "<pre>\n";
    //print_r($dlist);
    //echo "</pre>\n";
    //exit(0);

    $tpl->assign("NOTYPFILT", 1);

    $tpl->assign("DLIST", $dlist);
    if ( $uinfo['status'] == "admin" ) {
        $tpl->assign("TABALL", 1);
    } else {
        $tpl->assign("TABALL", 0);
    }

    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);
    $tpl->assign('SFILTR', $this->GetSavedFilters() );
    $tmp = $this->GetSavedSort();
    $tpl->assign('SVSORT', $tmp['MD'] );

    $utyp = $this->data->CurrentUserStatus();
    $datinf = $this->data->GetDateInfo($utyp);

    $tpl->assign("EOL", date("Y-m-d", $datinf['datelim']) );
    $tpl->assign("DATESEL", $datinf['datesel'] );
    $tpl->assign("ASKER", $_SERVER['PHP_AUTH_USER'] );

    if ( @$uinfo['listmax'] != 0 ) {
        $tpl->assign("MAXLIST", 1 );
    } else {
        $tpl->assign("MAXLIST", 0 );
    }      
    
    $tpl->display("tpl/mgmt/dospending_sig_dsfr.html");
  
  }

  function DosEnded($vars) {

    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);

    //echo "<pre>\n";
    //print_r($uinfo);
    //echo "</pre>\n";
    //exit(0);
    
    // management RAZ the std/sign mode
    @session_start();
    unset($_SESSION['MODE']);
   
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);
    $tpl->assign('SFILTR', $this->GetSavedFilters() );
    $tmp = $this->GetSavedSort();
    $tpl->assign('SVSORT', $tmp['MD'] );

    $elist = $this->data->GetDosListByUserBySignStatus($_SERVER['PHP_AUTH_USER'], array("done","refused"));

    $dcnt = $this->data->GetDosSignStatusCountByUser($uinfo['mail']);
    $tpl->assign("DCNT", $dcnt);
    $tpl->assign("DLIST", $elist);

    if ( $uinfo['status'] == "admin" ) {
        $tpl->assign("TABALL", 1);
    } else {
       $tpl->assign("TABALL", 0);
    }


    $tpl->assign("ELIST", $elist);

    //$tpl->display("tpl/mgmt/dosended_sig.html");
    $tpl->display("tpl/mgmt/dosended_sig_dsfr.html");
  }

  function View($vars) {

    $this->AskAuth();
 
    $verif = $this->data->VerifyDosAttach($vars['DID'], $_SERVER['PHP_AUTH_USER']);
    if ( $verif == 0 ) {
      $tpl = new Savant3();
      $tpl->assign("MSG", "Ce porte-documents ne vous est pas attaché !");
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/mgmt/error2.html");
      exit(0);
   }

    //$this->debug->Debug2("Mgmt_View",$vars);
    //exit(0);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);

    //$this->debug->Debug2("Mgmt_View",$pinf);
    //exit(0);

     // set auth before redirecting to the doc
    $this->data->UpdateAuth($fobj->did, $fobj->passwd); 

    $urls = URL::GetURLByFolder($this->gconf, $fobj);
    $gourl = $urls->GetDosMethod('Display');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);
  }

  function Detach($vars) {

    $this->AskAuth();
    
    $verif = $this->data->VerifyDosAttach($vars['DID'], $_SERVER['PHP_AUTH_USER']);
    if ( $verif == 0 ) {
      $tpl = new Savant3();
      $tpl->assign("MSG", "Ce porte-documents ne vous est pas attaché !");
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/mgmt/error2.html");
      exit(0);
    }
    $this->data->DosDetach($vars['DID'], $vars['RUSERI']); 

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);

  }


  function Delete($vars) {

    $this->AskAuth();
 
    $verif = $this->data->VerifyDosAttach($vars['DID'], $_SERVER['PHP_AUTH_USER']);
    if ( $verif == 0 ) {
      $tpl = new Savant3();
      $tpl->assign("MSG", "Ce porte-documents ne vous est pas attaché !");
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/mgmt/error2.html");
      exit(0);
    }

    $this->data->DeleteDosByDid($vars['DID']); 

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);


  }

  function MultiDetach($vars) {

    $this->AskAuth();
    //echo "<pre>\n";
    //print_r($vars);
    //echo "</pre>\n";
    //exit(0);

    $dlist = explode(" ",trim($vars['DETACHLIST']));
    foreach( $dlist as $did ) {
        $this->data->DosDetach($did, $vars['RUSERI']);
    }

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosEnded');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);

  }


  function MultiDelete($vars) {

    $this->AskAuth();
    //echo "<pre>\n";
    //print_r($vars);
    //echo "</pre>\n";
    //exit(0);
 
    $dlist = explode(" ",trim($vars['DELETELIST']));
    foreach( $dlist as $did ) {
        $this->data->DeleteDosByDid($did);
    }

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosEnded');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);

  }

  function MultiDownload($vars) {

    $this->AskAuth();
    // echo "<pre>\n";
    // print_r($vars);
    // echo "</pre>\n";
    // exit(0);
 
    $dlist = explode(" ",trim($vars['DOWNLOADLIST']));
    // there is a lot of whitespace in DOWNLOADLIST
    // filter the list
    $filteredlist = array();
    foreach($dlist as $elem) {
        if ( strlen($elem) >= 32 ) {
            $filteredlist[] = $elem;
        }
    }
    $this->data->MultiFoldersZip($filteredlist);

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosEnded');
    header("Location: ".$gourl);

  }

  function Account($vars) {

    //$this->debug->Debug2("Account", $vars);
    //exit(0);

    $this->AskAuth();
 
    $udata=Array();
    $udata['mail'] = $vars['ACTMAIL'];
    $udata['gvname'] = $vars['ACTGVN'];
    $udata['name'] = $vars['ACTNAM'];
    if ( @$vars['LISTMAX'] == "on" ) {
        $udata['listmax'] = 1;
    } else {
        $udata['listmax'] = 0;
    }
    
    $res = $this->data->UpdateUserInfo( $vars['RUSERI'], $udata);

    if ( !empty( $vars['ACTPSW1']) && !empty($vars['ACTPSW2']) and $vars['ACTPSW1'] == $vars['ACTPSW2'] ) {
      $res = $this->data->UpdateUserPassword( $vars['RUSERI'], $vars['ACTPSW1']);
    }

    if ( isset($vars['DELLOGO']) ) {
      $this->data->UpdateUserLogo( $vars['RUSERI'], NULL);
    } else { 
      if ( @$vars['ACTLOGO']['tmp_name'] != "" ) {
	    $res = $this->data->UpdateUserLogo( $vars['RUSERI'], $vars['ACTLOGO']);
      }
    }

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);

  }

  function PgtAccount($vars) {

    $tpl = new Savant3();
    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);
    $urls = URL::GetURLSimple($this->gconf);

    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);
    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);

    $tpl->display("tpl/mgmt/pg_sig_account.html");

  }


  function AccountPage($vars) {

    $tpl = new Savant3();
    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);
    $urls = URL::GetURLSimple($this->gconf);

    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);
    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);

    $tpl->display("tpl/mgmt/account_page.html");

  }
  
  function Attach($vars) {

    //$this->debug->Debug1("Attach");
    //exit(0);

    $this->AskAuth();
    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);

	if ( ! isset($vars['DID']) or ! isset($vars['MODE']) ) {
      $tpl = new Savant3();
      $tpl->assign("MSG", "Porte Document inconnu !");
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->display("tpl/mgmt/error2.html");
      exit(0);
    }

    switch($vars['MODE']) {
    case ACCES_RW:
      $mode = 'writer';
      break;
    case ACCES_RO:
      $mode = 'reader';
      break;
    }
 
    $this->data->DosAttach($vars['DID'], $uinfo['id'], $mode);
   
    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);

  }

  function LogOff(){
      
      $urls = URL::GetURLSimple($this->gconf);
      $gourl = $urls->GetAbsMgmtMethod('Index');

      @session_start();
      $_SESSION['LOGOFF']=@$_SERVER['PHP_AUTH_USER'];
          
      header("Location: ".$gourl);
  }
  
  //#######################################################
  // curl revproxy for the inner signator-xxx GetPub
  //
  function GetPubSign($vars){

      //$this->debug->Debug2("getpubsign",$vars);
      //exit(0);

      // get the signature class
      $sclass = "Signature_WS";    // no more config !
      $fclass = "./app/class.".$sclass.".php";
      if ( file_exists($fclass) ) {
          include_once $fclass;
          if ( isset($vars['SIGNENGINE']) ) {
              $wso = new $sclass($vars['SIGNENGINE']);
          } else {
              $wso = new $sclass();
          }
      } else {
           return("BADDATA");
      }

      $wsurl = $wso->GetPubUrl();

      // step 1 get the header
      $params = array(
          CURLOPT_URL => $wsurl,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HEADER => 1,
          CURLOPT_NOBODY => 1
      );
      
      $ch = curl_init();
      curl_setopt_array($ch, $params);
      
      $headers = curl_exec($ch);
      curl_close($ch);

      // step 2 re-pass some headers
      $hdrar = explode("\n",$headers);
      foreach ($hdrar as $hdr) {
          if ( strncmp("HTTP", $hdr, 4) == 0 ) continue;
          if ( strncmp("Date", $hdr, 4) == 0 ) continue;
          if ( strncmp("Server", $hdr, 6) == 0 ) continue;
          if ( strncmp("MIME", $hdr, 4) == 0 ) continue;
          header($hdr);
      }
      
      // step 3 passthru the url
      $swsfd = fopen($wsurl, "r");
      fpassthru($swsfd);

  }
  
  //===============================================
  // account creation stuff
  //===============================================

  function CreAcc1($vars) {

    //$this->debug->Debug1("CreAcc1");
    //exit(0);

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $tpl->display("tpl/mgmt/acccre1.html");

  }

  function CreAcc2($vars) {

    //$this->debug->Debug1("CreAcc2");
    //exit(0);

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    if ( ! isset($vars['ACCMAIL']) ) {
      $tpl->assign("ERR", "NOINFO"); 
      $tpl->display("tpl/mgmt/acccre2.html");
      return;
    }

    if ( $vars['ACCPASS'] != $vars['ACCPAS2'] ) {
      $tpl->assign("ERR", "PASS"); 
      $tpl->assign("GVN", $vars['ACCGVN']); 
      $tpl->assign("NAM", $vars['ACCNAME']); 
      $tpl->assign("MEL", $vars['ACCMAIL']); 
      $tpl->display("tpl/mgmt/acccre2.html");
      return;
    }

    $udata=Array();
    $udata['mail'] = $vars['ACCMAIL'];
    if ( isset($vars['ACCGVN']) && $vars['ACCGVN'] != "" ) {
        $udata['gvname'] = $vars['ACCGVN'];
    } else {
        $udata['gvname'] = "-";
    }
    if ( isset($vars['ACCNAME']) && $vars['ACCNAME'] != "" ) {
        $udata['name'] = $vars['ACCNAME'];
    } else {
        $udata['name'] = "-";
    }
    $udata['password'] = $vars['ACCPASS'];
    
    $uid = $this->data->CreateUserRequest($udata);
    if ( $uid == 0 ) {
      $tpl->assign("ERR", "LOGIN"); 
      $tpl->assign("GVN", $udata['gvname']); 
      $tpl->assign("NAM", $udata['name']); 
      $tpl->assign("MEL", $udata['mail']); 
      $tpl->display("tpl/mgmt/acccre2.html");
      return;
   }

    $tpl->assign("GVN", $udata['gvname']); 
    $tpl->assign("NAM", $udata['name']); 
    $tpl->assign("MEL", $udata['mail']); 

    $tpl->display("tpl/mgmt/acccre2.html");

    // send the mail for validation account 

    $mskid = sprintf("%s_%06d",GetRandomString(5),$uid);
    $tpl->assign("RANDID", base64_encode($mskid) ); 

    $tmpfname = tempnam("/tmp", "CSXmelAcre");
    $fm = fopen($tmpfname, "a");
    fwrite($fm,$tpl->fetch("tpl/mgmt/mail_creacc.html"));
    fclose($fm);
 
    $launch=$this->gconf->TopAppDir."/app/script/melsnd ".$udata['mail']." \"PoDoc - Création de compte\" ".$tmpfname;
    system(escapeshellcmd($launch));
 

  }

  function CreAcc3($vars) {

    //$this->debug->Debug2("CreAcc3",$vars);
    //exit(0);

    $udec1 = base64_decode($vars['UCODE']);
    preg_match('/.*_([0-9]*)/', $udec1, $arres);
    $uid0 = $arres[1];
    $uid = (Int)$uid0;

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $ret = $this->data->ValidateUser($uid);
    
    switch($ret) {
    case 0:
      $tpl->assign("ERR", "NONE");
      $uinfo =  $this->data->UserInfoByUid($uid);
      $tpl->assign("UINFO", $uinfo); 
      $tpl->display("tpl/mgmt/acccre3.html");
      break;
    case 1:
      $tpl->assign("ERR", "ALREADY"); 
      $uinfo =  $this->data->UserInfoByUid($uid);
      $tpl->assign("UINFO", $uinfo); 
      $tpl->display("tpl/mgmt/acccre3.html");
      break;
    case 2:
      $tpl->assign("ERR", "NOUSER"); 
      $tpl->display("tpl/mgmt/acccre2.html");
      break;
    case 3:
      $tpl->assign("ERR", "UNKNOWN"); 
      $tpl->display("tpl/mgmt/acccre2.html");
      break;
    }

  }

  function LostPassword($vars) {

    //$this->debug->Debug1("LostPassword");
    //exit(0);

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $tpl->display("tpl/mgmt/lostpass.html");

  }

  function ResetPassword($vars) {

    //$this->debug->Debug1("ResetPassword");
    //exit(0);

    $ret = $this->data->ResetPassword($vars['ACCMAIL']);

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("LANGSET", $this->gconf->LangSet);

    $tpl->display("tpl/mgmt/index.html");

    if ( isset($ret['newpasswd'] ) ) {
 
      // send the mail with new passwd 

      $tpl->assign("GVN", $ret['gvname']); 
      $tpl->assign("NAM", $ret['name']); 
      $tpl->assign("NEWPASS", $ret['newpasswd']); 
      
      $tmpfname = tempnam("/tmp", "CSXmelPass");
      $fm = fopen($tmpfname, "a");
      fwrite($fm,$tpl->fetch("tpl/mgmt/mail_passwd.html"));
      fclose($fm);
      
      $launch=$this->gconf->TopAppDir."/app/script/melsnd ".$ret['mail']." \"PoDoc - Nouveau mot de passe\" ".$tmpfname;
      system(escapeshellcmd($launch));
    }
 

  }

  //===============================================
  // filter stuff
  //===============================================

  function SortSave($vars) {

      @session_start();
      if ( ! empty($vars['AD']) ) {
          $_SESSION['SRT_AD']=$vars['AD'];
      }
      if ( ! empty($vars['AU']) ) {
          $_SESSION['SRT_AU']=$vars['AU'];
      }
       if ( ! empty($vars['MD']) ) {
          $_SESSION['SRT_MD']=$vars['MD'];
      }
     
      return;
  }

  function FilterSave($vars) {

      //$this->debug->DebugToFile("/tmp/deb.txt", $vars);

      @session_start();
      $_SESSION['FILTR_D']=$vars['FD'];
      $_SESSION['FILTR_M']=$vars['FM'];
      $_SESSION['FILTR_E']=$vars['FE'];
      $_SESSION['FILTR_S']=$vars['FS'];
      
      return;
  }

  private function GetSavedSort() {
      // init default sort
      $svsort = Array("AD"=>"0,0","AU"=>"0,0","MD"=>"1,0");
    
      @session_start();
      if ( isset($_SESSION['SRT_AD']) and $_SESSION['SRT_AD']!= "" ) {
          $svsort['AD']=$_SESSION['SRT_AD'];
      }
      if ( isset($_SESSION['SRT_AU']) and $_SESSION['SRT_AU']!= "" ) {
          $svsort['AU']=$_SESSION['SRT_AU'];
      }
      if ( isset($_SESSION['SRT_MD']) and $_SESSION['SRT_MD']!= "" ) {
          $svsort['MD']=$_SESSION['SRT_MD'];
      }
      return($svsort);

  }

  private function GetSavedFilters() {
    @session_start();
    $sfiltr = Array();
    
    if ( isset($_SESSION['FILTR_D']) and $_SESSION['FILTR_D']!= "" ) {
      $sfiltr['FD']=$_SESSION['FILTR_D'];
    }
    if ( isset($_SESSION['FILTR_M']) and $_SESSION['FILTR_M']!= "" ) {
      $sfiltr['FM']=$_SESSION['FILTR_M'];
    }
    if ( isset($_SESSION['FILTR_E']) and $_SESSION['FILTR_E']!= "" ) {
      $sfiltr['FE']=$_SESSION['FILTR_E'];
    }
    if ( isset($_SESSION['FILTR_S']) and $_SESSION['FILTR_S']!= "" ) {
      $sfiltr['FS']=$_SESSION['FILTR_S'];
    }
    return($sfiltr);

  }

  //===============================================
  // auth stuff
  //===============================================


  private function AskAuth() {
      switch($this->gconf->sso) {
      case "none":
          $this->AskAuthStd();
          break;
      case "openidc":
          $this->AskAuthSso();
          break;
      }
  }

  private function AskAuthSso(){

      $udata = $this->ssoc->VerifSsoUser("std");

      // kludge !! , init PHP_AUTH_USER    probably useless
      $_SERVER['PHP_AUTH_USER'] = $udata['user'];

      // useless now, already done in SSO client
      //@session_start();
      //$_SESSION['AUTHENTICATED'] = $udata['user'];
  }
  
  private function AskAuthStd(){

      $realm = $this->gconf->name;
   
      // demande d'identification
      if ( !isset($_SERVER['PHP_AUTH_USER']) ) {
          $this->Authenticate($realm);
      } else {
          @session_start();
          if ( $_SERVER['PHP_AUTH_USER'] == @$_SESSION['LOGOFF'] ) {
              unset($_SESSION['AUTHENTICATED']);
              unset($_SESSION['LOGOFF']);
              $this->Authenticate($realm);
          }
          $uok = $this->data->VerifUser($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
          if ( ! $uok ) {
              $this->Authenticate($realm);
          }
          $_SESSION['AUTHENTICATED']=$_SERVER['PHP_AUTH_USER'];
      }
  }
  
  private function Authenticate($realm){
      Header("status: 401 Unauthorized"); 
      Header("WWW-Authenticate: Basic realm=\"$realm\" ");
      Header("HTTP/1.0 401 Unauthorized");
      
      // display if authentication canceled
      $tpl = new Savant3();
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("APPNAM", $this->gconf->name);
      
      $tpl->display("tpl/mgmt/unauth_sig_dsfr.html");
      
      exit;
  }

  function NoAuth($vars){

      // display if  authentication failed
      $tpl = new Savant3();
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("APPNAM", $this->gconf->name);
      
      $tpl->assign("RURL", $urls->GetMgmtMethod('Index'));
      $tpl->assign("MSG",  "Vous n'avez pas les droits nécéssaires pour accèder à cette partie de l'application !");

      $tpl->display("tpl/mgmt/error2.html");
      exit;
  }

  function contactsActions($vars) {

    //$this->debug->Debug2("Account", $vars);
    //exit(0);

    $this->AskAuth();
 
    $aa = $vars;
    $uid = $vars['RUSERI'];
    
    switch($vars['CONTACTS_ACTION']) {
      case 'DEL':         
        if(empty($vars['CONTACTS_DEL_MAIL'])) {
            break;
        }
        $this->contacts->deleteUserContact($uid, $vars['CONTACTS_DEL_MAIL']);
        break;

      case 'ADD':
        if(empty($vars['CONTACTS_ADD_MAIL']) || empty($vars['CONTACTS_ADD_NAME'])) {
            break;
        }

        $this->contacts->addUserContact($uid, $vars['CONTACTS_ADD_MAIL'], $vars['CONTACTS_ADD_NAME']);
        break;
      default:

        break;        
    }

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('ContactsPage');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);

  }

  function ContactsPage($vars) {

    $tpl = new Savant3();
    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);
    $contacts = $this->contacts->getUserContacts($_SERVER['PHP_AUTH_USER']);
    $urls = URL::GetURLSimple($this->gconf);

    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);
    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);
    $tpl->assign("UCONTACTS", $contacts);

    $tpl->display("tpl/mgmt/contacts_page.html");

  }

  function ajaxContactsList() {

    $this->AskAuth();
    
    echo $this->contacts->getUserContactsForBootstrapTableAjax($_SERVER['PHP_AUTH_USER']);
  }

  //===============================================
  // CGU
  //===============================================

  function CGU($vars) {


      $tpl = new Savant3();
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);
      $tpl->assign("APPNAM", $this->gconf->name);
      $tpl->assign("LANGSET", $this->gconf->LangSet);
      
      $tpl->display("tpl/mgmt/cgu_sig_dsfr.html");
  }

  //===============================================
  // multi-doc sign
  //===============================================

  function Doc2Sign($vars) {

    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER'], true);

    // echo "<pre>\n";
    // print_r($uinfo);
    // echo "</pre>\n";
    // exit(0);
    
    // management RAZ the std/sign mode
    @session_start();
    unset($_SESSION['MODE']);
   
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $listtype = "all";
    
    $f2slist = $this->data->GetMultiFolderList2Sign($uinfo['mail']);

    $dcnt = $this->data->GetDosSignStatusCountByUser($uinfo['mail']);
    $tpl->assign("DCNT", $dcnt);
   
    $tpl->assign("F2SLIST", $f2slist);
    $tpl->assign("MYSELF", $uinfo['gvname']." ".$uinfo['name']);
    $tpl->assign("RUSERI", $uinfo['id']);
    $tpl->assign("RUSERM", $uinfo['mail']);
    $tpl->assign("UINFO", $uinfo);

    $utyp = $this->data->CurrentUserStatus();
    $datinf = $this->data->GetDateInfo($utyp);

    // Permet d'afficher le premier document de la list.
    foreach ($f2slist as $dosKey => $dosInfo) {
      if(!empty($dosInfo['filelist'])) {
        $firstFile = reset($dosInfo['filelist']);
        $dosUrls = URL::GetURLByInfo($this->gconf, $dosInfo);
        $dosUrls->InitContentDisplayParameters();
        $tpl->assign("CONTURL",  $dosUrls->GetDosData($firstFile));
        break;
      }
    }
    
    if ( $uinfo['status'] == "admin" ) {
        $tpl->assign("TABALL", 1);
    } else {
        $tpl->assign("TABALL", 0);
    }

    $tpl->assign("EOL", date("Y-m-d", $datinf['datelim']) );
    $tpl->assign("DATESEL", $datinf['datesel'] );

    // $tpl->display("tpl/mgmt/dos2sign_sig.html");
    $tpl->display("tpl/mgmt/dos2sign_sig_dsfr.html");
  }

  function Doc2SignThumbs($vars) {

    $this->AskAuth();

    $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER']);

    //echo "<pre>\n";
    //print_r($uinfo);
    //echo "</pre>\n";
    //exit(0);
    
    // management RAZ the std/sign mode
    @session_start();
    unset($_SESSION['MODE']);
   
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);

    $f2slist = $this->data->GetMultiFolderList2Sign($uinfo['mail']);
   
    $tpl->assign("F2SLIST", $f2slist);

    $tpl->display("tpl/mgmt/multisign_thumbs.html");
  }

  function MultiActionOnFiles($vars) {

      $this->AskAuth();

      // echo "<pre>\n";
      // print_r($vars);
      // echo "</pre>\n";
      // exit(0);

      $uinfo = $this->data->UserInfo($_SERVER['PHP_AUTH_USER'], true);

      // management RAZ the std/sign mode
      @session_start();
      unset($_SESSION['MODE']);
      
      $tpl = new Savant3();
      $urls = URL::GetURLSimple($this->gconf);
      $tpl->assign("URL", $urls);

      $file2sign = $vars['F2S'] ?? [];
      $file2refuse = $vars['F2R'] ?? [];
      
      if ( count($file2sign) == 0 and count($file2refuse) == 0 ) {
          $tpl->assign("ACTION", $vars['ACTION']);
          $tpl->assign("SIGNRES", "nosel");
          $tpl->display("tpl/mgmt/multisign_return.html");
      }
      
      $tpl->assign("F2S", $file2sign);
      $tpl->assign("F2R", $file2refuse);
      $tpl->assign("UINFO", $uinfo);
      
      if ( count($file2sign) >= 1 and count($file2refuse) == 0 ) {
          $tpl->display("tpl/mgmt/multisign_sign.html");
      } 

      if ( count($file2sign) == 0 and count($file2refuse) >= 1 ) {
          $tpl->display("tpl/mgmt/multisign_refuse.html" );
      }

      if ( count($file2sign) >= 1 and count($file2refuse) >= 1 ) {
          $tpl->display("tpl/mgmt/multisign_both.html" );
      }


  }

  function MultiSignSMS($vars) {

    $this->AskAuth();
    // called by javascript only, do not return html
      
    //$this->debug->Debug2("SignSMS", $vars);
    //exit(0);

    $sigphone = $vars['TEL'];
    $signame = $vars['NAM'];
    if ( empty($sigphone) ) {
        echo "ERROR invalid PHONENUM";
        return;
    }
    $res = $this->data->UserSignSMS($_SERVER['PHP_AUTH_USER'], $sigphone) ;
    if ( $res != 0 ) {
        echo "ERROR sms error";
    } else {
        echo "OK sms registered";
    }
  }

  function MultiSignFinal($vars) {

    // echo "<pre>\n";
    // print_r($vars);
    // echo "</pre>\n";
    // exit(0);

    $this->AskAuth();

    // we don't care about $vars['ACTION'] now , because the data->MultiActionFolders
    // manage both sign and refusal
    $vars['SIGNFILES'] = $vars['SIGNFILES'] ?? [];

    if ( count($vars['SIGNFILES']) >= 1 ) {
        $ret = $this->data->VerifyUserSignCode($_SERVER['PHP_AUTH_USER'], $vars['CODE']);
    } else {
        // no file to sign, no need code, assume OK to pass the next step
        $ret = "OK";
    }

    // data to recreate signature info 
    $signerinfo = Array();
    $signerinfo['mail'] = $vars['MAIL'];
    $signerinfo['name'] = $vars['NAME'];
    $signerinfo['phone'] = $vars['PHONE'];
    $signerinfo['sigimg'] = $vars['SIGIMG'];
   
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);

    switch($ret) {
    case "BADDATA":
        $tpl->assign("SIGNRES", "baddata");
        break;
    case "CODEBAD":
        //$tpl->assign("SIGNRES", "codebad");
        header("Content-type: application/json");
        $jsonConcontent = [
            'status' => 'codebad',
            'message' => 'Le code secret est erroné, veuillez renseigner le bon code.'
        ];
        echo json_encode($jsonConcontent);
        return;
        break;
    case "CODEOLD":
        $tpl->assign("SIGNRES", "codeold");
        break;
    case "OK":
        // complete signer info
        $signerinfo['sigimg'] = $vars['SIGIMG'];
        $signerinfo['signacode'] = $vars['CODE'];
        $signerinfo['codeverified'] = true;     // at this stage, it is already verified !
        
        $retop = $this->data->MultiActionFolders($vars['SIGNFILES'], $vars['REFUSEDFILES'], $signerinfo, $vars['MESSG']);
        $tpl->assign("SIGNRES", "done");
        $tpl->assign("OPERATIONS", $retop);   
        break;
    }
    
    $tpl->display("tpl/mgmt/multisign_return.html");

  }

  function MultiSignReq($vars) {

    // echo "<pre>\n";
    // print_r($vars);
    // echo "</pre>\n";
    // exit(0);

    $this->AskAuth();

    $fulist = array();
    
    // loop on SIGREQLIST    // should we use an intermediate data function ??
    $didlist = explode(" ", $vars['SIGREQLIST']);
    foreach($didlist as $did) {

        $fobj = Folder::getFromDid($did, $this->gconf);
        if ( is_null($fobj) ) {
            continue;
        }
        $fulist[] = URL::getURLByDID( $this->gconf, $did)->GetMinimumURL();

        // $vars['SIGNMEL'] is now an array of emails  (<input name="SIGNMEL[]" />)
        $signerlist = $vars['SIGNMEL'];

        // prepare signature request parameters with signer = asker and no comment
        $params = array(
            "signercount" => count($signerlist),
            "signasker" => trim($vars['SIGNASKR']),
            "signacomm" => trim($vars['SIGNCOMM'])
        );
        // and trigger the signature request 
        $this->data->SignRequest($fobj, $params, $signerlist);
        // attach to the signers accounts
        foreach($signerlist as $signer) {
            $this->data->DosAttach4SignVisa($fobj->did, $signer);
        }

        // blog
        $tmp1 = sprintf("Demande de signature pour %s", implode(',', $signerlist));
        $fobj->addBlog($params['signasker'],$tmp1);

    }
 
    // inits  
    $urls = URL::GetURLSimple($this->gconf);
    $tpl = new Savant3();

    // send all the mail , only once by signer
	$tpl->assign("URL", $urls);
	$tpl->assign("SIGNASKR", $params['signasker'] );
	$tpl->assign("SIGNCOMM", $params['signacomm'] );
	$tpl->assign("APPNAM", $this->gconf->name );
	$tpl->assign("FULIST", $fulist );

	$tmpfname = tempnam("/tmp", "CSXsign");
	$fm = fopen($tmpfname, "a");
	fwrite($fm,$tpl->fetch("tpl/mgmt/mail_multisign.html"));
	fclose($fm);

    $msubj = "Demande de signature.";
    
    foreach ( $signerlist as $signer) {  // should be detached ??
        $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$signer." \"".$msubj."\" ".$tmpfname;
        system(escapeshellcmd($launch));
    }

    // Add contact to user contactList if needed
    $this->contacts->addContactIfNeeded($vars);

    // redirect to doslist,  should do better !!
    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetMgmtMethod('DosList');
    header("Location: ".$gourl);

  }
    
  //===============================================
  // debug
  //===============================================

  function VerifyInstall($vars) {

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);
    $tpl->assign("MAINLOGO", $this->gconf->mainlogo);
    $install_status = $this->data->VerifyInstall();
    $tpl->assign("INSTALLSTATUS", $install_status);
    $tpl->display("tpl/mgmt/no_installed.html");
    exit(0);
  }

  function Debug($vars) {

    // $tpl = new Savant3();
    // $urls = URL::GetURLSimple($this->gconf);
    // $tpl->assign("URL", $urls);
    // $tpl->assign("APPNAM", $this->gconf->name);
    // $tpl->assign("MAINLOGO", $this->gconf->mainlogo);
    // $install_status = $this->data->VerifyInstall();
    // $tpl->assign("INSTALLSTATUS", $install_status);
    // $tpl->display("tpl/mgmt/no_installed.html");
    // exit(0);
      
    // global $topdir;
    // include_once $topdir."/config/SignPadesConfig.php";
    // $sconf = new SignPadesConfig();
    // echo "<pre>\n";
    // print_r($sconf);
    // echo "</pre>\n";
    // exit(0);

    //$this->debug->Debug1("Debug");
    //exit(0);

    // $dcnt = $this->data->GetDosSignStatusCountByUser("d.roche@girondenumerique.fr");
    // //$dlist = $this->data->GetDosListByUser("d.roche@girondenumerique.fr");
    // $dlist = $this->data->GetMultiFolderList2Sign("d.roche@girondenumerique.fr");
    // echo "<pre>\n";
    // echo "DCNT\n";
    // print_r($dcnt);
    // echo "==================\n";
    // print_r($dlist);
    // echo "</pre>\n";
    // exit(0);
      

    //$fobj = Folder::create($this->gconf, array('boom'=>'kaboom','bim'=>'kabim','bam'=>'kabam') );
    //$fobj = Folder::getFromDid('GPde1tbo0HlBkKahRLu7Js3wyPnqZMr5', $this->gconf, 1);
    $fobj = Folder::getFromDid('5SEzR2N5DbpGtHkaX3lUb6OtnBaJMBZF', $this->gconf, 1);
    echo "<pre>\n";
    print_r($fobj->_all_);
    echo "==============\n";
    $fobj->globalSignatureStatus();
    print_r($fobj->_all_);
    echo "</pre>\n";
    exit(0);
    
    //$sobj = Signature::create('d.roche@girondenumerique.fr', $fobj, $this->gconf);
    $sobj = Signature::getExisting('toto@gog.ol', $fobj, $this->gconf);
    print_r($sobj->_all_);
    echo "==============\n";
    //$sobj->delete();
    echo "</pre>\n";
    exit(0);

    
    //$uinfo = $this->data->UserInfo('d.roche@girondenumerique.fr');
    //$uobj = User::getFromMel('d.roche@girondenumerique.fr', $this->gconf);
    //$uobj = User::getFromUid(2, $this->gconf);
    //$this->AskAuth();
    //$uobj = User::getFromAuth($this->gconf);
    //echo "<pre>\n";
    //print_r($uninfo);
    //print_r($uobj->_all_);
    //echo "======================\n";
    // echo $uobj->name."\n";
    // $uobj->prout = "tagazou";
    // echo "======================\n";
    // print_r($uobj->_all_);
    //echo "</pre>\n";
    //exit(0);

    // $dlist = $this->data->GetDosListByUserBySignStatus('d.roche@girondenumerique.fr', array("draft"));
    // echo "<pre>\n";
    // print_r($dlist);
    // echo "</pre>\n";
    // exit(0);

    // @session_start();
    // unset($_SESSION['MODE']);
    // echo "<pre>\n";
    // print_r($_SESSION);
    // echo "</pre>\n";
   
    //$this->AskAuth();
    //$uinfo = $this->data->UserInfo("d.roche@girondenumerique.fr", true);
    // echo "<pre>\n";
    // print_r($uinfo);
    // echo "</pre>\n";
    // exit(0);

    //$f2slist = $this->data->GetMultiFolderList2Sign($_SERVER['PHP_AUTH_USER']);
    //$urls = URL::GetURLSimple($this->gconf);

    //$elist = $this->data->GetDosListByUserBySignStatus('d.roche@girondenumerique.fr', array("done","refused"));

    // echo "<pre>\n";
    // print_r($elist);
    // echo "</pre>\n";
    // exit(0);

    // echo "<pre>\n";
    // foreach($f2slist as $dos) {
    //     foreach($dos['filelist'] as $ind => $value) {
    //         $furl = $urls->GetDidInnerData($dos['did'],$value);
    //         $turl = $urls->GetDidDosThumb($dos['did'],$value);
    //         printf("file=%s,furl=%s,turl=%s\n", $value, $furl, $turl);
    //     }
    // }
    // echo "</pre>\n";
    

    //echo "status = ".$this->data->CurrentUserStatus();

    //$this->debug->Debug1("zz"); 
 
    //$ret = $this->data->GetUserLogo(2,"/tmp/zz.data");
    //echo "returned : ".strlen($ret);

    //echo "<img src=\"DispLogo?UID=1\">";
  }

  //===============================================
  // end
  //===============================================

}

