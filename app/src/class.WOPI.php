<?php
//#######################################################################
//
// WOPI (Web Application Open Platform) interface PODOC implementation
//
// for LOOL/Podoc Integration
//
// d.roche@girondenumerique.fr
// - 20200514    initial version
//
//#######################################################################

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/app/src/class.Folder.php";

class WOPI {

    //===============================================
    // framework
    //===============================================
    var $conf;

    function __construct($conf) { 
        $this->conf = $conf;

        // wopi specific log 
        $this->logfile = $this->conf->TopAppDir."/logs/wopi.log";
    }

    //===============================================
    // utilities
    //===============================================

    private function dolog($msg) {
        $fd = fopen($this->logfile, "a");
        fwrite($fd, $msg);
        fclose($fd);
    }

    private function inode2path($wopir) {

        $fobj = Folder::getFromDid($wopir['did'], $this->conf);
        $dosdir = $fobj->absdir;
        
        $cmd = sprintf("find %s -maxdepth 1 -inum %s", $dosdir, $wopir['filid']);
        $pd = popen($cmd, "r");
        $res = fread($pd, 10240);
        pclose($pd);
        return(trim($res));
    }
    
    private function limitedAuth($wopir) {

        // LOOL cannot ask for password, so this is a limited authentication check
        // only based on briefcase passwd status, without checking user !!
        // so we do not use class Data CheckAuth function !
        //
        // if briefcase->passwd  => no access
        // if briefcase->passadm => readonly
        // otherwise => readwrite
        
        $fobj = Folder::getFromDid($wopir['did'], $this->conf);
        $passwd = $fobj->passwd;
        $passadm = $fobj->passadm;

        if ( ! empty($passwd) ) {
            return("NO");
        }
        if ( ! empty($passadm) ) {
            return("RO");
        }
        return("RW");
    }
    
    private function requestInfo($vars) {

        // WOPI specific parsing of http request

        $wopireq = array();
        
        $wopireq['method'] = $_SERVER['REQUEST_METHOD'];
        list($dumb,$class,$meth,$oper, $arg0, $arg1, $arg2) = explode("/", $_SERVER['PATH_INFO']);
        if ( $class != "WOPI" or $meth != "go" ) {
            // should not happen
            return(null);
        }
        $wopireq['classop'] = $oper;
        
        switch($oper) {
        case "files":
            $wopireq['filid'] = $arg0;
            $wopireq['arg1'] = $arg1;
            $wopireq['arg2'] = $arg2;
            $wopireq['token'] = $vars['access_token'];
            $wopireq['did'] = $vars['access_token'];   // token = did in podoc implementation !
            break;
        case "containers":
            // not really implemented,  may be useless !
            $wopireq['contid'] = $arg0;
            break;
        case "ecosystem":
            // not really implemented,  may be useless !
            $wopireq['arg1'] = $arg1;
            $wopireq['arg2'] = $arg2;
            break;
        default:
            // bad WOPI protocol
            return(null);
        }
        return($wopireq);
    }

    //===============================================
    // protocol implementation
    //===============================================
    
    private function wopi_CheckFileInfo($wopireq) {
         
        $limauth = $this->limitedAuth($wopireq);
        if ( $limauth == "NO" ) {
            header($_SERVER["SERVER_PROTOCOL"]." 401 Unauthorized",true,401);
            echo "Unauthorized\n";
            return;
        }

        $rz = $this->inode2path($wopireq);
        if ( $rz != "" ) {

            $sts = stat($rz);
            $retdata = array();
            $retdata['BaseFileName'] = basename($rz);
            $retdata['Size'] = $sts['size'];
            $retdata['OwnerId'] = "www-data";
            $retdata['UserId'] = "www-data";
            $retdata['Version'] = "0";
            if ( $limauth == "RW" ) {
                $retdata['UserCanWrite'] = true;
            } else {
                $retdata['UserCanWrite'] = false;
            }
            //$retdata['UserFriendlyName'] = "";
            //$retdata['UserExtraInfo'] = "";
            $retdata['UserCanNotWriteRelative'] = false;
            //$retdata['PostMessageOrigin'] = "";
            //$retdata['LastModifiedTime'] = date("Y-m-d\Th:i:s.u\Z", $sts['mtime']);
            $retdata['LastModifiedTime'] = date("c", $sts['mtime']);   // iso8601 !!
            //$retdata['SupportsRename'] = true;
            //$retdata['UserCanRename'] = "";
            $retdata['EnableInsertRemoteImage'] = true;
            $retdata['EnableShare'] = true;
            //$retdata['HideUserList'] = "";
            //$retdata['DisablePrint'] = "";
            //$retdata['DisableExport'] = "";
            //$retdata['DisableCopy'] = "";
            //$retdata['HideExportOption'] = "";
            //$retdata['HidePrintOption'] = "";
            //$retdata['DownloadAsPostMessage'] = "";
            
            header("Content-Type: application/json; charset=utf-8");
            echo json_encode( $retdata );
        } else {
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found",true,404);
        }
     }
     
     private function wopi_GetFile($wopireq) {

        $rz = $this->inode2path($wopireq);
        if ( $rz != "" ) {
            header("Content-Type: application/octet-stream");
            readfile($rz);
        } else {
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found",true,404);
        }
     }
   
     private function wopi_PutFile($wopireq) {
        $rz = $this->inode2path($wopireq);
        if ( $rz != "" ) {

            //$this->dolog(sprintf("--> file=%s\n", $rz ));
            $d2w = file_get_contents('php://input');
            //$this->dolog(sprintf("--> size=%d\n", strlen($d2w)));
            
            file_put_contents ( $rz, $d2w );
        } else {
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found",true,404);
        }
    }
   
    //===============================================
    // entry point
    //===============================================

  
    function go($vars) {

        $wopr = $this->requestInfo($vars);

        // echo "<pre>\n";
        // print_r($wopr);
        // echo "</pre>\n";
        // exit(0);

        if ( is_null($wopr) ) {
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found",true,404);
        }


        $this->dolog(sprintf("METH=%s CLAS=%s FID=%s ARG1=%s ARG2=%s\n", $wopr['method'], $wopr['classop'], $wopr['filid'], $wopr['arg1'], $wopr['arg2']) );

        $combinoper = "|".$wopr['method']."|".$wopr['classop']."|".$wopr['arg1']."|";

        switch($combinoper) {
        case "|GET|files|contents|":
            $this->wopi_GetFile($wopr);
            break;
 
        case "|GET|files||":
            $this->wopi_CheckFileInfo($wopr);
            break;

        case "|POST|files|contents|":
            $this->wopi_PutFile($wopr);
            break;
 
        default:
            $this->dolog("not implemented\n");
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found",true,404);
        }
    }

}

