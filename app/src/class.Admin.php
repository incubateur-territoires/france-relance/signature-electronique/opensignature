<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/lib//Savant3/Savant3.php";
include_once $topdir."/app/src/class.Data.php";
include_once $topdir."/app/src/class.Folder.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/lib.LowLevelUtils.php";
include_once $topdir."/app/src/class.Debug.php";

class Admin {

  //===============================================
  // framework
  //===============================================

  var $public_functions;
  var $gconf;

  function __construct($conf) { 
    $this->gconf = $conf;
    $this->data = new Data($conf);
    $this->debug = new Debug();
    $this->topdir = dirname(dirname(__DIR__));

    if ( $this->gconf->sso == "openidc" ) {
        include_once $this->topdir."/app/src/class.OpenIDcClient.php";
        $this->ssoc = new OpenIDcClient($this->gconf, $this->data);
    }
    
   }

  //===============================================
  // web app
  //===============================================

  function DosList($vars) {

    $this->AskAuth();

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);
  
    $dflist = $this->data->GetDosListFullCached();
    //echo "<pre>\n";
    //print_r($dflist);
    //echo "</pre>\n";
    //exit(0);
   
    $tpl->assign("DFLIST", $dflist);
    $tpl->assign("MYSELF", "DosList");
    $tpl->assign('SFILTR', $this->GetSavedFilters() );
    $tmp = $this->GetSavedSort();
    $tpl->assign('SVSORT', $tmp['AD'] );
    @session_start();
    if ( is_null(@$_SESSION['RESTAB']) ) {
        $tpl->assign("RTABS", Array());
    } else {
        $tpl->assign("RTABS", @$_SESSION['RESTAB']);
    }  
    $tpl->assign("CURTAB", "");
    
    $tpl->display("tpl/adm/doslist_dsfr.html");

  }

  function D4U($vars) {

    $this->AskAuth();

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $uinfo = $this->data->UserInfoByUid($vars['UID']);
    if ( $uinfo == null ) {
      echo "ERROR: user not found";
      exit(1);
    }

    $curtag = "D4U/".$vars['UID'];
    $this->ResultTab($curtag);

    $dplist = $this->data->GetDosListByUser($uinfo['mail']);

    $tpl->assign("SEARCHINFO", $uinfo);

    $tpl->assign("DPLIST", $dplist);
    $tpl->assign("MYSELF", "D4U");
    @session_start();
    if ( is_null(@$_SESSION['RESTAB']) ) {
        $tpl->assign("RTABS", Array());
    } else {
        $tpl->assign("RTABS", @$_SESSION['RESTAB']);
    }
    $tpl->assign("CURTAB", $curtag);

    $tpl->display("tpl/adm/res_d4u_dsfr.html");

  }

  function UserList($vars) {

    $this->AskAuth();

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $uflist = $this->data->GetUserListFull();
    //echo "<pre>\n";
    //print_r($uflist);
    //echo "</pre>\n";
    //exit(0);
   
    $tpl->assign("UFLIST", $uflist);
    $tpl->assign("MYSELF", "UserList");
    $tpl->assign("STATUSES", GetUserStatuses());
    $tpl->assign('SFILTU', $this->GetSavedFilturs() );
    $tmp = $this->GetSavedSort();
    $tpl->assign('SVSORT', $tmp['AU'] );
    @session_start();
    if ( is_null(@$_SESSION['RESTAB']) ) {
        $tpl->assign("RTABS", Array());
    } else {
        $tpl->assign("RTABS", @$_SESSION['RESTAB']);
    }
    $tpl->assign("CURTAB", "");

    $tpl->display("tpl/adm/uzrlist_dsfr.html");

  }

  function U4D($vars) {

    $this->AskAuth();

    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);

    $fobj = Folder::getFromDid($vars['DID'], $this->gconf);
    if ( is_null($fobj) ) {
      echo "ERROR: briefcase not found";
      exit(1);
    }
    
    $curtag = "U4D/".$vars['DID'];
    $this->ResultTab($curtag);

    $uplist = $this->data->GetUserListByDid($fobj->did);

    $tpl->assign("SEARCHINFO", $fobj->_all_);

    $tpl->assign("UPLIST", $uplist);
    $tpl->assign("MYSELF", "U4D");
    @session_start();
    if ( is_null(@$_SESSION['RESTAB']) ) {
        $tpl->assign("RTABS", Array());
    } else {
        $tpl->assign("RTABS", @$_SESSION['RESTAB']);
    }
    $tpl->assign("CURTAB", $curtag);

    $tpl->display("tpl/adm/res_u4d_dsfr.html");

  }

  function View($vars) {

    $this->AskAuth();

    // no attachment verification, we are admin !
 
    $dobj = Folder::getFromDid($vars['DID'], $this->gconf);

    // set auth before redirecting to the doc
    $this->data->UpdateAuth($dobj->did, $dobj->passwd); 

    $urls = URL::GetURLByFolder($this->gconf, $dobj);
    $gourl = $urls->GetDosMethod('Display');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);
  }

  function Delete($vars) {

    //echo "<pre>\n";
    //print_r($vars);
    //echo "</pre>\n";
    //exit(0);

    $this->AskAuth();
 
    // no attachment verification, we are admin !
 
    $this->data->DeleteDosByDid($vars['DID']); 

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetAdmMethod('DosList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);
  }

  function CreUser($vars) {

    //echo "<pre>\n";
    //print_r($vars);
    //echo "</pre>\n";
    //exit(0);

    $this->AskAuth();
 
    $udata=Array();
    $udata['mail'] = $vars['UZMEL'];
    $udata['gvname'] = $vars['UZGVN'];
    $udata['name'] = $vars['UZNAM'];
    if ($vars['UZTYP'] == 'ldap') {
        $udata['password'] = "LDAP:".$this->gconf->ldap['server']."::uid=%s,".$this->gconf->ldap['base'];
    } else {
        $udata['password'] = $vars['UZPWD'];
    }
    $udata['status'] = $vars['UZSTS'];
 
    $uid = $this->data->CreateUserValid($udata);
    if ( $uid == 0 ) {
      echo "ERROR user creation";
      return;
    }

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetAdmMethod('UserList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);
  }

  function ModUser($vars) {

    //echo "<pre>\n";
    //print_r($vars);
    //echo "</pre>\n";
    //exit(0);

    $this->AskAuth();

    $udata=Array();
    $udata['mail'] = $vars['UZMEL'];
    $udata['gvname'] = $vars['UZGVN'];
    $udata['name'] = $vars['UZNAM'];

    $this->data->UpdateUserInfo($vars['UID'], $udata);

    if ($vars['UZTYP'] == 'ldap') {
       $tmpass = "LDAP:".$this->gconf->ldap['server']."::uid=%s,".$this->gconf->ldap['base'];
    } else {
       $tmpass = chop($vars['UZPWD']);
    }
    if ( ! empty($tmpass) ) {
      $this->data->UpdateUserPassword($vars['UID'], $tmpass);
    }

    $this->data->UpdateUserStatus($vars['UID'], $vars['UZSTS']);

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetAdmMethod('UserList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);
  }

  function DelUser($vars) {

    //echo "<pre>\n";
    //print_r($vars);
    //echo "</pre>\n";
    //exit(0);

    $this->AskAuth();
 
    $this->data->DeleteUserByUid($vars['UID']);

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetAdmMethod('UserList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);
  }

  function CloseRes($vars) {

    //echo "<pre>\n";
    //print_r($vars);
    //echo "</pre>\n";
    //exit(0);
    
    $this->AskAuth();
 
    $this->ResultDel($vars['TAG1']."/".$vars['TAG2']);

    $urls = URL::GetURLSimple($this->gconf);
    $gourl = $urls->GetAdmMethod('UserList');
    
    //echo $gourl."\n";
    header("Location: ".$gourl);
  }

  //===============================================
  // result tabs stuff
  //===============================================

  private function ResultTab($tag) {
    @session_start();
    if ( ! is_array(@$_SESSION['RESTAB']) ) {
      $_SESSION['RESTAB'] = array();
    }
    if ( ! in_array($tag, @$_SESSION['RESTAB']) ) {
      $_SESSION['RESTAB'][] = $tag; 
    }
  }

  private function ResultDel($tag) {
    @session_start();
    foreach (@$_SESSION['RESTAB'] as $ind => $val) {
        if ( $val == $tag ) {
            unset($_SESSION['RESTAB'][$ind]);
        }
    }
  }

  //===============================================
  // filter stuff
  //===============================================

  function SortSave($vars) {

      //$this->debug->DebugToFile("/tmp/deb.txt", $vars);

      @session_start();
      if ( ! empty($vars['AD']) ) {
          $_SESSION['SRT_AD']=$vars['AD'];
      }
      if ( ! empty($vars['AU']) ) {
          $_SESSION['SRT_AU']=$vars['AU'];
      }
       if ( ! empty($vars['MD']) ) {
          $_SESSION['SRT_MD']=$vars['MD'];
      }
     
      return;
  }

  function FilterSave($vars) {

      //$this->debug->DebugToFile("/tmp/deb.txt", $vars);

      @session_start();
      $_SESSION['FILTA_D']=$vars['FD'];
      $_SESSION['FILTA_M']=$vars['FM'];
      $_SESSION['FILTA_E']=$vars['FE'];
      
      return;
  }

  function FilturSave($vars) {

      //$this->debug->DebugToFile("/tmp/deb.txt", $vars);

      @session_start();
      $_SESSION['FILTU_M']=$vars['F1'];
      $_SESSION['FILTU_N']=$vars['F2'];
      $_SESSION['FILTU_C']=$vars['F3'];
      $_SESSION['FILTU_P']=$vars['F4'];
      
      return;
  }

  private function GetSavedSort() {
      // init default sort
      $svsort = Array("AD"=>"0,0","AU"=>"0,0","MD"=>"0,0");
    
      @session_start();
      if ( isset($_SESSION['SRT_AD']) and $_SESSION['SRT_AD']!= "" ) {
          $svsort['AD']=$_SESSION['SRT_AD'];
      }
      if ( isset($_SESSION['SRT_AU']) and $_SESSION['SRT_AU']!= "" ) {
          $svsort['AU']=$_SESSION['SRT_AU'];
      }
      if ( isset($_SESSION['SRT_MD']) and $_SESSION['SRT_MD']!= "" ) {
          $svsort['MD']=$_SESSION['SRT_MD'];
      }
      return($svsort);

  }

  private function GetSavedFilters() {
    @session_start();
    $sfiltr = Array();
    
    if ( isset($_SESSION['FILTA_D']) and $_SESSION['FILTA_D']!= "" ) {
      $sfiltr['FD']=$_SESSION['FILTA_D'];
    }
    if ( isset($_SESSION['FILTA_M']) and $_SESSION['FILTA_M']!= "" ) {
      $sfiltr['FM']=$_SESSION['FILTA_M'];
    }
    if ( isset($_SESSION['FILTA_E']) and $_SESSION['FILTA_E']!= "" ) {
      $sfiltr['FE']=$_SESSION['FILTA_E'];
    }
    return($sfiltr);

  }

  private function GetSavedFilturs() {
    @session_start();
    $sfiltr = Array();
    
    if ( isset($_SESSION['FILTU_M']) and $_SESSION['FILTU_M']!= "" ) {
      $sfiltr['F1']=$_SESSION['FILTU_M'];
    }
    if ( isset($_SESSION['FILTU_N']) and $_SESSION['FILTU_N']!= "" ) {
      $sfiltr['F2']=$_SESSION['FILTU_N'];
    }
    if ( isset($_SESSION['FILTU_C']) and $_SESSION['FILTU_C']!= "" ) {
      $sfiltr['F3']=$_SESSION['FILTU_C'];
    }
    if ( isset($_SESSION['FILTU_P']) and $_SESSION['FILTU_P']!= "" ) {
      $sfiltr['F4']=$_SESSION['FILTU_P'];
    }
    return($sfiltr);

  }

  //===============================================
  // auth stuff
  //===============================================

  private function AskAuth() {
      switch($this->gconf->sso) {
      case "none":
          $this->AskAuthStd();
          break;
      case "openidc":
          $this->AskAuthSso();
          break;
      }
   }

  private function AskAuthSso(){

      $udata = $this->ssoc->VerifSsoUser("adm");

      // kludge !! , init PHP_AUTH_USER    probably useless
      $_SERVER['PHP_AUTH_USER'] = $udata['user'];

      // useless now, already done in SSO client
      //@session_start();
      //$_SESSION['AUTHENTICATED'] = $udata['user'];
  }
  
  private function AskAuthStd(){

    $realm = "ADM:".$this->gconf->name;
    
    // demande d'identification
    if ( !isset($_SERVER['PHP_AUTH_USER']) ) {
      $this->Authenticate($realm);
    } else {
      $uok = $this->data->VerifAdmUser($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
      if ( ! $uok ) {
	$this->Authenticate($realm);
      }
    }
  }

  private function Authenticate($realm){
    Header("status: 401 Unauthorized"); 
    Header("WWW-Authenticate: Basic realm=\"$realm\" ");
    Header("HTTP/1.0 401 Unauthorized");
 
    // display if authentication canceled
    $tpl = new Savant3();
    $urls = URL::GetURLSimple($this->gconf);
    $tpl->assign("URL", $urls);
    $tpl->assign("APPNAM", $this->gconf->name);
    
    $tpl->display("tpl/mgmt/unauth.html");
   
    exit;
  }


  //===============================================
  // debug
  //===============================================

  function Debug($vars) {

    //$this->debug->Debug1("Debug");
    //exit(0);

    //@session_start();
    //$this->debug->Debug1("SESSION");
    
    @session_start();
    //$tabs = $_SESSION['RESTAB'];
    echo "<pre>\n";
    print_r($_SESSION);
    echo "</pre>\n";

    //$alist = $this->data-> GetDosListFull();
    //echo "<pre>\n";
    //print_r($alist);
    //echo "</pre>\n";

    //$u4list =  $this->data-> GetUserListByDID('0134c1d28abe9dac47537c55aac3a4ef');
    //echo "<pre>\n";
    //print_r($u4list);
    //echo "</pre>\n";

    //$ulist =  $this->data-> GetUserListFull();
    //echo "<pre>\n";
    //print_r($ulist);
    //echo "</pre>\n";

    //$d4list =  $this->data-> GetDosListByUser('dan.y.roche@gmail.com');
    //echo "<pre>\n";
    //print_r($d4list);
    //echo "</pre>\n";

    exit(0);
    
  }

  //===============================================
  // end
  //===============================================

}

