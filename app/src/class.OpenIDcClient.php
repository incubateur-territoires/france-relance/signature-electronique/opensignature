<?php

#######################################################
#
# interface caller to lib/OpenID-Connect-PHP module
#
#######################################################

$topdir = dirname(dirname(__DIR__));
require_once $topdir."/lib/OpenID-Connect-PHP/src/OpenIDConnectClient.php";
include_once $topdir."/app/src/class.Debug.php";

use Jumbojett\OpenIDConnectClient;

class OpenIDcClient {
    
    function __construct($conf, $datobj) { 

        $this->conf = $conf;
        $this->data = $datobj;
        $this->debug = new Debug();

        $this->errurl = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME']."/mgt/NoAuth";

        $this->oidc = new OpenIDConnectClient(
            $this->conf->ssourl,
            $this->conf->ssoid,
            $this->conf->ssosecret
        );

        $this->oidc->addScope('gn_data');

        //$this->oidc->addAuthParam(array("target_organization"=>"girondenumerique.fr"));   // podoc is multi-domain

    }

    function VerifSsoUser($type="all") {

        @session_start();

        $uzdata = Array();
        
        if ( is_null(@$_SESSION['AUTHENTICATED']) ) {
            $this->oidc->authenticate();
            $uinfo = (Array)$this->oidc->requestUserInfo();

            //$this->debug->DebugToFile("/tmp/oidc_cli.log", $uinfo, "after sso auth");
            $uzdata['user'] = $uinfo['clientIdentifier'];
        } else {

            // already authenticated session, do not run sso again, user is already created
            //$this->debug->DebugToFile("/tmp/oidc_cli.log", $uinfo, "authenticated in session");
            $uzdata['user'] = $_SESSION['AUTHENTICATED'];
        }

        // podoc auth only use a limited set of oidc data
        // not sure the domain is still used though
        $tmp = explode("@",$uzdata['user']);
        $uzdata['domain'] = $tmp[1];
            
        // the user login/passwd has been validated ( by sso
        // check the user status in the local podoc redis base ( not given by the SSO )
        // and autocreate it if needed !
        $ustatus = $this->data->UserStatus($uzdata['user']);

        if ( $ustatus == 'none' ) {
            if ( $this->conf->AutoCreateAccount == 0 ) {
                $this->unAuthenticated();
                return(null);
            } else {
                // autocreate
                $ret = $this->autoCreateUser($uinfo);
                $uzdata['status'] = $ret['status'];
            }
        } else {
            $uzdata['status'] = $ustatus;
        }

        if ( $this->CheckUserType($type, $uzdata['status']) ) {
            // store authentication in session
            $_SESSION['AUTHENTICATED'] = $uzdata['user']; 
            $_SESSION['USER_STATUS'] = $uzdata['status']; 
            return($uzdata);
        } else {
            $this->unAuthenticated();
            return(null);
        }
            
    }

    private function autoCreateUser($userinfo) {

        $credata  = array();
        $credata['mail'] = $userinfo['clientIdentifier'];        // do not use the mail ldap field,
        $credata['name'] = $userinfo['family_name'];   
        $credata['gvname'] = $userinfo['given_name'];
        $credata['password'] = "LDAP:_managed_by_sso_";
        $credata['status'] = "premium";     // default autocreated user status
        
        $this->data->CreateUserValid($credata);
        
        return($credata);
    }

    private function unAuthenticated() {

        header("location: ".$this->errurl);
    }

    private function CheckUserType($atyp, $utyp) {

        switch($atyp) {
        case "all":
            return(true);
            break;
        case "std":
            if ( $utyp == "request" ) {
                return(false);
            } else {
                return(true);
            }
            break;
        case "adm":
            if ( $utyp == "admin" ) {
                return(true);
            } else {
                return(false);
            }
            break;
        }
        
    }


    function LogOff($offurl) {

        // for now just remove authenticated session, but do not hit sso server
        @session_start();
        unset($_SESSION['AUTHENTICATED']);
        unset($_SESSION['USER_STATUS']);
        
        header("location: ".$offurl);
        return(null);
    }

        
}

