<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/lib//Savant3/Savant3.php";
include_once $topdir."/config/SignCmsConfig.php";
include_once $topdir."/config/SignTsaConfig.php";

class Verify {

  //===============================================
  // framework
  //===============================================

  var $gconf;

  function __construct($conf) { 

      $this->gconf = $conf;  // global conf
      $this->cms_conf = new SignCmsConfig;
      $this->tsa_conf = new SignTsaConfig;

      $this->logfile = "/tmp/verif.log";
      //$this->logfile = null;
  }
    
  //===============================================
  // web app
  //===============================================

    function VerifCMS($vars) {

        $tpl = new Savant3();

        $tpl->assign("NAME", $this->gconf->name);
        $tpl->display("tpl/verif/cms_index.html");
        
    }
    
    function DoVerifCMS($vars) {

        $tpl = new Savant3();

        //echo "<pre>\n";
        //print_r($vars);
        //echo "</pre>\n";
        //exit(0);

        if ( filesize($vars['FICSIGN']['tmp_name']) == 0 ) {
            $tpl->display("tpl/verif/cms_unverified.html");
            exit(0);
        }
        
        $tmpf2 = tempnam("/tmp", "vrfsg");
        $action = sprintf("%s cms -verify -binary -inform PEM -in %s -content %s -noverify > /dev/null 2>%s", $this->cms_conf->openssl, escapeshellcmd($vars['FICSIGN']['tmp_name']), escapeshellcmd($vars['FICORIG']['tmp_name']), $tmpf2);
        $this->Log($action);
        system($action);
        
        // parse the results
        $res1arr = file($tmpf2);

        //echo "<pre>\n";
        //print_r($res1arr);
        //echo "</pre>\n";
        //exit(0);
        
        if ( preg_match("/Verification/", $res1arr[0]) != 1 ) {
            unlink($tmpf2);
            $tpl->display("tpl/verif/cms_unverified.html");
            return;
        }

        if ( preg_match("/Verification successful/", $res1arr[0]) == 1 ) {

            $tmpf3 = tempnam("/tmp", "vrfsg");
            $action = sprintf("%s cms -cmsout -in %s -inform PEM -print | %s > %s", $this->cms_conf->openssl, escapeshellcmd($vars['FICSIGN']['tmp_name']), $this->cms_conf->dispcms, $tmpf3);
            $this->Log($action);
            system($action);

            $res2arr = file($tmpf3);

            // read metadata
            $mdata = Array();
            $cmslines = file($vars['FICSIGN']['tmp_name']);
            if (  strncmp($cmslines[0], "# Signataire:", 13)==0 ) {
                $mdata['signer'] = substr($cmslines[0],14);
            }
            if (  strncmp($cmslines[1], "# ReferencePreuve:", 18)==0 ) {
                $mdata['refproof'] = substr($cmslines[1],19);
            }
            
            //echo "<pre>\n";
            //print_r($res2arr);
            //echo "</pre>\n";
            //exit(0);

            unlink($tmpf2);
            unlink($tmpf3);

            $tpl->assign("CMSINFO", $res2arr);
            $tpl->assign("MDATA", $mdata);
            $tpl->display("tpl/verif/cms_verifok.html");
            
        } else {
        

            unlink($tmpf2);
            $tpl->display("tpl/verif/cms_verifbad.html");
            
        }
        
    }
    
    function VerifTSA($vars) {

        $tpl = new Savant3();

        $tpl->assign("NAME", $this->gconf->name);
        $tpl->display("tpl/verif/tsa_index.html");
        
    }
    
    function DoVerifTSA($vars) {

        $tpl = new Savant3();

        //echo "<pre>\n";
        //print_r($vars);
        //echo "</pre>\n";
        //exit(0);

        if ( filesize($vars['FICTSA']['tmp_name']) == 0 ) {
            $tpl->display("tpl/verif/tsa_unverified.html");
            exit(0);
        }
        
        $tmpres1 = tempnam("/tmp", "vrfsg");
        
        $action = sprintf("%s ts -reply -in %s -token_in -text 2>/dev/null | sed -e 's|\\\\xC3\\\\xA9|é|g' > %s", $this->tsa_conf->openssl, escapeshellcmd($vars['FICTSA']['tmp_name']), $tmpres1);
        
        $this->Log($action);
        system($action);
        
        // seems not to work well,  get incomplete result ...
        //$pd = popen($action, "r");
        //$result = fread($pd, 4096);
        //pclose($pd);

        $res1arr = file($tmpres1);
        unlink($tmpres1);
        
        //echo "<pre>\n";
        //print_r($res1arr);
        //echo "</pre>\n";
        //exit(0);
        
         if ( strncmp($res1arr[0], "Status info:", 12) != 0 ) {
            $tpl->display("tpl/verif/tsa_unverified.html");

        } else {

            $tmpres2 = tempnam("/tmp", "vrfsg");
        
            $action = sprintf("%s ts -verify -data %s -in %s -token_in -CAfile %s >%s 2>/dev/null",
                              $this->tsa_conf->openssl,
                              escapeshellcmd($vars['FICORIG']['tmp_name']),
                              escapeshellcmd($vars['FICTSA']['tmp_name']),
                              escapeshellcmd($vars['FICCA']['tmp_name']),
                              $tmpres2);
        
            $this->Log($action);
            system($action);
            
            $res2arr = file($tmpres2);
            unlink($tmpres2);
            
            //echo "<pre>\n";
            //print_r($res2arr);
            //echo "</pre>\n";
            //exit(0);
            
            if ( strncmp($res2arr[0], "Verification: OK", 16) == 0 ) {

                $tpl->assign("SIGINFO", $res1arr);
                $tpl->display("tpl/verif/tsa_verifok.html");
                
            } else {
                
                $tpl->display("tpl/verif/tsa_verifbad.html");

            }

        }
       
    }
    
    //===============================================
    // utilities
    //===============================================

    private function Log($msg) {
        if ( isset($this->logfile) ) {
            $fd = fopen($this->logfile, "a+");
            fwrite($fd, $msg);
            fwrite($fd, "\n");
            fclose($fd);
        }
    }

  //===============================================
  // end
  //===============================================

}

