<?php

$topdir = dirname(dirname(__DIR__));
require_once($topdir."/app/signengine/class.InfoSignature.php");
require_once($topdir."/app/signengine/class.TimeStamp.php");
require_once $topdir."/app/src/lib.LowLevelUtils.php";

//====================================
// Signature Proof Management
//
// called by Data controller during 
// folder signature action
//
//====================================

class SignProof {

    //===============================================
    // framework
    //===============================================

    function __construct($conf,$data) { 
        $this->gconf = $conf;
        $this->data = $data;

        $this->signinfo = new InfoSignature();
        $this->tsengine = new TimeStamp();

    }

    //===============================================
    // real code
    //===============================================

    function NewProof() {
    
        $ref0 = GetRandomString(32);
        $proofdir = $this->gconf->ProofDir."/".$this->GetDirFromDID($ref0);
        $cnt = 0;

        while ( file_exists($proofdir) and $cnt < 10 ) {

            $ref0 = GetRandomString(32);
            $proofdir = $this->gconf->ProofDir."/".$this->GetDirFromDID($ref0);
            $cnt += 1;
        }
        if ( $cnt >= 10 ) {
            echo "Proof Random generator exhausted ! aborting....\n";
            exit(1);
        }

        // initialise empty proofdata
        $this->reference = $ref0;
        $this->ProofData = array();
        $this->SignerCount = 0;

        
        mkdir($proofdir, 02775, 1);
        return($ref0);
    }

    function AddValues($arr) {

        $this->ProofData = array_merge($this->ProofData, $arr);
        
        return;
    }

    function AddSigner($signerinfo) {

        $this->SignerCount ++;
        $k = sprintf("Signataire%d", $this->SignerCount);
        $this->ProofData[$k] = $signerinfo;
        
        return;
    }

    function AddSignCerts() {

        // hardcoded URL !! ??
        
        $crts = Array('Certificats' => Array(
            'CA' => $this->signinfo->GetCA(),
            'CRT' => $this->signinfo->GetCRT(),
            'CRL' => $this->signinfo->GetCRL()
        ));
        
        $this->ProofData = array_merge($this->ProofData, $crts);
        
        return;
    }

    // function AddSmsStatus($smss) {
    //     if ( is_null($smss) ) {
    //         return;
    //     }
    //     $sts = Array('SMS_Status' => Array(
    //         'Envoi' => $smss['time'],
    //         'Numero' =>  $smss['tel'],
    //         'Operateur' =>  $smss['operator']) 
    //     );
    //     $this->ProofData = array_merge($this->ProofData, $sts);
    //     return;
    // }

    function AddRequesterInfo($mail,$time) {

        if ( $mail == "_none_" ) {
            $mel2 = "Inconnu";
        } else {
            $mel2 = $mail;
        }

        $ri = Array('Demandeur' => Array(
            'Mail' => $mel2,
            'Date' =>  date("Y/m/d-H:i:s", $time) )
        );
        $this->ProofData = array_merge($this->ProofData, $ri);
        
        return;
    }

    function AddSignVersion() {

        // hardcoded URL !! ??
        $arv = Array('SignatureEngineGitVersion' => $this->signinfo->GetGIT() );
            
        $this->ProofData = array_merge($this->ProofData, $arv);
        
        return;
    }

    function Finalize() {
        
        $pf = $this->Write();

        $outf = $pf.".tsa";
         
        $this->tsengine->TSOneFile($pf, $outf);

        $this->copyCA();

    }
    
    function Write() {
        
        $proofdir = $this->gconf->ProofDir."/".$this->GetDirFromDID($this->reference);
        $pf = $proofdir."/preuve.json";
        
        $fp = fopen($pf,"w");
        fwrite($fp, json_encode($this->ProofData,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
        fclose($fp);
        
        return($pf);
    }

    function copyCA() {
        
        $proofdir = $this->gconf->ProofDir."/".$this->GetDirFromDID($this->reference);
        $cadst = $proofdir."/ca.pem";
        $casrc = $this->tsengine->getCA();
        
        copy($casrc, $cadst);
    }

    //===================================================================
    // low level directory/DID
    //===================================================================

    // for now this is the same algo than the Folder object
    // but this is not related , and it may be different in the future
    // so keep it local to SignProof class !!
    
    private function GetDirFromDID($did) {
        $tdd = substr($did,0,1)."/".substr($did,1,1)."/".substr($did,2,1)."/".substr($did,3,1)."/".substr($did,4);
        return($tdd);
    }
    
    
    //===============================================
    // end
    //===============================================
    
    
}

