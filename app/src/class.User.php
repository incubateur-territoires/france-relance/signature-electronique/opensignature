<?php

//===============================================
// Signature User Management Object 
//
// d.roche@girondenumerique.fr
// initial version : 20230504
//===============================================

$topdir = dirname(dirname(__DIR__));

class User {

    private $gconf;
    private $udata;
    private $redis;
    
    public function __construct($conf) {

        // global conf
        $this->gconf = $conf;

        // empty user data
        $this->udata = array();

    }

    //===================================================================
    //  automatic getter/setter 
    //===================================================================

    public function __get($name) {
        if ( $name == "_all_" ) {
            return ($this->udata);
        } else {
            return ($this->udata[$name]);
        }
    }
    
    public function __set($name, $val) {
        if ( array_key_exists($name, $this->udata) and $this->udata[$name] == $val ) {
            // nothing to do
            return;
        }
        if ( $name == "password" ) {
            if ( strncmp("LDAP:", $val, 5) == 0 ) {
                $h = chop($val);
            } else {
                $h = sha1(chop($val));
            }
            $this->udata['password'] = $h;
        } else {
            $this->udata[$name] = $val;
        }
        $this->save();
    }

    //===================================================================
    // general CRUD  
    //===================================================================

    private function initFromMel($mel) {

        $this->RedisConnect();
        $udata = $this->redis->hgetall("user:".$mel);
 
        if ( empty($udata) ) {
            return(false);
        } else {
            // add mail
            $udata['mail'] = $mel;
            if ( strncmp($udata['password'], "LDAP:", 5) == 0 ) {
                $udata['type'] = "ldap";
            } else {
                $udata['type'] = "std";
            }
            // if smsstatus is here, convert json strict to php array
            if ( isset($udata['smsstatus']) ) {
                $tmp = $udata['smsstatus'];
                $arr = json_decode($tmp, true);
                $udata['smsstatus'] = $arr;
            }

            // TO MODIFY ????
            // if ( $signinfo ) {
            //     // fetch user signature info,
            //     $usigninfo = $this->fetchUserSignInfo($user);
            //     if ( isset($usigninfo['mobile']) ) {
            //         $udata['signphone'] = $usigninfo['mobile'];
            //     }
            //     if ( isset($usigninfo['imageSignature']) ) {
            //         $udata['imageSignature'] = $usigninfo['imageSignature'];
            //         $udata['mimetype'] = $usigninfo['mimetype'];        // is it useful ?
            //     }
            // }

            // store into object
            $this->udata = $udata;
            return(true);
        }
    }

    // access user by UID should no longer be used, meanwhile, keep this function...
    private function initFromUid($uid) {
        $this->RedisConnect();
        $mel = $this->redis->hget('Uid2Mel', $uid);
        if ( $mel ) {
            return($this->initFromMel($mel));
        } else {
            return(false);
        }
    }
    
    private function initFromAuth() {
        if ( isset($_SERVER['PHP_AUTH_USER']) ) {
            return($this->initFromMel($_SERVER['PHP_AUTH_USER']));
        } else {
            return(false);
        }
    }
    
    private function save() {
        // prepare data 2 save
        $d2save = $this->udata;
        unset($d2save['mail']);
        unset($d2save['type']);
        if ( array_key_exists('smsstatus', $this->udata) ) {
            $d2save['smsstatus'] = json_encode($d2save['smsstatus'],JSON_UNESCAPED_SLASHES);
        }
        $this->RedisConnect();
        $this->redis->hmset("user:".$this->udata['mail'], $d2save);
        return;
    }
    
    //===================================================================
    // static fetcher/creation functions
    //===================================================================

    static public function getFromUid($uid, $conf) {
        $uobj = new User($conf);
        if ( $uobj->initFromUid($uid) ) {
            return($uobj);
        } else {
            return(null);
        }
    }
    
    static public function getFromMel($mel, $conf) {
        $uobj = new User($conf);
        if ( $uobj->initFromMel($mel) ) {
            return($uobj);
        } else {
            return(null);
        }
    }
    
    static public function getFromAuth($conf) {
        $uobj = new User($conf);
        if ( $uobj->initFromAuth() ) {
            return($uobj);
        } else {
            return(null);
        }
    }
    
    static public function create($conf, $initialData = null) {
        $uobj = new User($conf); 
        $uobj->init($initialData);
        return($uobj);  
    }

    //===================================================================
    // verify user .  Only Used if SSO not configured
    //===================================================================

    public function verify() {
        return;
    }

    //===================================================================
    // redis stuff , duplicate code from class Data,
    // should be inherited somehow !!
    //===================================================================

    private function RedisConnect() {
        if ( is_null($this->redis) ) {
            $this->redis = new Redis();
            if ( isset($this->gconf->redis_server) and isset($this->gconf->redis_port) ) {
                if ( $this->redis->connect($this->gconf->redis_server, $this->gconf->redis_port) ) {
                    $this->redis->select($this->gconf->redis_base);
                    return(true);
                }
            }
            if ( isset($this->gconf->redis_socket) ) {
                if ( $this->redis->connect($this->gconf->redis_socket) ) {
                    $this->redis->select($this->gconf->redis_base);
                    return(true);
                }
            }
            echo "REDIS CONNECTION FAILED !";
            exit();
        } else {
            return(true);
        }
    }
    
    //===============================================
    // end
    //===============================================
}

   
