<?php

//===============================================
// Signature Action Object 
//
// store all info needed for ONE user signature

// d.roche@girondenumerique.fr
// initial version : 20230515
//===============================================

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/app/src/lib.LowLevelUtils.php";
include_once $topdir."/app/src/class.Data.php";

class Signature {

    private $data;      # private necessaire pour surcharge de fonction !!
    private $gconf;
    private $folder;
    private $mode;
    
    public function __construct($conf, $folder) {

        // global conf
        $this->gconf = $conf;

        // container folder object
        $this->folder = $folder;

        // init to empty array
        $this->data = array();

        // default to delayed mode  inherited from HLsign....
        $this->mode = "delayed";
        //$this->mode = "direct";
    }

    //===================================================================
    //  automatic getter/setter 
    //===================================================================

    public function __get($name) {
        if ( $name == "_all_" ) {
            return ($this->data);
        } else {
            return (@$this->data[$name]);
        }
    }
    
    public function __set($name, $val) {
        if ( array_key_exists($name, $this->data) and $this->data[$name] == $val ) {
            // nothing to do
            return;
        }
        $this->data[$name] = $val;
        $this->save();
    }

    //===================================================================
    // general CRUD  
    //===================================================================

    public function init($mail) {

        if ( empty($mail) ) {
            return(false) ;
        }
        $fodir = $this->folder->dadir;

        if ( ! file_exists($fodir."/.struct/sign_".$mail.".json") ) {
            return(false);
        }

        // fetch data from json file
        $jparam = json_decode(file_get_contents($fodir."/.struct/sign_".$mail.".json"), true);
        $this->data = $jparam;

        return(true);
    }
    
     public function update($values) {
        $cnt = 0;
        foreach( $values as $key => $val ) {
            if ( array_key_exists($key, $this->data) and $this->data[$key] == $val ) {
                continue;
            }
            $this->data[$key] = $val;
            $cnt++;
        }
        if ( $cnt ) {
            $this->save();
        }
    }

    public function delItem($key) {
        if ( array_key_exists($key, $this->data) ) {
            unset($this->data[$key]);
            $this->save();
        }
    }

    public function delItems($keys) {
        $cnt = 0;
        foreach( $keys as $key ) {
            if ( array_key_exists($key, $this->data) ) {
                unset($this->data[$key]);
                $cnt++;
            }
        }
        if ( $cnt ) {
            $this->save();
        }
    }

    public function delete() {

        $fodir = $this->folder->dadir;
        $mail = $this->data['mail'];
        
        unlink($fodir."/.struct/sign_".$mail.".json");
    }

    //===================================================================
    // internal private functions 
    //===================================================================
    
    private function save() {
        
        $fodir = $this->folder->dadir;
        $mail = $this->data['mail'];

        if ( is_null($mail) or empty($mail) ) {      // should not happen
            return;
        }
        
        $jdata = json_encode($this->data, JSON_PRETTY_PRINT|JSON_INVALID_UTF8_SUBSTITUTE|JSON_UNESCAPED_SLASHES);
        $fp = fopen($fodir."/.struct/sign_".$mail.".json", 'w');
        fwrite($fp, $jdata);
        fclose($fp);
    }

    //===================================================================
    // SMS stuff
    //===================================================================

    public function signSMS() {
        // phone have to be previously initialized

        if ( empty($this->data['phone']) ) {
            trigger_error("No signerphone, abort SignSMS");
            return(1);
        }
        
        $code = GetRandomDigit(5);

        $this->data['signacode'] = $code;
        $this->data['codetime'] = date("U");

        // SEND THE SMS !!
        // get the sms class
        $sclass = $this->gconf->SmsClass;
        $fclass = $this->gconf->TopAppDir."/app/src/class.".$sclass.".php";
        if ( file_exists($fclass) ) {
            include_once $fclass;
            $obj = new $sclass($this->gconf->SmsParam);
        } else {
            trigger_error("SignSMS : no SMS Class !");
            return(1);
        }          

        $tmp = sprintf("%s : votre code secret pour signature : %s", $this->gconf->name, $code);
      
        $rs = $obj->SendSMS($this->data['phone'], $tmp);     
        if ( ! is_null($rs) ) {
            $this->data['smsstatus'] = $rs;      // smsstatus is used by sign proof 
        }
        $this->save();
        
        return(0);
        
    }

    public function verifySignCode($givencode) {

        $code = $this->data['signacode'];
        if ( empty($code) ) {
            return("BADDATA");
        }          
        
        // check 15 minutes 
        $codetime = $this->data['codetime'];
        $now=time();
        $deltat = $now - $codetime;
        if ( $deltat > 900 ) {
            return("CODEOLD");
        }
        
        //check the code
        if ( $givencode != $code ) {
            return("CODEBAD");
        }
        return("OK");
    }

    //===================================================================
    // signature action
    //===================================================================

    // more generic signature action
    // process both signature an refusal
    // may call private func SignatureRefuse and/or signatureAccept
    
    public function signatureTrigger($code) {

        $retval = "";

        $signcount = (is_array($this->data['signlist']))?count($this->data['signlist']):0;
        $refusecount = (is_array($this->data['refuselist']))?count($this->data['refuselist']):0;
        
        switch ( $this->data['action'] ) {
        case "refuse":
            // all folder refused
            $this->signatureRefuse(false);
            break;

        case "autosign":
            // all folder accepted
            $retval = $this->signatureAccept($code);
            break;
            
        case "sign":
        case "both":
            // may have both signature and refuse action ( if multiple folder at once )
            
            $signcount = (is_array($this->data['signlist']))?count($this->data['signlist']):0;
            $refusecount = (is_array($this->data['refuselist']))?count($this->data['refuselist']):0;
        
            if ( $signcount > 0 and $refusecount > 0 ) {
                $this->data['partial'] =  true;
                $this->save();
            }

            if ( $refusecount > 0 ) {
                $this->signatureRefuse(true);
            }
            if ( $signcount > 0 ) {
                $retval = $this->signatureAccept($code);
            }
            break;
        }
        
        return($retval);
        
    }

    private function signatureRefuse($partial=false) {

        // if partial mode  ( both sign and refuse action on same folder / multisign )
        // we do not want to set the refused status to the folder !
        // we just flag the partial status ( with the number of signed == partial,  may be usefull later )

        if ( $partial ) {
            $this->data['partial'] = true;
            $this->save();
        } else {
            $this->data['status'] = "refused";
            $this->save();

            // if refused, adjust the folder status
            $this->folder->globalSignatureStatus();
        } 
      
        // message  
        $tmp1 = "Refus de signature : ".$this->data['comment'];
        // on refuse the signer-name is not initialised
        $this->folder->addBlog($this->data['mail'],'<i class="fas fa-times-circle blog-ko"></i>&nbsp;'.$tmp1);
        
        if ( ! is_null($this->folder->signasker) ) {
            // send the mail
            $urls = URL::GetURLByFolder($this->gconf, $this->folder);
            $tpl = new Savant3();
            $tpl->assign("URL", $urls);
            $tpl->assign("DOSINFO", $this->folder->_all_ );
            $tpl->assign("SIGNER", $this->data['mail'] );
            $tpl->assign("APPNAM", $this->gconf->name );
            
            $tmpfname = tempnam("/tmp", "CSXsign");
            $fm = fopen($tmpfname, "a");
            fwrite($fm,$tpl->fetch("tpl/doc/mail_sign_refused.html"));
            fclose($fm);
            
            $msubj = "Refus de signature ! : ".$this->folder->title;
            $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$this->folder->signasker." \"".$msubj."\" ".$tmpfname;
            system(escapeshellcmd($launch));
        }
        
    }

    private function signatureAccept($code) {

        // verify code only if not null
        if ( ! is_null($code) ) {
            $ret = $this->verifySignCode($code);

            // failure blog message must be done now,   not delayed....
            switch($ret) {
            case "BADDATA":
            case "BADFOLDER":
                $this->folder->addBlog($this->gconf->name,"Echec de signature");
                $retval = "notdone";
                break;
            case "CODEBAD":
                $this->folder->addBlog($this->gconf->name,"Echec de signature : Code secret erroné");
                $retval = "notdone";
                break;
            case "CODEOLD":
                $this->folder->addBlog($this->gconf->name,"Echec de signature : Code secret périmé");
                $retval = "notdone";
                break;
            case "OK":
                
                // register the signer IP for later use
                $this->data['signerip'] = $_SERVER['REMOTE_ADDR'];
                $this->data['codeverified'] = true;
                $this->save();
                break;
            }
        }
        
        // process signature
        
        if ( $this->mode == "direct" ) {
            // we need to call the hlsign class
            $dobj->SignatureProcess();
            $retval = "done";
        }
        if ( $this->mode == "delayed" ) {
            $this->data['status'] = "delayed";
            $this->save();
            if ( $this->folder->globalSignatureStatus() ) {
                // we need to call the data class
                $dobj = new Data($this->gconf);
                $dobj->SpoolSign($this->folder->did);
                $retval = "delayed";
            } else {
                $retval = "multiple";
            }
        }
        
        return($retval);
        
    }
   
    //===================================================================
    // signature image stuff
    //===================================================================

    public function initSignImage($type, $usigninfo) {

        $fodir = $this->folder->dadir;
        $mail = $this->data['mail'];
        
        if ( $type != "img" ) {
            // should we initialize the default image ?
            return;
        }

        if ( isset($usigninfo['imageSignature']) ) {
            if ( $usigninfo['mimetype'] == "image/jpeg" ) {
                $signimg = $fodir."/.struct/signimage_".$mail.".jpg";
            } else {
                $signimg = $fodir."/.struct/signimage_".$mail.".png";
            }
            $fi = fopen($signimg, 'w');
            fwrite($fi, base64_decode($usigninfo['imageSignature']));
            fclose($fi);
        }
    }

    public function getSignImage() {

        $fodir = $this->folder->dadir;
        $mail = $this->data['mail'];
        $bazimg = $fodir."/.struct/signimage_".$mail;
        if ( file_exists($bazimg.".jpg") ) {
            return($bazimg.".jpg");
        }
        if ( file_exists($bazimg.".png") ) {
            return($bazimg.".png");
        }
        return(null);

    }

    public function delSignImage() {          // probably useless
    
        $fodir = $this->folder->dadir;
        $mail = $this->data['mail'];
        $bazimg = $fodir."/.struct/signimage_".$mail;
        if ( file_exists($bazimg.".jpg") ) {
            unlink($bazimg.".jpg");
        }
        if ( file_exists($bazimg.".png") ) {
            unlink($bazimg.".png");
        }
    }
    
    //===================================================================
    // static fetcher/creation functions
    //===================================================================

    static public function getExisting($mail, $folder, $conf) {
        $sobj = new Signature($conf, $folder);
        if ( $sobj->init($mail) ) {
            return($sobj);
        } else {
            return(null);
        }
    }
    
    static public function create($mail, $folder, $conf, $initialData = null) {
        if ( is_null($mail) or is_null($folder) or empty($mail) or empty($folder) ) {
            return(null);
        }
        $sobj = new Signature($conf, $folder);      // normaly never fails...

        $tmpdata = array();
        // init at least the mail
        $tmpdata['mail'] = $mail;
        if ( ! is_null($initialData) ) {
            $tmpdata = array_merge($tmpdata, $initialData);
        }

        $sobj->update($tmpdata);
        return($sobj);  
    }

    // no longer used ? to be removed 
    // static public function getOrCreate($mail, $folder, $conf, $initialData = null) {
    //     $sobj = new Signature($conf, $folder);      // normaly never fails...
    //     if ( $sobj->init($mail) ) {
    //         $tmpdata = $sobj->_all_;
    //     } else {
    //         $tmpdata = array();
    //         // init at least the mail
    //         $tmpdata['mail'] = $mail;
    //     }
    //     if ( ! is_null($initialData) ) {
    //         $tmpdata = array_merge($tmpdata, $initialData);
    //     }
    //     $sobj->update($tmpdata);
    //     return($sobj);  
    // }

    //===================================================================
    // miscellaneous 
    //===================================================================

    // duplicate of Folder function,   which one do we keep ????
    
    private function FilterMultiSig($list) {
        // if found multiple signed pdf,   keep the oldest

        $frac = array();
        
        foreach( $list as $elem ) {
            
            $ext = pathinfo($elem, PATHINFO_EXTENSION);
            if ( strtolower($ext) != "pdf" ) {
                $frac[$elem] = $elem;
            } else {
                
                $bn = pathinfo($elem, PATHINFO_FILENAME);
                $pos = strpos($bn, "-sig");
                if ( $pos ) {
                    $rac = substr($bn, 0, $pos);
                } else {
                    $rac = $bn;
                }
                if ( array_key_exists($rac, $frac) ) {
                    $st1 = stat($this->dadir."/".$frac[$rac]);
                    $mtim1 = $st1['mtime'];
                } else {
                    $mtim1 = 0;
                }
                $st2 = stat($this->dadir."/".$elem);
                $mtim2 = $st2['mtime'];
                // keep the oldest
                if ( $mtim2 > $mtim1 ) {
                    $frac[$rac] = $elem;
                }
            }
            
        }
      
        return(array_values($frac));
    }

    //===============================================
    // end
    //===============================================
}

   




