<?php

class DataProof {

  //===============================================
  // framework
  //===============================================

  function __construct($conf) { 
    $this->gconf = $conf;
  }

  //===============================================
  // internal dir structure
  //===============================================

  function GetDirFromPID($pid) {
      $tdd = substr($pid,0,1)."/".substr($pid,1,1)."/".substr($pid,2,1)."/".substr($pid,3,1)."/".substr($pid,4);
      return($tdd);
  }

  function GetPIDFromDir($dir) {
    if ( preg_match("|.*/(.)/(.)/(.)/(.)/(.*)/.*|", $dir, $rm) ) {
      $pid = $rm[1].$rm[2].$rm[3].$rm[4].$rm[5];
      return($pid);
    } else {
      return(NULL);
    }
  }

  function GetAbsDirFromPID($pid) {
      return( $this->gconf->ProofDir."/".$this->GetDirFromPID($pid) );
  }

  function GetRelDirFromPID($pid) {
      return( basename($this->gconf->ProofDir)."/".$this->GetDirFromPID($pid) );
  }

  //===============================================
  // fetch proof data
  //===============================================

  function GetProof($refp) {

      $dir = $this->GetAbsDirFromPID($refp);

      if ( file_exists($dir."/preuve.json") and file_exists($dir."/preuve.json.tsa") ) {
          $rv = array(
              'ref'=> $refp,
              'rel_preuve'=> $this->GetRelDirFromPID($refp)."/preuve.json",
              'rel_tsa'=>$this->GetRelDirFromPID($refp)."/preuve.json.tsa",
              'abs_preuve'=> $this->GetAbsDirFromPID($refp)."/preuve.json",
              'abs_tsa'=>$this->GetAbsDirFromPID($refp)."/preuve.json.tsa",
              'abs_ca'=>$this->GetAbsDirFromPID($refp)."/ca.pem"
          );
          return($rv);
      } else {
          return(null);
      }
  }

  //===============================================
  // admin
  //===============================================

  function GetProofList() {

      $plist = array();

      $glob = $this->gconf->ProofDir."/?/?/?/?/*/preuve.json";
      $li0 = glob($glob);
      $len1 = strlen($this->gconf->ProofDir);

      foreach($li0 as $file) {

          $tmp1 = substr($file, $len1);
          $ref = substr($tmp1,1,1).substr($tmp1,3,1).substr($tmp1,5,1).substr($tmp1,7,1).substr($tmp1,9,28);

          $sts = stat($file);
          // the real iso 8601 format ( Y-m-d\TH:i:s )  breaks the javascript sorting !!! 
          $date = date("Y-m-d H:i:s", $sts['mtime']);

          $jdata = chop(file_get_contents($file));
          $info = json_decode($jdata, 1);
          
          $plist[] = array("ref"=>$ref, "date"=>$date, "signer"=>$info['Signataire']['Mail'] );
      }

      return($plist);
  }


  //===============================================
  // end
  //===============================================

}

