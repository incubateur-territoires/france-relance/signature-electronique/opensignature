<?php

$topdir = dirname(dirname(__DIR__));
require_once($topdir."/lib/tcpdf/tcpdf.php");
require_once($topdir."/config/SignCmsConfig.php");


//====================================
// Signature Slip Generator
//
// called by Data controller during 
// folder signature action
//
// based on TCPDF examples
//
//====================================

class SignSlip {

    //===============================================
    // framework
    //===============================================

    function __construct($conf, $data) { 
        $this->gconf = $conf;
        $this->data = $data;

        $this->SlipData = array();

    }

    //===============================================
    // init code
    //===============================================

    function AddValues($arr) {

        $this->SlipData = array_merge($this->SlipData, $arr);
        
        return;
    }

    function InitCerts() {

        // use CMS sign engine config, this is the most detailled !
        //
        $signconf = new SignCmsConfig;

        
        $certinf = Array();
        
        $crt = trim(file_get_contents($signconf->pubcert));
        $res1 = openssl_x509_parse($crt);         

        $certinf['crtname'] = str_replace("/", " / ", trim($res1['name'], "/"));
        $certinf['crtserial'] = $res1['serialNumberHex'];
        
        $ca = trim(file_get_contents($signconf->rootca));
        $res2 = openssl_x509_parse($ca);         

        $certinf['caname'] =  str_replace("/", " / ", trim($res2['name'], "/"));
        
        $this->CertsInfo = $certinf;
        
        return;
    }

    //===============================================
    // generation code
    //===============================================

    function Generate($fout) {

        // create new PDF document
        $this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor(PDF_AUTHOR);
        $this->pdf->SetTitle('Bordereau de Signature');

        // set default header data
        // ATTENTION, the path for images has to be configured in /lib/tcpdf/config/tcpdf_config.php var K_PATH_IMAGES
        $this->pdf->SetHeaderData($this->gconf->SignPlatformLogo, 30, 'Bordereau de Signature', 'Plateforme: '.$this->gconf->SignPlatformUrl);

        // set header and footer fonts
        $this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 16));
        $this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // ---------------------------------------------------------

        // set font
        $this->pdf->SetFont('helvetica', '', 14);
        // add a page
        $this->pdf->AddPage();

        if ( count($this->SlipData['signers']) == 1 ) {
            $this->pdf->Write(0, 'Informations du signataire:', '', 0, 'L', true, 0, false, false, 0);
        } else {
            $this->pdf->Write(0, 'Informations des signataires:', '', 0, 'L', true, 0, false, false, 0);
        }    

        // ---------------------------------------------------------

        $this->pdf->Ln(5);

        $this->pdf->SetFont('times', '', 10);

        //$this->pdf->SetCellPadding(0);
        //$this->pdf->SetLineWidth(2);

        // set color for background
        $this->pdf->SetFillColor(255, 255, 200);

        if ( count($this->SlipData['signers']) == 1 ) {
            $signr0 = $this->SlipData['signers'][0];
            $this->MultiRow('Signataire:', sprintf("%s, identifié.e par code SMS.\n",$signr0['Identite']), 6);
            $this->MultiRow('Mail signataire:', $signr0['Mail']."\n", 6);
        } else {
            foreach($this->SlipData['signers'] as $num => $signri) {
                $tit = sprintf("Signataire%d:", $num+1);
                $com = sprintf("%s (%s), identifié.e par code SMS.\n",$signri['Identite'],$signri['Mail']);
                $this->MultiRow($tit, $com, 6);
            }
        }
        
        $this->MultiRow('Date de signature:', $this->SlipData['date']."\n", 6);
        $this->MultiRow('Certificat signataire:', $this->CertsInfo['crtname']."\n",14);
        $this->MultiRow('Autorité de Certification:', $this->CertsInfo['caname']."\n", 10);
        $this->MultiRow('N°Série certificat:', $this->CertsInfo['crtserial']."\n", 6);
        $this->MultiRow('Référence preuve:', $this->SlipData['proof']."\n", 6);

        // ---------------------------------------------------------

        $this->pdf->Ln(5);

        $this->pdf->SetFont('helvetica', '', 14);

        $this->pdf->Write(0, 'Fichiers signés:', '', 0, 'L', true, 0, false, false, 0);

        $this->pdf->Ln(5);

        // ---------------------------------------------------------

        // set font
        $this->pdf->SetFont('helvetica', '', 8);

        // column titles
        $header = array('Fichier', 'Condensat SHA512');

        // print colored table
        $this->ColoredTable($this->SlipData['files']);

        // reset pointer to the last page
        $this->pdf->lastPage();

        // ---------------------------------------------------------

        //Close and output PDF document
        $this->pdf->Output($fout, 'F');
        
    }
    
    private function MultiRow($left, $right, $height) {
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0)

        $page_start = $this->pdf->getPage();
        $y_start = $this->pdf->GetY();
        
        // write the left cell
        $this->pdf->MultiCell(40, $height, $left, 1, 'R', 1, 2, '', '', true, 0);
        
        $page_end_1 = $this->pdf->getPage();
        $y_end_1 = $this->pdf->GetY();
        
        $this->pdf->setPage($page_start);
        
        // write the right cell
        $this->pdf->MultiCell(0, $height, $right, 1, 'J', 0, 1, $this->pdf->GetX() ,$y_start, true, 0);
        
        $page_end_2 = $this->pdf->getPage();
        $y_end_2 = $this->pdf->GetY();
        
        // set the new row position by case
        if (max($page_end_1,$page_end_2) == $page_start) {
            $ynew = max($y_end_1, $y_end_2);
        } elseif ($page_end_1 == $page_end_2) {
            $ynew = max($y_end_1, $y_end_2);
        } elseif ($page_end_1 > $page_end_2) {
            $ynew = $y_end_1;
        } else {
            $ynew = $y_end_2;
        }
        
        $this->pdf->setPage(max($page_end_1,$page_end_2));
        $this->pdf->SetXY($this->pdf->GetX(),$ynew);
    }

    private function ColoredTable($filelist) {
        // Colors, line width and bold font
        $this->pdf->SetFillColor(255, 255, 200);
        $this->pdf->SetTextColor(0);
        $this->pdf->SetDrawColor(0, 0, 0);
        $this->pdf->Cell(0, 5, "fichiers / condensat sha512", 1, 0, 'C', 1);
        $this->pdf->Ln();
        $this->pdf->SetFillColor(224, 235, 255);
        $this->pdf->SetTextColor(0);
        $this->pdf->SetFont('');
        // Data
        $fill = 0;
        foreach($filelist as $fil => $sha) {
            $this->pdf->SetFont('helvetica', '', 8);
            $this->pdf->Cell(0, 6, $fil, 'LR', 0, 'L', $fill);
            $this->pdf->Ln();
            $this->pdf->SetFont('courier', '', 6);
            $this->pdf->Cell(0, 5, "       ".$sha, 'LR', 0, 'L', $fill);
            $this->pdf->Ln();
            $fill=!$fill;
        }
        //$this->pdf->Cell(array_sum($w), 0, '', 'T');
        $this->pdf->Cell(0, 0, '', 'T');
    }

    //===============================================
    // utilities
    //===============================================
    
    private function GetCertFromUrl($url) {

        $hh = fopen($url, "r");
        $res = fread($hh, 40960);
        fclose($hh);

        return($res);
    }

    //===============================================
    // end
    //===============================================
    
    
}

