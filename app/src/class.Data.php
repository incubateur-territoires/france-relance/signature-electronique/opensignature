<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/app/src/class.Folder.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.SignProof.php";
include_once $topdir."/app/src/class.SignSlip.php";
include_once $topdir."/app/src/class.Debug.php";
include_once $topdir."/app/src/lib.LowLevelUtils.php";

define("ACCES_NO", 0);
define("ACCES_RW", 1);
define("ACCES_RO", 2);


class Data {

  //===============================================
  // framework
  //===============================================

  function __construct($conf) { 
    $this->gconf = $conf;
    $this->debug = new Debug();
    $this->redis = NULL;
  }

  //===============================================
  // multi step dos structure creation
  //===============================================

  //===============================================
  // dos struct mgmt
  //===============================================

  // called by new step 1 dos create
  function PreCreateDos() {

    $utyp = $this->CurrentUserStatus();
    $datinf = $this->GetDateInfo($utyp);

     $precreatedata = array(
        "title" => date("_Ymd_His"),
        "endoflife" => date("Y-m-d", $datinf['datelim']),
        "dispmode" => "sign",        // forced to sign in this context
        "signstatus" => "draft"        // forced draft mode at creation
    );
      
    $fobj = Folder::create($this->gconf, $precreatedata);
    return($fobj);
  }

  // called by new step 2 dos create
  // function PostCreateDosStruct($did,$vars,$uinfo) {
  //   $partial = $this->DosStructCreateExtended($did,$vars);
  //   if ( isset($uinfo['llogo']) ) {
  //       $ddir = $this->GetAbsDirFromDID($did);
  //       $this->GetUserLogo($uinfo['id'], $ddir."/.logo");
  //   }
  //   return($partial);
  // }
    
  function UpdateDos($fobj, $vars) {
    
    if ( is_null($fobj) ) {
      return null;
    }

    // prepare data
    $updateddata = array(
        "title" => @$vars['DOSNAM'],
        "comment" => @$vars['DOSCOM'],
        "passwd" => @$vars['DOSPSW'],
        "endoflife" => @$vars['DOSLIM']
    );
    $fobj->update($updateddata);
    
  }
 
  function DeleteDosByObj($fobj) {

    if ( is_null($fobj) ) {
      return null;
    }
    $fobj->delete();

    // next delete folder/user associations
    $this->DosDestroy($fobj->did);
   }

  function DeleteDosByDid($did) {

    $fobj = Folder::getFromDid($did, $this->gconf);
    $this->DeleteDosByObj($fobj);
   }

  //===============================================
  // 
  //===============================================

  // register sign dir in the signspool
  // for later (incrontab) signature processing 
  function SpoolSign($did) {
    
    $spoolf = $this->gconf->signspool."/".$did;

    $fp = fopen($spoolf, 'a');
    fwrite($fp, "just a flag\n");
    fclose($fp);

  }

  function CheckAuth($foldobj) {
    
    $did = $foldobj->did;
    $passwd = @$foldobj->passwd;

    $appnam = $this->gconf->name;

    //$this->debug->Debug2("CheckAuth",  $this->gconf);
    //exit(0);

    @session_start();

    if ( $passwd == "" || ( isset($_SESSION[$appnam.'_'.$did]) && $_SESSION[$appnam.'_'.$did] == $passwd ) ) {

      if ( ! is_null($foldobj->passadm) ) {

          $passadm = $foldobj->passadm; 
          if (  isset($_SESSION[$appnam.'ADM_'.$did]) && $_SESSION[$appnam.'ADM_'.$did] == $passadm ) {
              return(ACCES_RW);
          } else {
              return(ACCES_RO);
          }
      } else {
          return(ACCES_RW);
      }
    }
   
    return(ACCES_NO);

  }  

  function UpdateAuth($did, $passwd) {
    
    @session_start();
    $appnam = $this->gconf->name;
    $_SESSION[$appnam.'_'.$did] = $passwd; 
  }
 
  //===============================================
  // SMS status , used with sign proof 
  //===============================================

  function SetUserSmsStatus($umel, $status) {

    $data = array("smsstatus"=>json_encode($status,JSON_PRETTY_PRINT ));

    $this->UpdateUserInfoByMel($umel,$data);
  }

  //===============================================
  // mgmt and BD stuff
  //===============================================

  function GetDosListByUser($mel, $lmod = "all") {        // $lmod = all|std|visa|sign 
      
      $this->RedisConnect();

      $res = array();
      $dlist = $this->redis->smembers("dlist:".$mel);
      foreach ($dlist as $did ) {
          $val = $this->redis->hget("OwnerRel", $mel."|".$did);
          $res[] = array('dos_id'=>$did, 'mode'=>$val);
      }
      
      //echo "<pre>\n";
      //print_r($res);
      //echo "</pre>\n";

      $list = array();

      foreach ($res as $elem){
          $subd = Array();
          $fobj = Folder::getFromDid($elem['dos_id'], $this->gconf);
          $finf = $fobj->_all_;  // use a copy of folder object because we override some info !
          
          if ( $lmod == "all" or $lmod == $finf['dispmode'] ) {
              if ( $elem['mode'] == "reader" and @$finf['passadm'] != "" ) {
                  $finf['passadm'] = "######";
              }
              if ( strncmp(@$pinf['passadm'], "_SIGN_", 6) == 0 ) {
                  $finf['passadm'] = "######";
              }
              $subd['mode'] = $elem['mode'];
              $subd['data'] = $finf;
              $subd['signerlist'] = $fobj->getSignatureList();
              $list[$elem['dos_id']] = $subd;
          }
      }
 
      return($list);

  }
    
  function GetDosListByUserBySignStatus($mel, $statuslist) {       // should probably be more generic.....

      $l0 = $this->GetDosListByUser($mel, "sign");        // display mode sign forced here

      $l1 = array();
      foreach($l0 as $did => $elem) {

          $edata = $elem['data'];
          $signerlist = $elem['signerlist'];
          // specific case : asked : compare with $user
          if ( isset($edata['signstatus']) ) {
              if ( $edata['signstatus'] == "asked"  ) {
                  if ( in_array($mel, $signerlist) ) {
                      $dosstatus = "asked-tosign";
                  } else {
                      $dosstatus = "asked-pending";
                  }
              } else {
                  $dosstatus = $edata['signstatus'];
              }
              if ( in_array($dosstatus, $statuslist) ) {
                  $l1[$did] = $elem;
              }
          }
      }
      return($l1);
  }

  function GetDosSignStatusCountByUser($mel) {    

      $l0 = $this->GetDosListByUser($mel, "sign");       // display mode sign forced here

      $cnt = array();
      foreach($l0 as $did => $elem) {

          $edata = $elem['data'];
          $signerlist = $elem['signerlist'];
          if ( isset($edata['signstatus']) ) {
              if ( $edata['signstatus'] == "asked" ) {
                  
                  // specific asked case  : must compare with user
                  if ( in_array($mel, $signerlist) ) {
                      $tag = "asked-tosign";
                  } else {
                      $tag = "asked-pending";
                  }
                  
              } else {
                  $tag = $edata['signstatus'];
              }
          } else {
              $tag = "unknown";
          }
          @$cnt[$tag] ++;
      }
      return($cnt);
  }

  // no longer used,  use GetDosListByUserBySignStatus by passing draft in status list
  //
  // function GetDosListByUserByNoSign($mel) {       // no longer used, 
  //     $l0 = $this->GetDosListByUser($mel, "all");
  //     $l1 = array();
  //     foreach($l0 as $did => $elem) {
  //         $edata = $elem['data'];
  //         if ( isset($edata['signstatus']) or isset($edata['signature']) ) {
  //             continue;
  //         } else {
  //             $l1[$did] = $elem;
  //         }
  //     }
  //     return($l1);
  // }

  function GetMultiFolderList2Sign($mel, $lmod = "all") {        
      
      $this->RedisConnect();

      $res = array();
      $dlist = $this->redis->smembers("dlist:".$mel);
      foreach ($dlist as $did ) {
          $val = $this->redis->hget("OwnerRel", $mel."|".$did);
          $res[] = array('dos_id'=>$did, 'mode'=>$val);
      }
      
      $list = array();

      foreach ($res as $elem){
          $subd = Array();
          $fobj = Folder::getFromDid($elem['dos_id'], $this->gconf);
          $signerlist = $fobj->getSignatureList();

          if ( $fobj->signstatus == "asked" and $elem['mode'] == "writer" ) {   // is test on mode really usefull ?
          
              if ( in_array($mel,$signerlist) ) {
          
                  // refetch folder with signable files
                  $fobj2 = Folder::getFromDid($elem['dos_id'], $this->gconf, 2);
                  $list[$elem['dos_id']] =  $fobj2->_all_;
              }
          }
      }
 
      return($list);

  }

  function VerifUser($user, $passwd ) {
    return $this->VerifUserByType($user, $passwd, 'std');
  }

  function VerifAdmUser($user, $passwd ) {
    return $this->VerifUserByType($user, $passwd, 'adm');
  }

  function VerifUserByType($user, $passwd, $type ) {
    // return 0 if not OK
    // return 1 if user valid
    // type = std|adm

    $this->RedisConnect();

    $uinfo = $this->redis->hgetall("user:".$user);

    if ( empty($uinfo) ) {
      // user not found 
      // call UserAutoCreate if configured
      if ( isset($this->gconf->AutoCreateAccount) ) {
          // Automatic User Creation
          include_once "func.AutoCreateAccount.php";
          $aca = AutoCreateAccount($user, $this->gconf);
          if ( $aca == NULL ) {
              // auto creation failed , not verified
              return(0);
          }
          // fallback to standard user verification
          $passhash = $aca['PASSTAG'];
      } else {
          return(0);
      }
    } else {
      $passhash = $uinfo['password'];
    }        

    $uzv = $this->VerifUserPassword($user,$passwd,$passhash);
    if ( $uzv == 0 ) {
      // passwd not good 
      return(0);
    }
    
    // password verified, now check user type

    $typeok = 0;
    if ( $type == "std" ) {
        if ( $uinfo['status'] != 'request' ) {
            $typeok = 1;
        }
    }
    if ( $type == "adm" ) {
        if ( $uinfo['status'] == 'admin' ) {
            $typeok = 1;
        }
    }

    if ( $typeok == 1 ) {
      // if user verified , put status in session for use with display
      @session_start();
      $_SESSION['USER_STATUS'] = $uinfo['status']; 
      return(1);
    } else {
      return(0);
    }
  }

  function VerifUserPassword($user,$passwd,$passhash) {
    // return 1 if passwd match, 0 otherwise

      if ( strncmp($passhash,"LDAP:",5) == 0 ) {
          // ===============================
          // authenticate user with LDAP
          // ===============================
          if ( preg_match('/(.*)@(.*)\.(.*)/', $user, $rm) != 1 ) {
              // user no match mail
              return(0);
          }
      
          $login = $rm[1];
          $domain = $rm[2];
          $tld = $rm[3];
          
          if ( preg_match('/LDAP:(.*):(.*):(.*)/', $passhash, $rm) != 1 ) {
              // no valid LDAP info
              return(0);
          }

          $ldpserv = $rm[1];
          if ( $rm[2] != "" ) {
              $ldpport = $rm[2];
          } else {
              $ldpport = 389;
          }
          $ldpdn = sprintf($rm[3],$login,$domain,$tld);

          $ldpconn = @ldap_connect($ldpserv, $ldpport);
          // force LDAPv3 ( for openldap )
          ldap_set_option($ldpconn, LDAP_OPT_PROTOCOL_VERSION, 3);
          if ( @ldap_bind ($ldpconn, $ldpdn, $passwd) ) {
              // connected
              @ldap_unbind($ldpconn);
              return(1);
          } else {
              // connect failed
              return(0);
          }
          
      } else {
          // ===============================
          // local SHA1 user authentication
          // ===============================
          $h = sha1($passwd);
          if ( $h == $passhash ) {
              return(1);
          } else {
              return(0);
          }
      }
  }

    function UserInfo($user, $signinfo=false ) {

    $this->RedisConnect();
      
    $uinfo = $this->redis->hgetall("user:".$user);
 
    if ( empty($uinfo) ) {
        return(null);
    } else {
        // add mail
        $uinfo['mail'] = $user;
        if ( strncmp($uinfo['password'], "LDAP:", 5) == 0 ) {
            $uinfo['type'] = "ldap";
        } else {
            $uinfo['type'] = "std";
        }
        // if smsstatus is here, convert json strict to php array
        if ( isset($uinfo['smsstatus']) ) {
            $tmp = $uinfo['smsstatus'];
            $arr = json_decode($tmp, true);
            $uinfo['smsstatus'] = $arr;
        }
        if ( $signinfo ) {
            // fetch user signature info,
            $usigninfo = $this->fetchUserSignInfo($user);
            if ( isset($usigninfo['mobile']) ) {
                $uinfo['signphone'] = $usigninfo['mobile'];
            }
            if ( isset($usigninfo['imageSignature']) ) {
                $uinfo['imageSignature'] = $usigninfo['imageSignature'];
                $uinfo['mimetype'] = $usigninfo['mimetype'];        // is it useful ?
            }
        }
        return($uinfo);
    }
  }

  function UserInfoByUid($uid ) {

    $this->RedisConnect();

    $umel = $this->redis->hget('Uid2Mel', $uid);

    if ( empty($umel) ) {
        return(null);
    } else {
        return($this->UserInfo($umel));
    }
  }

  function UserStatus($user) {

    $this->RedisConnect();

    $ustatus = $this->redis->hget("user:".$user, "status");
    if ( empty($ustatus) ) {
      return('none');
    } else {
      return($ustatus);
    }
    
  }

  function CurrentUserStatus() {

    if ( isset($_SERVER['PHP_AUTH_USER']) ) {
        $user = $_SERVER['PHP_AUTH_USER'];
    } else {
        // if no authenticated user , look user_status in session
        @session_start();
        if ( isset($_SESSION['USER_STATUS']) ) {
            return($_SESSION['USER_STATUS']);
        } else {
            return('none');
        }
    }
    
    $this->RedisConnect();

    $ustatus = $this->redis->hget("user:".$user, "status");
 
    if ( empty($ustatus) ) {
      return('none');
    } else {
      return($ustatus);
    }

  }

  function UpdateUserInfo($uid,$udata) {         // should be refactored to not use uid !!!

    $umel = $this->redis->hget('Uid2Mel', $uid);  
    
    $this->UpdateUserInfoByMel($umel,$udata);
    
    return;
  }

  function UpdateUserInfoByMel($umel,$udata) {    

    $this->RedisConnect();

    $this->redis->hmset("user:".$umel, $udata);
    
    return;
  }

  function UpdateUserPassword($uid, $newpasswd ) {

    $this->RedisConnect();

    $umel = $this->redis->hget('Uid2Mel', $uid);    // should be refactored to not use uid !!!
    
    if ( strncmp("LDAP:", $newpasswd, 5) == 0 ) {
      $h = chop($newpasswd);
    } else {
      $h = sha1(chop($newpasswd));
    }

    $this->redis->hset("user:".$umel, "password", $h);

    return;
  }

  function UpdateUserLogo($uid, $logouploaded ) {

      // DO NOT manage user logo any more !
      return;
  }

  function GetUserLogo($uid, $ofile="") {

      // DO NOT manage user logo any more !
      return(null);
  }

  function DosAttach($did,$uid,$mode ) {

    $this->RedisConnect();

    $umel = $this->redis->hget('Uid2Mel', $uid);    // should be refactored to not use uid !!!
    $this->redis->hset("OwnerRel", $umel."|".$did, $mode);
    $this->redis->sadd("dlist:".$umel, $did);
    $this->redis->sadd("ulist:".$did, $umel);

    return;
  }

  function DosAttach4SignVisa($did,$umel) {

    $this->RedisConnect();

    $uid = $this->redis->hget("user:".$umel, "id"); 
 
    if ( empty($uid) ) {
        // user unknown ,  do nothing
        return;
    }
    $this->DosAttach($did, $uid, "writer");
    
  }

  function DosDestroy($did ) {

    $this->RedisConnect();

    $ul0 = $this->redis->smembers("ulist:".$did);
    
    foreach ($ul0 as $umel) {
        $this->redis->hdel("OwnerRel", $umel."|".$did);
        $this->redis->srem("dlist:".$umel, $did);
    }
    $this->redis->del("ulist:".$did);
    
  }

  function DosDetach($did, $uid) {

    $this->RedisConnect();

    $umel = $this->redis->hget('Uid2Mel', $uid);    // should be refactored to not use uid !!!
    
    $this->redis->hdel("OwnerRel", $umel."|".$did);
    $this->redis->srem("dlist:".$umel, $did);
    $this->redis->srem("ulist:".$did, $umel);

  }

  function VerifyDosAttach($did,$mel ) {

   $this->RedisConnect();

   $mode =  $this->redis->hget("OwnerRel", $mel."|".$did);

   if ( empty($mode) ) {
      return(0);
    } else {
      return(1);
    }
  }

  function GetDateInfo($utyp ) {
    switch($utyp) {
    case 'none':
    case 'std':
      // its normally no longer possible to have
      // unregistered or standard users in signature
      // keep this code if we re-enable such users...
      // in this case, 15 days folder duration is hardcoded....
      $dateahead = time() + 1296000; // 15 days ahead
      $datesel = "no";
      break;
    case 'premium':
    case 'admin':
      // if folder duration is defined in conf , take it
      // otherwise this is 6 months  ( 16070400 sec )
      if ( @$this->gconf->folderDuration > 0 ) {
          $fdur = $this->gconf->folderDuration;
      } else {
          $fdur = 16070400;    // 6 months ahead
      }
      $dateahead = time() + $fdur;
      $datesel = "yes";
      break;
    }
    $ret = array('datelim' => $dateahead, 'datesel' => $datesel);
    return($ret);
  }
 

  //===============================================
  // mgmt user stuff
  //===============================================

    function CreateUserRequest($udata) {                   // no longer used in GN context !!
    // return 0 if not OK
    // return ID if user created

    $this->RedisConnect();

    $nextuid = $this->redis->hincrby("AutoIncrement", "user", 1);
    
    if ( empty($nextuid) ) {
        return(0);
    }
    
    $h = sha1(chop($udata['password']));

    $umel = $udata['mail'];
    unset( $udata['mail']);
    $udata['id'] = $nextuid;
    $udata['password'] = $h;
    $udata['credate'] = date("Y-m-d H:i:s");
    $udata['status'] = 'request';

    $this->redis->hmset("user:".$umel, $udata);
    $this->redis->hset("Uid2Mel", $nextuid, $umel);

    return($nextuid);
  }

  function CreateUserValid($udata) {
    // return 0 if not OK
    // return ID if user created

    $this->RedisConnect();

    $nextuid = $this->redis->hincrby("AutoIncrement", "user", 1);
    
    if ( empty($nextuid) ) {
        return(0);
    }

    if ( strncmp("LDAP:", $udata['password'], 5) == 0 ) {
      $h = chop($udata['password']);
    } else {
      $h = sha1(chop($udata['password']));
    }

    $umel = $udata['mail'];
    unset( $udata['mail']);
    $udata['id'] = $nextuid;
    $udata['password'] = $h;
    $udata['credate'] = date("Y-m-d H:i:s");

    $this->redis->hmset("user:".$umel, $udata);
    $this->redis->hset("Uid2Mel", $nextuid, $umel);
    
    return($nextuid);
  }

  function ValidateUser($uid) {                           // no longer used in GN context !!
    // return 0 if user validated
    // return 1 if user already validated
    // return 2 if non existing user
    // return 3 other error ( should not happen )


    $this->RedisConnect();

    $umel = $this->redis->hget('Uid2Mel', $uid);    // should be refactored to not use uid !!!

    $uinfo = $this->redis->hgetall("user:".$umel);

    if ( empty($uinfo) ) {
      return(2);
    }

    if ( $uinfo['status'] != 'request' ) {
      return(1);
    }

    $this->redis->hset("user:".$umel,"status","std");
    
    return(0);
  }

  function ResetPassword($umail) {                          // no longer used in GN context !!
    // return udata struct with new passwd

    $this->RedisConnect();

    $uinfo = $this->redis->hgetall("user:".$umail);
 
    if ( empty($uinfo) ) {
      // user not found , do nothing
      return(null);
    }

    if ( strncmp("LDAP:", $uinfo['password'], 5) == 0 ) {
      // DO NOT RESET ldap password
      return(NULL);
    }
 
    $uinfo['newpasswd'] = GetRandomString(6);
    $h = sha1($uinfo['newpasswd']);

    $this->redis->hset("user:".$umail,"password",$h);

    return($uinfo);
  }

  //===============================================
  // admin stuff
  //===============================================

  function GetDosListFull() {
    
    $top = $this->gconf->AbsDataDir;
    
    //$this->debug->TimeDebug("DosListFull-start");
    $lst0 = glob($top."/?/?/?/?/*/.struct");
    $lst1 = array();
    
    //$this->debug->TimeDebug("DosListFull-loop");
    foreach ( $lst0 as $dir) {
      $did = $this->GetDIDFromDir($dir);
 
      $dosinf = Folder::getFromDid($did, $this->gconf)->_all_;
      $ucnt = $this->GetUserCntByDID($did);

      $lst1[$did] = array('info' => $dosinf, 'count' => $ucnt);
    }
    //$this->debug->TimeDebug("DosListFull-end");

    return($lst1);
  }

  function GetDosListFullCached() {

    $cfil = $this->gconf->CacheDir."/fulldoslist.data";

    // if cache not here get the real data
    if ( ! file_exists($cfil) ) {
      return($this->GetDosListFull());
    }

    $sr = stat($cfil);
    // if cache too small get the real data
    if ( $sr['size'] < 512 ) { 
      return($this->GetDosListFull());
    }
    // if cache too old get the real data
    $now = date("U");
    $delta = $now - $sr['mtime'];
    if ( $delta > 3600 ) { 
      return($this->GetDosListFull());
    }

    // otherwise get the cache
    $serdata = file_get_contents($cfil);
    $dflist = unserialize($serdata);
    return($dflist);
  }

  function GetUserCntByDID($did) {

      $this->RedisConnect();

      $ulist = $this->redis->smembers("ulist:".$did);
      return(count($ulist));
  }
 
  function GetUserListByDID($did) {

      $this->RedisConnect();

      $ul0 = $this->redis->smembers("ulist:".$did);
      $ulist = array();

      foreach ($ul0 as $umel){
          $uinf0 = $this->UserInfo($umel);
          $ulist[] = $uinf0;
      }
 
      return($ulist);

   }
 
  function GetUserListFull() {

      $this->RedisConnect();

      $tmp0 = $this->redis->keys("user:*");
      
      $ulist = array();

      foreach ($tmp0 as $ukey){
          $umel = substr($ukey, 5);
          $uinf0 = $this->UserInfo($umel);

          // count DOS number for the user
          $dlist = $this->redis->smembers("dlist:".$umel);
          
          $uinf0['cnt'] = count($dlist);

          $ulist[] = $uinf0;
      }
 
      return($ulist);

  }

  function DeleteUserByUid($uid) {

      $this->RedisConnect();

      $umel = $this->redis->hget('Uid2Mel', $uid);    // should be refactored to not use uid !!!

      $this->redis->del("user:".$umel);
      $this->redis->hdel('Uid2Mel',$uid);
 
      return;
  }

  function UpdateUserStatus($uid,$newstatus) {

      $this->RedisConnect();

      $umel = $this->redis->hget('Uid2Mel', $uid);    // should be refactored to not use uid !!!

      $this->redis->hset("user:".$umel,"status",$newstatus);
 
      return;
  }

  function UpdateUserSubscription($uid,$enddate) {        // unused in GN  context
      return;
  }

  function IncreaseUserSubscription($uid,$duration,$nbdays=0) {      // unused in GN  context
      return;
  }

  //===============================================
  // External App
  //===============================================

  function GetXappList() {
      $xapdir = $this->gconf->TopAppDir."/app/xapp";
      $thelist = array();

      
      $do = dir($xapdir);
      while (false !== ($class = $do->read())) {
          if ( $class[0] == '.' || $class == "Ethercalc" ) {
              // kludge but we dont want to promote Ethercal any more
              // but continuing to open existing ones,
              // so we cannot just remove the Ethercalc class
              continue;
          }
          $classfile = $xapdir."/".$class."/class.".$class.".php";
          if ( file_exists($classfile) ) {
              include_once $classfile;
              $obj = new $class($this->gconf);
              
              $thelist[$class] = $obj;
          }
      }
      $do->close();

      return($thelist);
  }
  
  function GetXapp($class) {
      $xapdir = $this->gconf->TopAppDir."/app/xapp";
      
      $classfile = $xapdir."/".$class."/class.".$class.".php";
      if ( file_exists($classfile) ) {
          include_once $classfile;
          $obj = new $class($this->gconf);
          
          return($obj);
      } else {
          return(NULL);
      }
  }

  function DelXapp($xappfile) {

      $xinfo = parse_ini_file($xappfile);
      $xapdir = $this->gconf->TopAppDir."/app/xapp";
      $class = $xinfo['CLASS'];
      $classfile = $xapdir."/".$class."/class.".$class.".php";
      if ( file_exists($classfile) ) {
          include_once $classfile;
          $obj = new $class($this->gconf);
          
          if ( method_exists($obj,"Delete") ) {
              $obj->Delete($xinfo);
          }
          unset($obj);
      }
  }
  
  //===============================================
  // Sign stuff
  //===============================================

  function SignRequest($foldobj, $signparams, $signerlist) {

      // signparams no longer used here....

      if ( is_null($foldobj) || empty($signparams) ) {
          trigger_error("BAD arguments, abort SignRequest");
          return(null);
      }

      // prepare (or fetch) signature info for each signer
      // in case of autosign  this is already here, so test before ....
      foreach($signerlist as $signer ) {
          $sobj = Signature::getExisting($signer, $foldobj, $this->gconf);
          if ( is_null($sobj) ) {
              Signature::create($signer, $foldobj, $this->gconf);    // no need to test return
          }
      }

      // update signature info 
      $signinfo = array(
          "signreqtim" => date("U"),
          "signstatus" => "asked"
      );
      $signinfo = array_merge($signinfo, $signparams);
      $foldobj->update($signinfo);

      // lock the folder
      $foldobj->lock("_SIGN_".GetRandomString(6));
      
      return(null);
  }
  
   
  function SignFinish($foldobj, $signobj, $mode) {          

      if ( is_null($foldobj) ) {            // $signobj allowed to be null in mode delayed
          trigger_error("BAD foldobj arguments, abort SignFinish");
          return(null);
      }
      //  $signobj allowed to be null , and probably useless now  !!!
      //if ( is_null($signobj) and $mode != "delayed" ) {            // $signobj allowed to be null only in mode delayed
      //    trigger_error("BAD signobj arguments, abort SignFinish");
      //    return(null);
      //}

      // specific status  mode for delayed signature, but DO NOT REMOVE signacode !
      if ( $mode == "delayed" ) {
          $foldobj->signstatus = "delayed";
          return;
      }
      
      if ( $mode == "aborted" ) {              // problably useless
          // return to the draft state
          $foldobj->signstatus = "draft";
      }

      if ( $mode == "done" ) {
          $foldobj->signstatus = "done";   
          //$signobj->status = "done";  // done earlier
      }

      if ( $mode == "refused" ) {
          $foldobj->signstatus = "refused";  
          //$signobj->status = "refused";  // done earlier
      }

      $foldobj->globalSignatureStatus();
      
      if ( @$foldobj->signstatus != "done" ) {
          $foldobj->unLock();
      }
      
     return(null);
  }
  
  function SignPartial($foldobj, $num) {

      if ( is_null($foldobj) ) {
          trigger_error("BAD arguments, abort SignPartial");
          return(null);
      }

      // set the signature partial flag
      $foldobj->signpartial = $num;
      return(null);
  }
  
  // register signature SMS secret code in the user,   not on the folder
  // used by multisign stuff
  function UserSignSMS($user, $phone) {

      if ( empty($phone) ) {
          trigger_error("No signerphone, abort SignSMS");
          return(1);
      }
      
      $code = GetRandomDigit(5);

      $acode = array("smscode"=>$code, "signphone"=>$phone, "smstime"=>date("U"));
      $this->UpdateUserInfoByMel($user,$acode);
      
      // SEND THE SMS !!
      // get the sms class
      $sclass = $this->gconf->SmsClass;
      $fclass = $this->gconf->TopAppDir."/app/src/class.".$sclass.".php";
      if ( file_exists($fclass) ) {
          include_once $fclass;
          $obj = new $sclass($this->gconf->SmsParam);
      } else {
          trigger_error("SignSMS : no SMS Class !");
          return(1);
      }          

      $tmp = sprintf("%s : votre code secret pour signature : %s", $this->gconf->name, $code);
      
      $rs = $obj->SendSMS($phone, $tmp);     
      if ( ! is_null($rs) ) {
          // register SMS status into the user for later use
          $this->SetUserSmsStatus($user, $rs);
      }
      
      return(0);
  }

  // verification signature SMS secret code in the user,   not on the folder
  // used by multisign stuff
  function VerifyUserSignCode($user, $scode) {

      if ( empty($user) or empty($scode) ) {
          trigger_error("BAD arguments, abort VerifyUserSignCode");
          return("BADDATA");
      }

      $uinfo = $this->UserInfo($user);
      
      // check 15 minutes 
      $now=time();
      $deltat = $now - $uinfo['smstime'];
      if ( $deltat > 900 ) {
          return("CODEOLD");
      }

      //check the code
      $ocode = $uinfo['smscode'];
      if ( $ocode != $scode ) {
          return("CODEBAD");
      }
      return("OK");
  }
  
  //===============================================
  // Signature Low Level Actions
  //===============================================

  function SignFolder($dosobj, $signobjlist) {

      if ( is_null($dosobj) or count($signobjlist) < 1  ) {
          trigger_error("BAD arguments, abort SignFolder");
          return(array('status'=>"BADDATA",'signlist'=>null));
      }

      $dadir = $dosobj->dadir;

      // dedicated proof management object
      $proofo = new SignProof($this->gconf, $this);
      $newproof = $proofo->NewProof();
      $proofo->AddRequesterInfo($dosobj->signasker, $dosobj->signreqtim);

      // dedicated slip management object
      $slipo = new SignSlip($this->gconf, $this);

      // prepare listhash for further usage  ( slip and proof )
      // will contain hash for every signed files
      $listhash = array();

      // get the signature engine class 
      include_once $this->gconf->TopAppDir."/app/src/class.SignatureWrapper.php";
      $sigengine = new SignatureWrapper();

      // signer array init  , will be used for slip and proof
      $signerlist = array();

      // loop on every signer 
      foreach($signobjlist as $signobj) {

          // fetch signature image if present 
          $sigimg = $signobj->getSignImage();
 
          $signid = $signobj->name;
          if ( empty($signid) or $signid == "none" ) {
              $signid = $signobj->mail;
          }

          // add to signerlist,    will be used for slip and proof...
          $signerinfo = array(
              'Identite' => $signid,
              'Mail' => $signobj->mail,
              'Telephone' => $signobj->phone,
              'AdresseIP' => $signobj->signerip,
              'SmsDate' => $signobj->smsstatus['time'],
              'SmsOperateur' => $signobj->smsstatus['operator']
          );
          $signerlist[] = $signerinfo;
          
          // get the signer ip
          $signerip = $signobj->signerip;
          if ( empty($signerip) ) {
              $signerip = @$_SERVER['REMOTE_ADDR'];  // not working in delayed signature, but should not happen
          }

          $proofo->AddSigner($signerinfo);

          // get the sign list for this signer
          $list2sign = $signobj->signlist;
          if ( empty($list2sign) ) {
              // should probably not happen, there is always a signlist ?
              // but just in case, take the expurged filelist
              $tmplist =  $dosobj->filelist;
              $list2sign = array();
              foreach($tmplist as $file) {
                  $tmp = pathinfo($file);
                  $ext = $tmp['extension'];
                  if ( $ext == "sig" || $ext ==  "asc" || $ext ==  "p7s" || $ext ==  "tsa" || $ext ==  "cms" ) {
                      continue;
                  }
                  $list2sign[] = $file;
              }
          }

          // prepare working list with extension, racine for each file to sign
          $listwork = array();
          foreach($list2sign as $file) {
              $tmp = pathinfo($file);
              $ext = $tmp['extension'];
              $rac = $tmp['filename'];
              $listwork[] = compact('ext', 'rac', 'file');
              # add to listhash if not already done
              if ( ! array_key_exists($file, $listhash) ) {
                  $hash = hash('sha512',file_get_contents($dadir."/".$file));
                  $listhash[$file] = $hash;
              }
          }

          // adjusting listwork to deal with multisign xxxx-sigN.pdf
          // awfull kludge !!
          foreach( $listwork as $wid => $elem) {
              if ( $elem['ext'] == "pdf" or $elem['ext'] == "PDF" ) {
                  $mli0 = glob($dadir."/".$elem['rac']."-sig*".$elem['ext']);
                  if ( count($mli0) > 0 ) {
                      $mli1 = array();
                      foreach($mli0 as $el1) {
                          preg_match("/.*-sig([0-9]*).[pPdDfF]*/", $el1, $mm);
                          $mli1[] = $mm[1];
                      }
                      sort($mli1, SORT_NUMERIC);
                      $lx = end($mli1);
                      $listwork[$wid]['file'] = sprintf("%s-sig%s.%s",$elem['rac'],$lx,$elem['ext']);
                  }
              }
          }

          // loop on files ( except xapp, xlnk, sig, asc ) and call external class to sign
          foreach($listwork as $elem) {
              // init the engine with the extension
              if ( isset($this->gconf->SignEngine[$elem['ext']]) ) {
                  $engine = $this->gconf->SignEngine[$elem['ext']];
              } else {
                  $engine = $this->gconf->SignEngine['DEFAULT'];
              }
              $sigengine->Init($engine);
              $signarg = Array(
                  'fpath' => $dadir."/".$elem['file'],
                  'namid' => $signid,
                  'mail' => $signobj->mail,
                  'phone' => $signobj->phone,
                  'proof' => $newproof,
                  'crtfile' => null,       // certfile/certpass no longer used,  should be removed from api 
                  'crtpass' => null,
                  'sigimg' => $sigimg
              );
              $ret = $sigengine->SignOneFile($signarg);     // should test the result !!

          }

          // set status done for this signer
          $signobj->status = "done";

      }

      // initialise slip infos...
      $slipo->AddValues(Array(
          'signers' => $signerlist,
          'proof' => $newproof )
      );
      $slipo->AddValues(Array(
          'date' => date("Y/m/d-H:i:s"),
          'files' => $listhash)
      );
      // generate slip
      $slipo->InitCerts();
      if ( $dosobj->signercount == 1 ) {
          $bn2 = str_replace("@",".",$signobjlist[0]->mail);
      } else {
          $bn2 = "multiple";
      }
      $brdname = $dadir."/bordereau-".$bn2.".".date("U").".pdf";
      $slipo->Generate($brdname);

      // set proof info
      $proofo->AddValues(Array(
          'Signature' => Array(
              'date' => date("Y/m/d-H:i:s"),
              'fichiers' => $listhash)
      ));
      
      // finalize proof
      $proofo->AddSignCerts();
      $proofo->AddSignVersion();
      $proofo->Finalize();
      
      // signature completed, clear the sign status
      $this->SignFinish($dosobj,null,"done");

      // reuse the original list2sign for the return
      return(array('status'=>"OK",'signlist'=>$list2sign));
  }


  //===============================================
  // external apps ID mapping
  //===============================================

  function ExternalAppMappingAdd($xaid, $did) {
 
      $this->RedisConnect();
      $this->redis->hset("ExternalIdsMap", $xaid, $did);
  }

  function GetRealDID($xaid) {
 
      $this->RedisConnect();
      $realdid = $this->redis->hget("ExternalIdsMap", $xaid);
      if ( empty($realdid) ) {
          return("invalid");
      } else {
          return($realdid);
      }
  }

  function ExternalAppMappingDel($xaid) {
 
      $this->RedisConnect();
      $this->redis->hdel("ExternalIdsMap", $xaid);
  }

  //===============================================
  // utilities
  //===============================================

  private function GetDIDFromDir($dir) {
    if ( preg_match("|.*/(.)/(.)/(.)/(.)/(.*)/\.struct|", $dir, $rm) ) {
      $did = $rm[1].$rm[2].$rm[3].$rm[4].$rm[5];
      return($did);
    } else {
      return(NULL);
    }
  }

  private function RedisConnect() {
      if ( is_null($this->redis) ) {
          $this->redis = new Redis();
          if ( isset($this->gconf->redis_server) and isset($this->gconf->redis_port) ) {
              if ( $this->redis->connect($this->gconf->redis_server, $this->gconf->redis_port) ) {
                  $this->redis->select($this->gconf->redis_base);
                  return(true);
              }
          }
          if ( isset($this->gconf->redis_socket) ) {
              if ( $this->redis->connect($this->gconf->redis_socket) ) {
                  $this->redis->select($this->gconf->redis_base);
                  return(true);
              }
          }
          echo "REDIS CONNECTION FAILED !";
          exit();
      } else {
          return(true);
      }
  }

  //===============================================
  // fetch user sign info on gaapse API
  //===============================================

  function fetchUserSignInfo($userid) {

      if ( $this->gconf->gapiurl == "" or $this->gconf->gapikey == "" ) {
          // no gaapse API defined
          return(false);
      }

      $tmp = explode('@', $userid);
      $adata = array("login"=>$tmp[0], "organizationName"=>$tmp[1]);
      $jdata = json_encode($adata);

      $headers = array("accept: application/json", "X-Api-key: ".$this->gconf->gapikey);
      
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $this->gconf->gapiurl);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $jdata);
      
      $resp = curl_exec($ch);
      curl_close($ch);

      $jresp = json_decode($resp, true, 16, JSON_INVALID_UTF8_IGNORE );

      if ( $jresp['code'] == 200 and isset($jresp['results']) ) {
          return($jresp['results']);
      } else {
          return(null);
      }
      
  }
  

    
  //===============================================
  // verify software installation
  //===============================================

   function VerifyInstall() {
       global $topdir;
       
       $retdata = array();

       $ind = 1;
       $countbad = 0;    //    normaly 0


       # --- redis config ( non initialized sample config file ) ----
       if ( $this->gconf->redis_base == 9999 ) {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>redis server non configuré</b>, vérifiez la configuration dans ./config/Config.php");
           $this->redis = null;
           $countbad++;
       } else {

           # --- redis server ----
           try {
               $this->RedisConnect();
               $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"redis server ok");
           } catch(Exception $e) {
               $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>redis server inaccessible</b>, vérifiez que le serveur redis est bien démarré, et correctement configuré dans ./config/Config.php");
               $this->redis = null;
               $countbad++;
           }
       }
       $ind++;
       
       # --- initial user ----
       if ( $this->redis ) {
           $ulist = $this->redis->keys("user:*");
           if ( count($ulist) >= 1 ) {
               $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"compte initial ok");
           } else {
               $initdata = array("id"=>1,
                                  "name"=>"Admin",
                                  "gvname"=>"Joe",
                                  "status"=>"admin",
                                  "password"=>"594c39167fb74b8b65f833efc76601521e5988da");
               $this->redis->hmset("user:admin@some.where", $initdata);
               $this->redis->hset("Uid2Mel", 1, "admin@some.where");
               $this->redis->hset("AutoIncrement", "user", 1);
               $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"compte admin initial créé = admin@some.where / admin007");
               $countbad++;
           }   
           $ind++;
       }
       
       # --- writable ./data ----
       $fp = fopen($this->gconf->TopAppDir."/data/.test.txt", 'w');
       if ( $fp ) {
           fwrite($fp, "test\n");
           fclose($fp);
           unlink($this->gconf->TopAppDir."/data/.test.txt");
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"repertoire ./data ok");
       } else  {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>repertoire ./data non écrivable</b>, réglez les droits pour l'utilisateur www-data");
           $countbad++;
       }
       $ind++;
       
       # --- writable ./proof ----
       $fp = fopen($this->gconf->TopAppDir."/proof/.test.txt", 'w');
       if ( $fp ) {
           fwrite($fp, "test\n");
           fclose($fp);
           unlink($this->gconf->TopAppDir."/proof/.test.txt");
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"repertoire ./proof ok");
       } else  {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>repertoire ./proof non écrivable</b>, réglez les droits pour l'utilisateur www-data");
           $countbad++;
       }
       $ind++;
       
       # --- writable ./tmp/cache ----
       $fp = fopen($this->gconf->TopAppDir."/tmp/cache/.test.txt", 'w');
       if ( $fp ) {
           fwrite($fp, "test\n");
           fclose($fp);
           unlink($this->gconf->TopAppDir."/tmp/cache/.test.txt");
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"repertoire ./tmp/cache ok");
       } else  {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>repertoire ./tmp/cache non écrivable</b>, réglez les droits pour l'utilisateur www-data");
           $countbad++;
       }
       $ind++;
       
       # --- writable ./tmp/spool ----
       $fp = fopen($this->gconf->TopAppDir."/tmp/spool/.test.txt", 'w');
       if ( $fp ) {
           fwrite($fp, "test\n");
           fclose($fp);
           unlink($this->gconf->TopAppDir."/tmp/spool/.test.txt");
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"repertoire ./tmp/spool ok");
       } else  {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>repertoire ./tmp/spool non écrivable</b>, réglez les droits pour l'utilisateur www-data");
           $countbad++;
       }
       $ind++;
       
       # --- writable ./tmp/spoolsign ----
       $fp = fopen($this->gconf->TopAppDir."/tmp/spoolsign/.test.txt", 'w');
       if ( $fp ) {
           fwrite($fp, "test\n");
           fclose($fp);
           unlink($this->gconf->TopAppDir."/tmp/spoolsign/.test.txt");
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"repertoire ./tmp/spoolsign ok");
       } else  {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>repertoire ./tmp/spoolsign non écrivable</b>, réglez les droits pour l'utilisateur www-data");
           $countbad++; 
       }
       $ind++;
       
       # --- incrontab ----
       $comm = sprintf("incrontab -l | fgrep -q %s", $this->gconf->TopAppDir);
       system($comm, $retval);
       if ( $retval == 0 ) {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"incrontab ok");
       } else  {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>incrontab non paramétré</b>, utilisez le script ./app/script/initincrontab avec l'utilisateur www-data ( préalablement autorisé dans /etc/incron.allow )");
           $countbad++; 
       }
       $ind++;

       # --- presence certificat : CMS  
       include_once $topdir."/config/SignCmsConfig.php";
       $cmsconfig = new SignCmsConfig;
       if ( file_exists($cmsconfig->defcert) and file_exists($cmsconfig->defpass) and file_exists($cmsconfig->rootca) ) {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"certificat signature ok");
       } else {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>certificat signature non paramétré</b>, vérifiez que le certificat est bien renseigné dans ./config/SignCmsConfig.php");
           $countbad++; 
       }
       $ind++;
           
       # --- presence certificat : PADES 
       $comm0=sprintf("grep ^CERT %s/config/SignPadesConfig.php", $this->gconf->TopAppDir);
       $pd = popen($comm0, "r");
       $padconf0 = fgets($pd, 2000);
       pclose($pd);
       $padconf1 = trim(substr($padconf0,5));
       if ( file_exists($padconf1) ) {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"certificat PADES ok");
       } else {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>certificat signature PADES non paramétré</b>, vérifiez que le certificat est bien renseigné dans ./config/SignPadesConfig.php");
           $countbad++; 
       }
       $ind++;
           
        # --- presence certificat : TSA  
       include_once $topdir."/config/SignTsaConfig.php";
       $tsaconfig = new SignTsaConfig;
       if ( isset($tsaconfig->default['type']) and file_exists($tsaconfig->default['rootca']) ) {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"certificat ou webservice TSA ok");
       } else {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>certificat ou webservice TSA non paramétré</b>, vérifiez que la configuration dans ./config/SignTsaConfig.php");
           $countbad++; 
       }
       $ind++;
           
       # --- presence java  
       $comm = sprintf("type java >/dev/null 2>&1");
       system($comm, $retval);
       if ( $retval == 0 ) {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"java ok");
       } else  {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>java non installé</b>, installer java / openjdk");
           $countbad++; 
       }
       $ind++;
           
       # --- presence qrencode  
       if ( file_exists("/usr/bin/qrencode") ) {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"OK", 'title'=>"qrencode ok");
       } else {
           $retdata[sprintf('%03d',$ind)] = array('status'=>"KO", 'title'=>"<b>qrencode non installé</b>");
           $countbad++; 
       }
       $ind++;
           
   
       # --- final score
       if ( $countbad > 0 ) {
           $retdata['global'] = false;
       } else {
           $retdata['global'] = true;
       }
       return($retdata);
   }

    
    //===============================================
    // multi folder sign
    // manage both sign and refuse actions
    //
    // this function is probably misplaced, too high level
    // for data class, but it is not worth to create
    // a dedicated class for just that
    //===============================================

    function MultiActionFolders($signlist, $refuselist, $signerinfo, $comment) {  

        // echo "<pre>\n";
        // print_r($signerinfo);
        // echo "</pre>\n";
        // exit(0);

        $uinfo = $this->UserInfo($_SERVER['PHP_AUTH_USER']);

        // extract tree info folders/action/files from flat list
        $folderfileinfo = array();
        foreach($signlist as $elem) {
            list($did,$file) = explode('@',$elem);
            if ( ! @is_array($folderfileinfo[$did] ) ) {
                $folderfileinfo[$did] = array("sign"=>array(), "refuse"=>array());
            }
            array_push($folderfileinfo[$did]['sign'], $file);
        }
        foreach($refuselist as $elem) {
            list($did,$file) = explode('@',$elem);
            if ( ! @is_array($folderfileinfo[$did] ) ) {
                $folderfileinfo[$did] =  array("sign"=>array(), "refuse"=>array());
            }
            array_push($folderfileinfo[$did]['refuse'], $file);
        }
            
        // trigger signature for each folder
        $cntsign = 0;
        $cntrefuse = 0;
        foreach($folderfileinfo as $did => $fdata) {

            $countsign4folder = 0;
            $countrefuse4folder = 0;
            $fobj = Folder::getFromDid($did, $this->gconf);

            // update signature info for each folder
            $sobj = Signature::getExisting($signerinfo['mail'], $fobj, $this->gconf);
            if ( is_null($sobj) ) {
                // unknown signer
                continue;
            }
            $sobj->update($signerinfo);
            
            // update also the signature image
            $usigninfo = $this->fetchUserSignInfo($signerinfo['mail']);
            $sobj->initSignImage($signerinfo['sigimg'], $usigninfo);

            if ( count($fdata['sign']) >=1 ) {
                $flist = $fdata['sign'];
            
                // register the list for later use
                $sobj->signlist = $flist;
                $cntsign += count($flist);
                $countsign4folder += count($flist);     

                // register the signer IP for later use
                $sobj->signerip = $_SERVER['REMOTE_ADDR'];

                // register the sms status for later use
                $sobj->smsstatus =  $uinfo['smsstatus'];

            }
            
            if ( count($fdata['refuse']) >=1 ) {
                $flist = $fdata['refuse'];

                $sobj->refuselist = $flist;
                $cntrefuse += count($flist);
                $countrefuse4folder += count($flist);
                 
                // re-use the fobj initialised at the beginning of the loop
                // in case of the delayed action occurs before !

                // add file list to comment
                $comm2 = $comment."  Pour les fichiers : ".implode(",",$flist);
                $sobj->comment = $comm2;
            }

            if ( $countsign4folder > 0 and $countrefuse4folder == 0 ) {
                $action = "sign";
            } elseif ( $countsign4folder == 0 and $countrefuse4folder > 0 ) {
                $action = "refuse";
            } elseif ( $countsign4folder > 0 and $countrefuse4folder > 0 ) {
                $action = "both";
            } else {
                $action = "none";    // should not happen
            }

            $sobj->update(array("action" => $action));

            // sms code has already been verified in Mgmt/MultiSignFinal
            // so call trigger with null code
            $ret = $sobj->signatureTrigger(null);

        }
        $r = array("signed"=>$cntsign, "refused"=>$cntrefuse);
        return($r);

    }

    //======================================================
    // multi folder zip
    // prepare and send recursive zip for a given folderlist
    //======================================================

    function MultiFoldersZip($folderlist) {


        // prepare the file-tree
        $topzdir = sprintf("/tmp/dossiers_%s", date("U"));
        mkdir($topzdir);

        foreach($folderlist as $did) {
            $subdir = sprintf("%s/%s", $topzdir, $did);
            mkdir($subdir);
            $f0 = Folder::getFromDid($did, $this->gconf);

            $src = $f0->absdir;
            $comm = sprintf("cp %s/* %s",$src, $subdir);
            system($comm);
            $ftit = sprintf("%s/titre.txt", $subdir);
            $fd = fopen($ftit, "w");
            $title = $f0->title;
            fwrite($fd, $title."\n");
            fclose($fd);

        }

        // run and send the zip
        $cmd = sprintf("cd %s; zip -rq -x '*/index.html' - *", $topzdir);
        
        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=".basename($topzdir).".zip");
        header("Content-Description: File");
        
        $pn=popen($cmd , "r"); 
        fpassthru($pn); 
       
        // purge file-tree
        $comm = sprintf("rm -rf %s",$topzdir);
        system($comm);
       
    }

    
  //===============================================
  // end
  //===============================================

    
}

