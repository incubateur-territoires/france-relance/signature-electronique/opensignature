<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir . "/app/src/class.Debug.php";

class Contacts {

  protected $gconf;
  protected $debug;
  protected $redis;

  public function __construct($conf) {
    $this->gconf = $conf;
    $this->debug = new Debug();
    $this->redis = NULL;
  }

  public function getUserContacts($user) {

    $this->RedisConnect();

    $contacts = $this->redis->hgetall("contacts:" . $user);

    return $contacts;
  }

  public function getUserContactsForBootstrapTableAjax($user) {
    $contacts = $this->getUserContacts($user);
    $contactsList = array_map( function($k, $v ) {      
      return ['cname' => $v, 'cmail' => $k];
    }, array_keys($contacts), array_values($contacts));
    $total = count($contactsList);
    header('Content-type: application/json');
    return json_encode(
        ['total' => $total,
        'totalNotFiltered' => $total,
        'rows' => $contactsList
      ]);
  }


  public function addContactIfNeeded($vars) {
    if (
      !empty($vars['CONTACT_ADD']) && $vars['CONTACT_ADD'] === 'contact_add' && !empty($vars['SIGNASKR']) 
      && !empty($vars['SIGNMEL']) && !empty($vars['CONTACT_NAME'])
      ) {
      $this->addUserContactbyMel($vars['SIGNASKR'], $vars['SIGNMEL'], $vars['CONTACT_NAME']);
    }
  }

  public function addUserContact($uid, $cmail, $cname) {
    $this->RedisConnect();
    $umel = $this->redis->hget('Uid2Mel', $uid);
    $addRes = $this->addUserContactbyMel( $umel, $cmail, $cname);
    return $addRes;
  }

  public function addUserContactbyMel($umail, $cmail, $cname) {
    $this->RedisConnect();
    $addRes =  $this->redis->hset("contacts:" . $umail, $cmail, $cname);

    return $addRes;
  }
  
  public function deleteUserContact($uid, $cmail) {

    $this->RedisConnect();
    $umel = $this->redis->hget('Uid2Mel', $uid);

    $delRes =  $this->redis->hdel("contacts:" . $umel, $cmail);
    return $delRes;
  }

  private function RedisConnect() {
    if (is_null($this->redis)) {
      $this->redis = new Redis();
      if (isset($this->gconf->redis_server) and isset($this->gconf->redis_port)) {
        if ($this->redis->connect($this->gconf->redis_server, $this->gconf->redis_port)) {
          $this->redis->select($this->gconf->redis_base);
          return (true);
        }
      }
      if (isset($this->gconf->redis_socket)) {
        if ($this->redis->connect($this->gconf->redis_socket)) {
          $this->redis->select($this->gconf->redis_base);
          return (true);
        }
      }
      echo "REDIS CONNECTION FAILED !";
      exit();
    } else {
      return (true);
    }
  }
}
