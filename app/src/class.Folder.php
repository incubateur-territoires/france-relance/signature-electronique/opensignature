<?php

//===============================================
// Signature Folder Management Object 
//
// d.roche@girondenumerique.fr
// initial version : 20230419
//===============================================

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/lib.LowLevelUtils.php";
include_once $topdir."/app/src/class.Signature.php";
include_once $topdir."/app/src/class.Data.php";

class Folder {

    private $data;      # private necessaire pour surcharge de fonction !!
    private $gconf;
    private $dadir;
    
    public function __construct($conf) {

        // global conf
        $this->gconf = $conf;

        // init to empty array
        $this->data = array();

        // store the folder dir
        $this->dadir = null;

    }

    //===================================================================
    //  automatic getter/setter 
    //===================================================================

    public function __get($name) {
        if ( $name == "_all_" ) {
            return ($this->data);
        } elseif ( $name == "dadir") {
            return ($this->dadir);
        } else {
            return (@$this->data[$name]);
        }
    }
    
    public function __set($name, $val) {
        if ( array_key_exists($name, $this->data) and $this->data[$name] == $val ) {
            // nothing to do
            return;
        }
        $this->data[$name] = $val;
        $this->save();
        if ( $name == " passwd" or $name == "passadm" ) {
            $this->setupHtaccessFile();
        }
    }

    //===================================================================
    // general CRUD  
    //===================================================================

    public function init($did, $mode=0) {
        // mode=0 , no file
        // mode=1 , all files
        // mode=2 , just file to sign

        if ( empty($did) ) {
            return(false) ;
        }
        $dadir = $this->GetAbsDirFromDID($did);
        if ( ! file_exists($dadir) ) {
            return(false);
        }
        $this->dadir = $dadir;
        // init at least the did, rdir, absdir
        $this->data['did'] = $did;
        $this->data['rdir'] = $this->GetDirFromDID($did);
        $this->data['absdir'] = $this->gconf->AbsDataDir."/".$this->data['rdir'];
        // displaymode forced in mode sign in this context
        $this->data['dispmode'] = "sign";

        if ( file_exists($dadir."/.struct/data.json") ) {
            // new data format ( 2023 ), juste read the json
            $jparam = json_decode(file_get_contents($dadir."/.struct/data.json"), true);
            $this->data = array_merge($this->data, $jparam);

            // various times
            $st1 = stat($dadir."/.struct/.htaccess");
            $this->data['cretime'] = $st1['ctime'];
            $st2 = stat($dadir."/.struct/data.json");
            $this->data['modtime'] = $st2['ctime'];

            // blog is not stored in data.json
            if ( file_exists($dadir."/.struct/blog") ) {
                $this->data['hasblog'] = 1;
                $this->data['blogcnt'] = $this->fetchBLogCount($dadir);
            } else {
                $this->data['hasblog'] = 0;
                $this->data['blogcnt'] = 0;
            }
                
        } else {
            // old data struct , read all files
            if ( file_exists($dadir."/.struct/title") ) {
                $this->data['title'] = chop(file_get_contents($dadir."/.struct/title"));
            }
            if ( file_exists($dadir."/.struct/passwd") ) {
                $this->data['passwd'] = chop(file_get_contents($dadir."/.struct/passwd"));
            }
            if ( file_exists($dadir."/.struct/comment") ) {
                $this->data['comment'] = file_get_contents($dadir."/.struct/comment");
            }
            if ( file_exists($dadir."/.struct/passadm") ) {
                $this->data['passadm'] = chop(file_get_contents($dadir."/.struct/passadm"));
            }
            if ( file_exists($dadir."/.struct/endoflife") ) {
                $this->data['endoflife'] = chop(file_get_contents($dadir."/.struct/endoflife"));
            } else {
                $this->data['endoflife'] = "2040-12-31";
            }
            if ( file_exists($dadir."/.struct/signature") ) {
                $this->data['signature'] = true;
                $tmp = file($dadir."/.struct/signature");
                $this->data['signertype'] = chop($tmp[0]);
                $this->data['signermail'] = chop($tmp[1]);
                $this->data['signerasker'] = chop($tmp[2]);
                $this->data['signerreqtim'] = chop($tmp[3]);
                $this->data['signerphone'] = chop($tmp[4]);
                $this->data['signername'] = chop($tmp[5]);
            }
            if ( file_exists($dadir."/.struct/signacomm") ) {
                $this->data['signature-comment'] = chop(file_get_contents($dadir."/.struct/signacomm"));
            }
            if ( file_exists($dadir."/.struct/signstatus") ) {
                $this->data['signstatus'] = chop(file_get_contents($dadir."/.struct/signstatus"));
            }
            if ( file_exists($dadir."/.struct/signpartial") ) {
                $this->data['signpartial'] = chop(file_get_contents($dadir."/.struct/signpartial"));
            }
            if ( file_exists($dadir."/.struct/visa") ) {   // old visa struct
                $this->data['visa'] = true;
                $tmp = file($dadir."/.struct/visa");
                $this->data['visa-mail'] = chop($tmp[0]);
                $this->data['visa-asker'] = chop($tmp[1]);
            }
            if ( file_exists($dadir."/.struct/visaskr") ) {   // new visa struct
                $this->data['visa'] = true;
                $tmp = file($dadir."/.struct/visaskr");
                $this->data['visa-asker'] = chop($tmp[0]);
                $tmp0 = glob($dadir."/.struct/visator/*@*");
                $vizator = array();
                foreach($tmp0 as $tmp1) {
                    $vzorsts = chop(file_get_contents($tmp1));
                    if ( $vzorsts == "-" ) {
                        $vizator[] = basename($tmp1);
                    }
                }
                $this->data['visa-mail'] = implode(",", $vizator);
            }
            if ( file_exists($dadir."/.struct/visastatus") ) {
                $this->data['visastatus'] = chop(file_get_contents($dadir."/.struct/visastatus"));
            }
            if ( file_exists($dadir."/.struct/blog") ) {
                $this->data['hasblog'] = 1;
               $this->data['blogcnt'] = $this->fetchBLogCount($dadir);
            } else {
                $this->data['hasblog'] = 0;
                $this->data['blogcnt'] = 0;
            }
            if ( file_exists($dadir."/.struct/orga") ) {
                $this->data['orga'] = chop(file_get_contents($dadir."/.struct/orga"));
            }
            
            if ( file_exists($dadir."/.struct/framadate") ) {
                $this->data['framadate'] = chop(file_get_contents($dadir."/.struct/framadate"));
            }
            
            if ( file_exists($dadir."/.struct/ar") ) {
                $this->data['AR'] = true;
            }
            
            if ( file_exists($dadir."/.logo") ) {
                $this->data['ownlogo'] = true;
            }
            
            if ( file_exists($dadir."/.struct/dispmode") ) {
                $this->data['dispmode'] = chop(file_get_contents($dadir."/.struct/dispmode"));
            } else {
                $this->data['dispmode'] = "std";
            }

        }     // end of reading folder parameter data
            
        // last modif time = max data.json, struct, datapath, blog; work for both data format
        if ( file_exists($dadir."/.struct/blog") ) {
            $r1 = stat($dadir."/.struct/blog");
            $t1 = $r1['mtime'];
        } else {
            $t1 = 0;
        }
        if ( file_exists($dadir."/.struct") ) {
            $r2 = stat($dadir."/.struct");
            $t2 = $r2['mtime'];
        } else {
            $t2 = 0;
        }
        $r3 = stat($dadir);
        $t3 = $r3['mtime'];
        if ( file_exists($dadir."/.struct/data.json") ) {
            $r4 = stat($dadir."/.struct/data.json");
            $t4 = $r4['mtime'];
        } else {
            $t4 = 0;
        }
        $this->data['modified'] = date("Y-m-d", max($t1,$t2,$t3,$t4));
        
        // file list if needed, whatever data format is !
        if ( $mode > 0 ) {
            $this->getFiles($mode);
        }

        return(true);
    }
    
    public function getFiles($mode) {         // may be called from an already existing object
        // mode=1 , all files
        // mode=2 , just file to sign
                
        $flist = array();
                
        if ($dh = opendir($this->dadir)) {
            while (false !== ($entry = readdir($dh))) {
                if (substr($entry,0,1) !="." and $entry != "index.html" ) {
                    if ( $mode == 2 ) {
                        $tmp = pathinfo($entry);
                        $ext = $tmp['extension'];
                        if ( in_array($ext , Array("xapp", "xlnk", "sig", "cms", "asc", "p7s", "tsa")) )  {
                            continue;
                        } else if ( strncmp($entry,"bordereau-",10) == 0 ) {
                            continue;
                        } else {
                            $flist[] = $entry;
                        }
                    } else {
                        $flist[] = $entry;
                    }
                }	
            }
            closedir($dh);
            sort($flist,SORT_NATURAL);
        }
        if ( $mode == 2 ) {
            $this->data['filelist'] = $this->FilterMultiSig($flist);
        } else {
            $this->data['filelist'] = $flist;
        }
    }

    public function update($values) {
        $cnt = 0;
        $hnt = 0;
        foreach( $values as $key => $val ) {
            if ( array_key_exists($key, $this->data) and $this->data[$key] == $val ) {
                continue;
            }
            $this->data[$key] = $val;
            $cnt++;
            if ( $key == " passwd" or $key == "passadm" ) {
                $hnt++;
            }
           
        }
        if ( $cnt ) {
            $this->save();
        }
        if ( $hnt ) {
           $this->setupHtaccessFile();
        }
    }

    public function delItem($key) {
        if ( array_key_exists($key, $this->data) ) {
            unset($this->data[$key]);
            $this->save();
            if ( $key == " passwd" or $key == "passadm" ) {
                $this->setupHtaccessFile();
            }
        }
    }

    public function delItems($keys) {
        $cnt = 0;
        $hnt = 0;
        foreach( $keys as $key ) {
            if ( array_key_exists($key, $this->data) ) {
                unset($this->data[$key]);
                $cnt++;
                if ( $key == " passwd" or $key == "passadm" ) {
                    $hnt++;
                }
            }
        }
        if ( $cnt ) {
            $this->save();
        }
        if ( $hnt ) {
           $this->setupHtaccessFile();
        }
    }

    public function delete() {
        if ( is_dir($this->dadir) ) {
            // ---- wipe all
            system("rm -rf ".$this->dadir);
        }
    }

    public function createStruct($initialData = null) {

        $hh = $this->CreRandomDir();

        $this->dadir = $hh['FHD'];
        // minimum init
        $this->data['did'] = $hh['H'];
        $this->data['rdir'] = $hh['HD'];
        if ( is_null($initialData) ) {
            $this->data['title'] = "empty";
            $this->data['endoflife'] = date("Y-m-d");
        } else {
            $this->data = array_merge($this->data, $initialData);
        }
        
        mkdir($this->dadir."/.struct", 02775);
        $fp = fopen($this->dadir."/.struct/.htaccess", 'w');
        fwrite($fp, "Require all denied\n");
        fclose($fp);
        chmod($this->dadir."/.struct/.htaccess", 0444);
        
        $this->save();

        $urls = URL::GetURLByDID($this->gconf, $hh['H']);
        
        $fp = fopen($this->dadir."/index.html", 'w');
        fwrite($fp, "<html><head>\n");
        fwrite($fp, "<meta http-equiv=\"refresh\" content=\"0; url=".$urls->GetDosMethod('Display')."\" />\n");
        fwrite($fp, "</head></html>\n");
        fclose($fp);
        $this->setupHtaccessFile();
        
    }

    //===================================================================
    // folder files stuff 
    //===================================================================

    public function deleteFiles($dlist) {

        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        foreach ( $dlist as $ind => $value) {
            $dfil = $this->dadir."/".$value;
            $ext = pathinfo($dfil, PATHINFO_EXTENSION);
            unlink($dfil);
        }
    }
    
    public function renameFiles($olist, $nlist) {

        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        foreach ( $nlist as $ind => $value) {
            if ( $value != "" ) {
                $ofil = $this->dadir."/".$olist[$ind];
                $nfil = $this->dadir."/".trim($value);
                rename($ofil,$nfil);
                $cnt += 1;
            }
        }
    }

    // used by the classic(old) upload form  nf1..nf8 variables
    public function AddClassicUploadFiles($vars) {

        $nslot=8;
        $totalret = 0;
        
        for ($i = 1; $i <= $nslot; $i++) {
            $nfi = "nf".$i;
            
            if ( ! array_key_exists($nfi, $vars) ) {
                continue;
            }
            $ret = $this->processUploadedFile($vars[$nfi]);
            $totalret += $ret;
        }
        // $this->spoolUploaded($did);    ## no thumbnail generation in signature context
        return($totalret);
    }

    // used by the JQFU upload form  files variables
    // Jquery File Upload process file 1 by 1 even if input multiple !
    public function AddJQFUFiles($vars) {

        // re-arrange input array for one file
        $finfo = Array();
    
        if ( array_key_exists('files', $vars) ) {
            $tmp = $vars['files'];
            $finfo['name'] = $tmp['name'][0];
            $finfo['type'] = $tmp['type'][0];
            $finfo['tmp_name'] = $tmp['tmp_name'][0];
            $finfo['error'] = $tmp['error'][0];
            $finfo['size'] = $tmp['size'][0];
        }
        $ret = $this->processUploadedFile($finfo);
        if ( $ret == 1 ) {
            //$this->SpoolUploaded($did);   ## no thumbnail generation in signature context
            // return the file info array
            // for XHR respons
            return($finfo);
        } else {
            return(null);
        }
    }
    
    // common file processing ( called by standard form :AddDosFile2
    // or by jquery file upload : AddDosFileJQFU
    // INPUT: filinfo = array with (name,type,tmp_name,error,size)
    // OUTPUT: 1 if file processed, 0 if not
    private function processUploadedFile($filinfo) {

        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }

        if ( $filinfo['size'] > 0 && $filinfo['error'] == 0 ) {
            $dfil = $this->dadir."/".$filinfo['name'];
            
            // UNZIP if name = ___.xxx
            
            if ( strncmp($filinfo['name'],"___.",4) == 0 ) {
                $extn = substr($filinfo['name'], 4);
                switch($extn) {
                case "zip":
                case "ZIP":
                    $cmd = "cd ".$ddir."; unzip ".$filinfo['tmp_name'];
                    echo $cmd;
                    system($cmd);
                    break;
                case "tgz":
                case "TGZ":
                    $cmd = "cd ".$ddir."; tar xzf ".$filinfo['tmp_name'];
                    echo $cmd;
                    system($cmd);
                    break;
                case "tbz2":
                case "TBZ2":
                    $cmd = "cd ".$ddir."; tar xjf ".$filinfo['tmp_name'];
                    echo $cmd;
                    system($cmd);
                    break;
                default:
                    rename($filinfo['tmp_name'], $dfil);
                }
                return(1);
            } else {
                rename($filinfo['tmp_name'], $dfil); 
            }
            // file processed
            return(1);
            
        } else {
            // empty file or error 
            return(0);
        }
        
    }

    public function GenerateAndSendZip($filelist="*") {
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        $cmd = "cd ".$this->dadir.";zip -jq -x index.html - ".$filelist;
        
        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=".$this->data['did'].".zip");
        header("Content-Description: File");
        
        $pn=popen($cmd , "r"); 
        return fpassthru($pn); 
    }

    public function resetFileList() {            //only used once in class.Doc,   is it really usefull ?
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return(null);
        }
        if ( is_array($this->data['filelist']) ) {
            $ret = reset($this->data['filelist']);
        } else {
            $ret = null;
        }
        return($ret); 
    }
  


    //===================================================================
    // locking stuff 
    //===================================================================

    public function lock($passadm) {
    
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        $this->data['passadm'] = $passadm;
        $this->save();
        $this->setupHtaccessFile();
        
    }

    public function unLock() {
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        unset($this->data['passadm']);
        $this->save();
        $this->setupHtaccessFile();
       
    }
    
    //===================================================================
    // blog specific functions 
    //===================================================================

   public function fetchBlog() {
 
        $blist = array();
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return($blist);
        }
    
        $bldir = $this->dadir."/.struct/blog";
        if ( ! is_dir($bldir) ) {
            return($blist);
        }
        
        if ($dh = opendir($bldir)) {
            
            while (false !== ($entry = readdir($dh))) {
                if ($entry != "." && $entry != "..") {
                    $belem = array();
                    $bents = $bldir."/".$entry."/sign";
                    if ( file_exists($bents) ) {
                        $belem['signature'] = file_get_contents($bents);
                    } else {
                        $belem['signature'] = "inconnu";
                    }
                    $bentc = $bldir."/".$entry."/comment";
                    if ( file_exists($bentc) ) {
                        $belem['comment'] = file_get_contents($bentc);
                    } else {
                        $belem['comment'] = "";
                    }	
                    $blist[$entry] = $belem;
                    
                }
            }
            closedir($dh);
            krsort($blist);
        }
        
        return($blist);
        
    }

    public function fetchBlogCount() {
        
        $cnt = 0;
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return($cnt);
        }
        $bldir = $this->dadir."/.struct/blog";
        if ( ! is_dir($bldir) ) {
            return($cnt);
        }
        if ($dh = opendir($bldir)) {
            
            while (false !== ($entry = readdir($dh))) {
                if ($entry != "." && $entry != "..") {
                    $cnt++;
                }
            }
            closedir($dh);
        }
        
        return($cnt);
        
    }
    
    public function activateBlog() {
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        $bldir1 = $this->dadir."/.struct/blog";
        $bldir2 = $this->dadir."/.struct/blog_no";
        
        if ( is_dir($bldir1) ) {
            return ;
        }
        if ( is_dir($bldir2) ) {
            rename($bldir2, $bldir1) ;
            return ;
        }
        mkdir($bldir1, 02775); 
        
    }
  
    // no longer used,  blog is now always activated in signature context
    function deActivateBlog() {
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        $bldir1 = $this->dadir."/.struct/blog";
        $bldir2 = $this->dadir."/.struct/blog_no";
        
        if ( is_dir($bldir2) ) {
            return ;
        }
        if ( is_dir($bldir1) ) {
            rename($bldir1, $bldir2);
            return ;
        }
    }
    
    public function addBlog($signature, $comment) {
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        $bldir = $this->dadir."/.struct/blog";
        if ( ! is_dir($bldir) ) {
            return ;
        }
        
        $nowu = date("U");
        $entdir = sprintf("%s/%s-00",$bldir,$nowu);
        // addBlog may occurs at the same second !  ( multisign case )
        // so ensure previous blog entry will not be erased
        $loop = 1;
        while ( is_dir($entdir) and $loop < 10 ) {
            $entdir =  sprintf("%s/%s-%02u",$bldir,$nowu,$loop);
            $loop++;
        }
        if ( $loop >=10 ) {
            // very unlikely !!
            return;
        }
        
        $entsign = $entdir."/sign";
        $entcomm = $entdir."/comment";
        
        mkdir($entdir, 02775);
        $fp = fopen($entsign, 'w');
        fwrite($fp, $signature);
        fclose($fp);
        $fp = fopen($entcomm, 'w');
        fwrite($fp, $comment);
        fclose($fp);
        
    }
  
    public function delBlogAll()  {
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        $bldir = $this->dadir."/.struct/blog";
        if ( ! is_dir($bldir) ) {
            return ;
        }
        
        system("rm -rf ".$bldir."/*");
        
    }
    
    public function delBlogEntry($entry)  {
        
        if ( is_null($this->dadir) or ! is_dir($this->dadir) ) {
            return;
        }
        $bldir = $this->dadir."/.struct/blog";
        if ( ! is_dir($bldir) ) {
            return ;
        }
        $bedir = $bldir."/".$entry;
        if ( ! file_exists($bedir) ) {
            return ;
        }
        
        system("rm -rf ".$bedir);
        
    }

    //===================================================================
    // internal private functions 
    //===================================================================


    // setup htaccess file for the briefcase datadir
    // for security and DAV access
    // according to passwd or status 
    private function setupHtaccessFile() {

        if ( is_null($this->dadir) ) {
            return;
        }
        
        if ( isset($this->data['passwd']) ) {
            $passwd = $this->data['passwd'];
        } else {
            $passwd = "";
        }
        if ( isset($this->data['passadm']) ) {
            $passadm = $this->data['passadm'];
        } else {
            $passadm = "";
        }

        if ( $passwd != "" ) {
            // generate htpass for dav access
            $cmd = "/usr/bin/htpasswd -cbm ".$this->dadir."/.struct/htpass '*' \"".$passwd."\"";
            //$this->debug->DebugToFile("/tmp/sethtfil.log", $cmd);
            system($cmd);
        }
        
        if ( file_exists($this->dadir."/.htaccess") ) {
            chmod($this->dadir."/.htaccess", 0644);
        }
        $fp = fopen($this->dadir."/.htaccess", 'w');
        
        // in any case de-activate php+pl 
        fwrite($fp, "RemoveHandler .php .phtml .php3 .php4\n");
        fwrite($fp, "RemoveType .php .phtml .php3 .php4\n");
        fwrite($fp, "php_flag engine off\n");
        fwrite($fp, "\n");
        fwrite($fp, "<FilesMatch \.(pl|php|php3)$>\n");
        fwrite($fp, " Require all denied\n");
        fwrite($fp, "</FilesMatch>\n");
        fwrite($fp, "\n");
        
        if ( $passadm != "" ) {
            // locked briefcase, de-activate any DAV modification
            fwrite($fp, "<Limit PUT DELETE MKCOL PROPPATCH COPY MOVE LOCK UNLOCK>\n");
            fwrite($fp, " Require all denied\n");
            fwrite($fp, "</Limit>\n");
            if ( $passwd != "" ) {
                // allow dav read for with passwd
                fwrite($fp, "<Limit PROPFIND OPTIONS>\n");
                fwrite($fp, " AuthType Basic\n");
                fwrite($fp, " AuthName DAV\n");
                fwrite($fp, " AuthBasicProvider file\n");
                fwrite($fp, " AuthUserFile ".$this->dadir."/.struct/htpass\n");
                fwrite($fp, " Require valid-user\n");
                fwrite($fp, "</Limit>\n");
            } else {
                // allow dav read for all
                fwrite($fp, "<Limit PROPFIND OPTIONS>\n");
                fwrite($fp, " Require all granted\n");
                fwrite($fp, "</Limit>\n");
            }
        } else {
            // open briefcase, de-activate only mkcol
            fwrite($fp, "<Limit MKCOL>\n");
            fwrite($fp, " Require all denied\n");
            fwrite($fp, "</Limit>\n");
            if ( $passwd != "" ) {
                // allow dav read/write for with passwd
                fwrite($fp, "<Limit PROPFIND OPTIONS PUT DELETE PROPPATCH COPY MOVE LOCK UNLOCK>\n");
                fwrite($fp, " AuthType Basic\n");
                fwrite($fp, " AuthName DAV\n");
                fwrite($fp, " AuthBasicProvider file\n");
                fwrite($fp, " AuthUserFile ".$this->dadir."/.struct/htpass\n");
                fwrite($fp, " Require valid-user\n");
                fwrite($fp, "</Limit>\n");
            } else {
                // allow dav read/write for all
                fwrite($fp, "<Limit PROPFIND OPTIONS PUT DELETE PROPPATCH COPY MOVE LOCK UNLOCK>\n");
                fwrite($fp, " Require all granted\n");
                fwrite($fp, "</Limit>\n");
            }
        }
        fclose($fp);
        chmod($this->dadir."/.htaccess", 0444);
    }

    private function save() {
        $d2sav = $this->data;
        // those are not to be saved
        unset($d2sav['did']);
        unset($d2sav['rdir']);
        unset($d2sav['modified']);
        unset($d2sav['filelist']);
        
        $jdata = json_encode($d2sav, JSON_PRETTY_PRINT|JSON_INVALID_UTF8_SUBSTITUTE|JSON_UNESCAPED_SLASHES);
        $fp = fopen($this->dadir."/.struct/data.json", 'w');
        fwrite($fp, $jdata);
        fclose($fp);
    }

    //===================================================================
    // real signature processing 
    //===================================================================

    function SignatureProcess() {

        $sigobjlist = $this->getSignatureObjects();

        //  carefull,   this is the data object, not the data array containing folder info
        $dataobj = new Data($this->gconf);  

        $ret2 = $dataobj->SignFolder($this, $sigobjlist);

        if ( $ret2['status'] != "OK" ) {
            return;
        }

        $signed = "\n\n";
        foreach($ret2['signlist'] as $elem ) {
            $signed .= $elem."\n";
        }

        if ( $this->data['signercount'] > 1 ) {
            $signer = "signataires multiples";
        } else {
            $signer = $sigobjlist[0]->mail;
        }
        $this->addBlog($signer,'<i class="fas fa-signature blog-ok"></i>&nbsp;Signature effectuée ! sur les fichiers suivants :'.$signed);

        // prepare the mail file 
        $urls = URL::GetURLByFolder($this->gconf, $this);
        $tpl = new Savant3();
        $tpl->assign("URL", $urls);
        $tpl->assign("DOSINFO", $this->_all_ );
        $tpl->assign("APPNAM", $this->gconf->name );
        
        $tmpfname = tempnam("/tmp", "CSXsign");
        $fm = fopen($tmpfname, "a");
        fwrite($fm,$tpl->fetch("tpl/doc/mail_sign_done.html"));
        fclose($fm);
        
        $msubj = "Signature effectuée ! : ".$this->data['title'];

        // send the mail to the asker AND all the signer

        if ( ! is_null($this->data['signasker']) ) {
            $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$this->data['signasker']." \"".$msubj."\" ".$tmpfname;
            system(escapeshellcmd($launch));
        }
        foreach( $sigobjlist as $sobj ) {
            $signer = $sobj->mail;
            $launch=$this->gconf->TopAppDir."/app/script/melsigsnd ".$signer." \"".$msubj."\" ".$tmpfname;
            system(escapeshellcmd($launch));
        }

        
    }

    function SignatureAbort() {
        $this->removeAllSignature();

        // message  
        $this->addBlog($this->gconf->name,"Annulation demande de signature");
    }


    //===================================================================
    // static fetcher/creation functions
    //===================================================================

    static public function getFromDid($did, $conf, $mode=0) {
        $fobj = new Folder($conf);
        if ( $fobj->init($did, $mode) ) {
            return($fobj);
        } else {
            return(null);
        }
    }
    
    static public function getFromUrl($url, $conf) {
        $fobj = new Folder($conf);

        $pattern = "|.*/([0-9a-zA-Z]{32})$|";
        $ret = preg_match($pattern, $ustr, $ra);
        if ( $ret != 1 ) {
            // not a valid url
            return null;
        }

        if ( $fobj->init($ra[1]) ) {
            return($fobj);
        } else {
            return(null);
        }
    }
    
    static public function create($conf, $initialData = null) {
        $fobj = new Folder($conf);      // normaly never fails...
        $fobj->createStruct($initialData);
        $fobj->activateBlog();    // always now
        return($fobj);  
    }

    //===================================================================
    // signature struct management
    //===================================================================

    public function getSignatureList() {

        $list0 = glob($this->dadir."/.struct/sign_*.json");
        $list1 = array();
        foreach($list0 as $elem) {
            $list1[] = substr(basename($elem), 5, -5);
        }
        return($list1);
            
    }
    
    public function getSignatureObjects() {

        $list0 = $this->getSignatureList();
        $list1 = array();
        foreach($list0 as $elem) {
            $list1[] = Signature::getExisting($elem, $this, $this->gconf );
        }
        return($list1);
        
    }

    public function removeAllSignature() {

        $cmd = "rm ".$this->dadir."/.struct/sign_*.json ".$this->dadir."/.struct/signimage_*";
        system($cmd);
        unset($this->data['signreqtim']);
        unset($this->data['signercount']);
        unset($this->data['signasker']);
        unset($this->data['signacomm']);
        unset($this->data['passadm']);
        $this->data['signstatus'] = "draft";
        $this->save();
    }

    public function globalSignatureStatus() {
        // return true if all signer have responded, false otherwise

        $sobjs = $this->getSignatureObjects();
        $statuses = array('done'=>0, 'refused'=>0, 'delayed'=>0);
        $count = 0;
        foreach($sobjs as $sobj ) {
            $s0 = $sobj->status;
            if ( isset($s0) ) {
                $statuses[$s0] +=1;
                $count++;
            }
        }
        if ( $count < $this->data['signercount'] ) {
            // all signer not finished, do not adjust global signature status
            return(0);
        }

        // calculate global status
        if ( $statuses['delayed'] != 0 ) {
            // if at least one delayed, the global is delayed
            $this->data['signstatus'] = "delayed";
        } elseif ( $statuses['refused'] != 0 ) {
            // if at least one refused, the global is refused,   may be stupid 
            $this->data['signstatus'] = "refused";
        } else {
            $this->data['signstatus'] = "done";
        }
        $this->save();

        return(1);  
       
    }

   //===================================================================
    // miscellaneous 
    //===================================================================

    private function FilterMultiSig($list) {
        // if found multiple signed pdf,   keep the oldest

        $frac = array();
        
        foreach( $list as $elem ) {
            
            $ext = pathinfo($elem, PATHINFO_EXTENSION);
            if ( strtolower($ext) != "pdf" ) {
                $frac[$elem] = $elem;
            } else {
                
                $bn = pathinfo($elem, PATHINFO_FILENAME);
                $pos = strpos($bn, "-sig");
                if ( $pos ) {
                    $rac = substr($bn, 0, $pos);
                } else {
                    $rac = $bn;
                }
                if ( array_key_exists($rac, $frac) ) {
                    $st1 = stat($this->dadir."/".$frac[$rac]);
                    $mtim1 = $st1['mtime'];
                } else {
                    $mtim1 = 0;
                }
                $st2 = stat($this->dadir."/".$elem);
                $mtim2 = $st2['mtime'];
                // keep the oldest
                if ( $mtim2 > $mtim1 ) {
                    $frac[$rac] = $elem;
                }
            }
            
        }
      
        return(array_values($frac));

    }

    //===================================================================
    // low level directory/DID
    //===================================================================

    private function CreRandomDir() {

        $ht = array();
        $cnt = 0;

        $r20 = GetRandomString(32);
        $tmp = $this->GetAbsDirFromDID($r20);
        
        while ( file_exists($tmp) and $cnt < 10 ) {
            
            $r20 = GetRandomString(32);
            $tmp = $this->GetAbsDirFromDID($r20);
            $cnt += 1;
        }
        if ( $cnt >= 10 ) {
            echo "Random generator exhausted ! aborting....\n";
            exit(1);
        }
        
        $ht['H'] = $r20;
        $ht['HD'] = $this->GetDirFromDID($r20);
        $ht['FHD'] = $tmp;
        mkdir($ht['FHD'], 02775, 1);
        
        return ($ht);
    }

    private function GetDirFromDID($did) {
        $tdd = substr($did,0,1)."/".substr($did,1,1)."/".substr($did,2,1)."/".substr($did,3,1)."/".substr($did,4);
        return($tdd);
    }
    
    private function GetAbsDirFromDID($did) {
        return( $this->gconf->AbsDataDir."/".$this->GetDirFromDID($did) );
    }
    

    //===============================================
    // end
    //===============================================
}

   
