<?php
/**
 * First, download the latest release of PHP wrapper on github
 * And include this script into the folder with extracted files
 */

class Sms_Dumb {

    var $logfile;
    var $ovh;
    
    function __construct($param) {
        $this->logfile = "/tmp/smssign.log";
        //$this->logfile = null;

        $this->operator = "Bidon";
        $this->param = $param['dumb'];     // useless, juste for debug

    }

    function SendSMS( $phone, $message ) {

        $tmp = $this->param."|".$phone."|".$message;
        
        $this->Log($tmp);
        
        // this always works !
        $retval = Array(
            'time' => date("Y/m/d-H:i:s"),
            'tel' => $phone,
            'operator' => $this->operator) ;
        return($retval);
    }
 
    function Log($msg) {
        if ( isset($this->logfile) ) {
            $fd = fopen($this->logfile, "a+");
            fwrite($fd, $msg);
            fwrite($fd, "\n");
            fclose($fd);
        }
    }

}

