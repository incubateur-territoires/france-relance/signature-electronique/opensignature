<?php

//===============================================
// Low Level miscellaneous data utilities
//===============================================


function GetRandomString($len) {
    $chartab = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return (substr(str_shuffle($chartab), 0, $len));
}
    
function GetRandomDigit($len) {
    $chartab = "012345678901234567890123456789";
    return (substr(str_shuffle($chartab), 0, $len));
}
    
function GetUserStatuses($full=0) {
    if ( $full ) {
        // return array("request","std","premium","admin");
        // std user status is no longer used !
        return array("request","premium","admin");
    } else {
        // return array("std","premium","admin");
        // std user status is no longer used !
        return array("premium","admin");
    }
}

function NormalizeName($str) {
    $badchars = array("/","\\","&","*");
    return str_replace($badchars,"_",$str);
}

// from official php base64 doc 
function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}
// from official php base64 doc 
function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

// extended ID stuff only used by podoc,  not signature....
function encode_xid($data) {
    preg_match("|(.*):(.*)|", $this->gconf->xidkiv, $rv);
    list($dumb,$k,$iv) = $rv;
    $res = $this->base64url_encode(openssl_encrypt($data, "aes-128-cbc", $k, true, $iv));
    return($res);
}

function decode_xid($data) {
    preg_match("|(.*):(.*)|", $this->gconf->xidkiv, $rv);
    list($dumb,$k,$iv) = $rv;
    $res = openssl_decrypt($this->base64url_decode($data), "aes-128-cbc", $k, true, $iv);
    return($res);
}

