<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.Data.php";
include_once $topdir."/app/src/class.DataProof.php";

class Proof {

  //===============================================
  // framework
  //===============================================

  var $public_functions;
  var $gconf;

  function __construct($conf) { 
    $this->gconf = $conf;
    $this->data = new Data($conf);
    $this->dataproof = new DataProof($conf);
    $this->topdir = dirname(dirname(__DIR__));

    if ( $this->gconf->sso == "openidc" ) {
        include_once $this->topdir."/app/src/class.OpenIDcClient.php";
        $this->ssoc = new OpenIDcClient($this->gconf, $this->data);
    }
    
   }

  //===============================================
  // web app
  //===============================================

  function Index($vars) {


      $tpl = new Savant3();
      $tpl->assign("APPNAM", $this->gconf->name);
      $tpl->assign("PLATURL", $this->gconf->SignPlatformUrl);
      $tpl->assign('SHOW_MODAL', false);
      $tpl->display("tpl/proof/index_dsfr.html");
  }

  function EnterRef($vars) {

      $this->AskAuth();

      $tpl = new Savant3();
      $tpl->assign("APPNAM", $this->gconf->name);
      $tpl->assign("PLATURL", $this->gconf->SignPlatformUrl);
      $tpl->assign('SHOW_MODAL', true);

      $tpl->display("tpl/proof/index_dsfr.html");
  }

  function GetProof($vars) {
      
      $this->AskAuth();

      header("location: /prf/Show/".$vars['REFPROOF']);
  }

  function Show($vars) {
     
      $this->AskAuth();

      //$this->debug($vars);
      //exit(0);

      $proof = $this->dataproof->GetProof($vars['REFPROOF']);

      if ( empty($proof) ) {
      
          $tpl = new Savant3();
          $tpl->assign("APPNAM", $this->gconf->name);
          
          $tpl->display("tpl/proof/badref.html");

      } else {

          $tpl = new Savant3();
          $tpl->assign("APPNAM", $this->gconf->name);

          $tpl->assign("LNK1", $proof['rel_preuve']);
          $tpl->assign("LNK2", $proof['rel_tsa']);
          $tpl->assign("REFP", $vars['REFPROOF']);

          
          
          $tpl->display("tpl/proof/page.html");
      }

  }

  function DownProof($vars) {
      
      $this->AskAuth();

      $proof = $this->dataproof->GetProof($vars['REFPROOF']);

      if ( empty($proof) ) {
          exit(0);
      }

      $filename = $vars['REFPROOF'].".json";
      $filepath = $proof['abs_preuve'];
      
      header("MIME-Version: 1.0");
      header("Expires: Sat, 01 Jan 2000 05:00:00 GMT");        // date in the past
      header("Last-Modified:".date("D, d M Y H:i:s")." GMT");  // always modified
      header("Cache-Control: no-cache, must-revalidate");      // HTTP/1.1
      header("Pragma: no-cache");                              // HTTP/1.0
      header("Content-type: application/download");   
      header("Content-Disposition: attachment; filename=\"$filename\"");
      header("Content-Description: File"); 
      
      $fn=fopen($filepath , "r"); 
      return fpassthru($fn); 
      
      exit(0);
  }

  function DownTSA($vars) {
      
      $this->AskAuth();

      $proof = $this->dataproof->GetProof($vars['REFPROOF']);

      if ( empty($proof) ) {
          exit(0);
      }

      $filename = $vars['REFPROOF'].".tsa";
      $filepath = $proof['abs_tsa'];
      
      header("MIME-Version: 1.0");
      header("Expires: Sat, 01 Jan 2000 05:00:00 GMT");        // date in the past
      header("Last-Modified:".date("D, d M Y H:i:s")." GMT");  // always modified
      header("Cache-Control: no-cache, must-revalidate");      // HTTP/1.1
      header("Pragma: no-cache");                              // HTTP/1.0
      header("Content-type: application/download");   
      header("Content-Disposition: attachment; filename=\"$filename\"");
      header("Content-Description: File"); 
      
      $fn=fopen($filepath , "r"); 
      return fpassthru($fn); 
      
      exit(0);
  }

  function DownCA($vars) {
      
      $this->AskAuth();

      $proof = $this->dataproof->GetProof($vars['REFPROOF']);

      if ( empty($proof) ) {
          exit(0);
      }

      $filename = $vars['REFPROOF']."_ac.pem";
      $filepath = $proof['abs_ca'];
      if ( ! file_exists($filepath) ) {
          // old proof generated without CA
          // use the internal TSA rootCA  ( this should be in global conf )
          $filepath = $this->gconf->internalTsaCA;
      }
      
      header("MIME-Version: 1.0");
      header("Expires: Sat, 01 Jan 2000 05:00:00 GMT");        // date in the past
      header("Last-Modified:".date("D, d M Y H:i:s")." GMT");  // always modified
      header("Cache-Control: no-cache, must-revalidate");      // HTTP/1.1
      header("Pragma: no-cache");                              // HTTP/1.0
      header("Content-type: application/download");   
      header("Content-Disposition: attachment; filename=\"$filename\"");
      header("Content-Description: File"); 
      
      $fn=fopen($filepath , "r"); 
      return fpassthru($fn); 
      
      exit(0);
  }

  function ShowProof($vars) {
      
      $this->AskAuth();

      $proof = $this->dataproof->GetProof($vars['REFPROOF']);

      if ( empty($proof) ) {
          exit(0);
      }

      $pdata = json_decode(file_get_contents($proof['abs_preuve']),true);

      $tpl = new Savant3();
      $tpl->assign("APPNAM", $this->gconf->name);
      
      $tpl->assign("REFP", $proof['ref']);
      $tpl->assign("PDATA", $pdata);
      $tpl->display("tpl/proof/showprf.html");
     
  }

  function ShowTSA($vars) {
      
      $this->AskAuth();

      $proof = $this->dataproof->GetProof($vars['REFPROOF']);

      if ( empty($proof) ) {
          exit(0);
      }

      $tmpres1 = tempnam("/tmp", "spts");

      $action = sprintf("openssl ts -reply -in %s -token_in -text 2>/dev/null | sed -e 's|\\\\xC3\\\\xA9|é|g' > %s", $proof['abs_tsa'], $tmpres1);
      system($action);
      $res1arr = file($tmpres1);
      unlink($tmpres1);

      $tpl = new Savant3();
      $tpl->assign("APPNAM", $this->gconf->name);
      
      $tpl->assign("REFP", $proof['ref']);
      $tpl->assign("TDATA", $res1arr);
      $tpl->display("tpl/proof/showtsa.html");
     
 
  }

  //===============================================
  // admin stuff
  //===============================================

  function SignProofList($vars) {
      
      $this->AskAuth();

      $tpl = new Savant3();
      $tpl->assign("APPNAM", $this->gconf->name);
      $tpl->assign("MYSELF", "SignProofList");

      $pl = $this->dataproof->GetProofList();
      $tpl->assign("PLIST", $pl);
      $pf = $this->GetSavedProofFilter();
      $tpl->assign("PFILTR", $pf);

      $tpl->display("tpl/proof/sigprooflist_dsfr.html");
  }

  function FilterProofSave($vars) {

      //$this->debug->DebugToFile("/tmp/deb.txt", $vars);

      @session_start();
      $_SESSION['FILT_PR']=$vars['PR'];
      $_SESSION['FILT_PU']=$vars['PU'];
      $_SESSION['FILT_PD']=$vars['PD'];
      
      return;
  }

  private function GetSavedProofFilter() {
    @session_start();
    $sfiltr = Array();
    
    if ( isset($_SESSION['FILT_PR']) and $_SESSION['FILT_PR']!= "" ) {
      $sfiltr['PR']=$_SESSION['FILT_PR'];
    }
    if ( isset($_SESSION['FILT_PU']) and $_SESSION['FILT_PU']!= "" ) {
      $sfiltr['PU']=$_SESSION['FILT_PU'];
    }
    if ( isset($_SESSION['FILT_PD']) and $_SESSION['FILT_PD']!= "" ) {
      $sfiltr['PD']=$_SESSION['FILT_PD'];
    }
    return($sfiltr);

  }

  //===============================================
  // auth stuff
  //
  // according to Patrick COILLAND <pcoilland@parkerwilliborg.com>
  // the proof folder MUST NOT be accessible worldwilde
  // so only admin users are authorized
  //===============================================

  private function AskAuth() {
      switch($this->gconf->sso) {
      case "none":
          $this->AskAuthStd();
          break;
      case "openidc":
          $this->AskAuthSso();
          break;
      }
  }

  private function AskAuthSso(){

      $udata = $this->ssoc->VerifSsoUser("adm");

      // kludge !! , init PHP_AUTH_USER    probably useless
      $_SERVER['PHP_AUTH_USER'] = $udata['user'];

      // useless now, already done in SSO client
      //@session_start();
      //$_SESSION['AUTHENTICATED'] = $udata['user'];
  }
  
  private function AskAuthStd(){

    $realm = "ADM:".$this->gconf->name;
    
    // demande d'identification
    if ( !isset($_SERVER['PHP_AUTH_USER']) ) {
      $this->Authenticate($realm);
    } else {
      $uok = $this->data->VerifAdmUser($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
      if ( ! $uok ) {
	$this->Authenticate($realm);
      }
    }
  }

  private function Authenticate($realm){
      Header("status: 401 Unauthorized"); 
      Header("WWW-Authenticate: Basic realm=\"$realm\" ");
      Header("HTTP/1.0 401 Unauthorized");
      
      // display if authentication canceled
      $tpl = new Savant3();
      $tpl->assign("APPNAM", $this->gconf->name);
      
      $tpl->display("tpl/proof/unauth.html");
      
      exit;
  }
  
  //===============================================
  // end
  //===============================================

}

