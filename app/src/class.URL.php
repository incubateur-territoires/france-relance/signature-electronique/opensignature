<?php

$topdir = dirname(dirname(__DIR__));
include_once $topdir."/app/src/conf.ContentDisplayParameters.php";

class URL {

  //===============================================
  // class
  //===============================================

  protected $gconf;
  protected $dosinfo;
  
  function __construct($conf) { 
    $this->gconf = $conf;
    $this->ContDisParam = null;
  }
  
  //===============================================
  // setters
  //===============================================
  
  function InitByInfo($pinf)  { 
    if ( $pinf ) {
      $this->dosinfo = $pinf;
    }
  }
  
  function InitByDID($did)  { 
    // generate a pseudo dosinfo for url class ( faster than FetchDosInfo ?)
    $this->dosinfo = Array('did' => $did); 
  }
  
  //===============================================
  // urls
  //===============================================
  
  function GetDosMethod($method,$qs="") {
    return($this->GetDosMethodArg1($method,"",$qs));
  }
  
  function GetDosMethodArg1($method,$arg1,$qs="") {
    if ( $this->dosinfo == null ) {
      return("no_dosinfo");
    }
    $theu = $this->gconf->TopAppUrl;
    // type wbx|rewrite defined in Config
    $theu .= $this->gconf->csxDocClass;
    $theu .= "/".$method."/".$this->dosinfo['did'];
    if ( $arg1 != "" ) {
      $theu .= "/".$arg1;
    }
    if ( $qs != "" ) {
      $theu .= "?".$qs;
    }
    return($theu);
  }
  
  function GetDosStruct() {
    if ( $this->dosinfo == null ) {
      return("no_dosinfo");
    }
    $theu = $this->gconf->TopAppUrl;
    $theu .= $this->gconf->DataDir;
    $theu .= "/".$this->dosinfo['rdir'];
    return($theu);
  }
  
  function GetAbsDosStruct() {   // no longer used !
    $tmp = $this->GetDosStruct();
    $theu = "http://".$_SERVER['SERVER_NAME'].$tmp;
    return($theu);
  }
  
  function GetDosData($file) {
    if ( $this->dosinfo == null ) {
      return("no_dosinfo");
    }
    
    $dsplug = $this->GetContentDisplayParameters($file);
    if ( $dsplug == "ExternLink" ) {
        $li = $this->GetLinkInfo($file);
        // awful klugde !
        // mark the url with XLINK to process by the calling function (GetInnerData ) 
        $theu = "XLINK|".$li['URL'];
    } else {
        $theu = $this->GetPluginDosData($dsplug,$file);
    }
    return($theu);
  }
  
  function GetDosDownload($file) {
    if ( $this->dosinfo == null ) {
      return("no_dosinfo");
    }
    
    $theu = $this->GetDosMethodArg1("Download",$file);
    return($theu);
    
  }

  function GetRawDosData($file) {
    // used only by plugins now ! 
    // since no-frame display !!
    if ( $this->dosinfo == null ) {
       return("no_dosinfo");
    }
    $theu = $this->GetDosStruct();
    $theu .= "/".$file;
    return($theu);
  }
 
  function GetAbsRawDosData($file) {    // no longer used !
    // used only by mobile app ( json export )
    $tmp = $this->GetRawDosData($file);
    $theu = "http://".$_SERVER['SERVER_NAME'].$tmp;
    return($theu);
  }
  
  function GetDosThumb($file) {
    if ( $this->dosinfo == null ) {
      return("no_dosinfo");
    }
    
    $thumf = $this->gconf->AbsDataDir."/".$this->dosinfo['rdir'];
    $thumf .= "/.thumbs/".$file.".png";
    
    if ( file_exists($thumf) ) {
      $theu = $this->gconf->TopAppUrl;
      $theu .= $this->gconf->DataDir;
      $theu .= "/".$this->dosinfo['rdir'];
      $theu .= "/.thumbs/".$file.".png";
    } else {
      $theu = $this->gconf->TopAppUrl;
      $theu .= "/pub/img/default_icon.png";
    }
    return($theu);
  }
  
  function GetPluginDosData($plugin,$file) {
    if ( $this->dosinfo == null ) {
      return("no_dosinfo");
    }
    $qs = "PLUGIN=".$plugin;
    $theu = $this->GetDosMethodArg1("DispPlugin",$file,$qs);
    return($theu);
  }

  function GetPluginMethod($plugin,$pmethod,$filepath) {
     $theu = $this->gconf->TopAppUrl;
     $theu .= "/plugins/".$plugin."/";
     $theu .= $pmethod;
     $theu .= "?FILEPATH=".$filepath;
     return($theu);
  }  

  function GetMinimumURL() {
      if ( is_null(@$_SERVER['HTTP_HOST']) ) {
          $server = $this->gconf->SignPlatformUrl;
          $theu = $server."/".$this->dosinfo['did'];
      } else {
          $server = @$_SERVER['HTTP_HOST'];
          $theu = "https://".$server."/".$this->dosinfo['did'];
      }
      return($theu);
  }
  
  function GetMinimumARURL($xid) {
      $tmp = $this-> GetMinimumURL();
      return($tmp.$xid);
  }
  
  function GetDavURL($mode=1) {

    $tmp1 = $this->gconf->TopAppUrl;
    $tmp1 .= $this->gconf->DataDir;
    $tmp1 .= "/".$this->dosinfo['rdir'];

    $tmp2 = $_SERVER['SERVER_NAME'].$tmp1;

    switch($mode) {
    case 1:
      $theu = "http://".$tmp2;
      break;
     case 2:
      $theu = "https://".$tmp2;
      break;
     case 3:
      $theu = "dav://".$tmp2;
      break;
     case 4:
      $theu = "davs://".$tmp2;
      break;
    }

    return($theu);
  }
  
  function GetVndSunStar($file) {

    $tmp1 = $this->gconf->TopAppUrl;
    $tmp1 .= $this->gconf->DataDir;
    $tmp1 .= "/".$this->dosinfo['rdir'];

    $tmp2 = $_SERVER['SERVER_NAME'].$tmp1;

    $theu = "vnd.sun.star.webdav://".$tmp2."/".$file;

    return($theu);
  }

  function GetLinkInfo($file) {
    if ( $this->dosinfo == null ) {
      return("no_dosinfo");
    }
    
    $xlfil = $this->gconf->AbsDataDir."/".$this->dosinfo['rdir'];
    $xlfil .= "/".$file;
    
    if ( file_exists($xlfil) ) {
        $linfo = parse_ini_file($xlfil);
        return($linfo);
    } else {
        return(NULL);
    }
  }
  
  //===============================================
  // inner content URL
  //===============================================
  
  function AddChangeContent($url) {
    $ur2 = "javascript:changeContent('".addslashes($url)."')";
    return($ur2);
  }
  
  function GetInnerMethod($method,$qs="") {
    $ur1 = $this->GetDosMethod($method,$qs);
    return $this->AddChangeContent($ur1);
  }
  
  function GetInnerMethodArg1($method,$arg1,$qs="") {
    $ur1 = $this->GetDosMethodArg1($method,$arg1,$qs);
    return $this->AddChangeContent($ur1);
  }
  
  function GetInnerData($file) {
    $ur1 = $this->GetDosData($file);
    if ( strncmp($ur1, "XLINK|", 6) == 0 ) {
        return substr($ur1, 6);
    } else  {
        return $this->AddChangeContent($ur1);
    }
    
  }
   
   
  //===============================================
  // management urls
  //===============================================
  
  function GetMgmtMethod($method,$qs="") {
    $theu = $this->gconf->TopAppUrl;
    // type wbx|rewrite  defined in Config
    $theu .= $this->gconf->csxMgmtClass;
    $theu .= "/".$method;
    if ( $qs != "" ) {
      $theu .= "?".$qs;
    }
    return($theu);
  }

  function GetMgmtInnerMethod($method,$qs="") {
    $ur1 = $this->GetMgmtMethod($method,$qs);
    return $this->AddChangeContent($ur1);
  }
  
  function GetMgmtMethodWithDID($method,$did) {
    return( $this->GetMgmtMethod1Arg($method,$did) );
  }
  
  function GetMgmtMethod1Arg($method,$arg) {
    $p1 = $this->GetMgmtMethod($method);
    return($p1."/".$arg);
  }

  function GetAbsMgmtMethod1Arg($method,$arg) {
    $tmp = $this->GetMgmtMethod1Arg($method,$arg);
    $mgproto = $this->MgmtProto($method);
    $theu = $mgproto."://".$_SERVER['SERVER_NAME'].$tmp;
    return($theu);
  }
  
  function GetAbsMgmtMethod($method) {
    $tmp = $this->GetMgmtMethod($method);
    $mgproto = $this->MgmtProto($method);
    $theu = $mgproto."://".$_SERVER['SERVER_NAME'].$tmp;
    return($theu);
  }
  
  function MgmtProto($method) {
	// awfull kludge ! 
	// get http/s proto by the method
	if ( $method == "Index" ) {
		return("http");
	} else {
		return("https");
	}
  }
  
  function GetSwitchedMgmtMethodWithDID($mode,$method,$did) {
      $tmp1 = $this->GetMgmtMethod1Arg($method,$did);
      $mgproto = $this->MgmtProto($method);
      switch($mode) {
      case "std":
          $svnam = $this->gconf->SwitchedUrlStd;
          break;
      case "sign":
          $svnam = $this->gconf->SwitchedUrlSign;
          break;
      case "visa":
          $svnam = $this->gconf->SwitchedUrlVisa;
          break;
      }
      $theu = $mgproto."://".$svnam.$tmp1;
      return($theu);
  }
  
  function GetSwitchedTopUrl($mode) {
      $mgproto = $this->MgmtProto("Index");
      switch($mode) {
      case "std":
          $svnam = $this->gconf->SwitchedUrlStd;
          break;
      case "sign":
          $svnam = $this->gconf->SwitchedUrlSign;
          break;
      case "visa":
          $svnam = $this->gconf->SwitchedUrlVisa;
          break;
      }
      $theu = $mgproto."://".$svnam;
      return($theu);
  }
  

  //===============================================
  // media urls
  //===============================================

  function GetMedia($path) {
    $theu = $this->gconf->TopAppUrl;
    $theu .= $path;
    return($theu);
  }
  
  function GetDefLogo($size) {
    $theu = $this->gconf->TopAppUrl;
    switch($size) {
    case "small": 
      $theu .= $this->gconf->DefLogoSmall;
      break;
    case "large": 
      $theu .= $this->gconf->DefLogoLarge;
      break;
    }
    return($theu);
  }

  function GetSignLogo($size) {
    $theu = $this->gconf->TopAppUrl;
    switch($size) {
    case "small": 
      $theu .= $this->gconf->SignLogoSmall;
      break;
    case "large": 
      $theu .= $this->gconf->SignLogoLarge;
      break;
    }
    return($theu);
  }

  function GetVisaLogo($size) {
    $theu = $this->gconf->TopAppUrl;
    switch($size) {
    case "small": 
      $theu .= $this->gconf->VisaLogoSmall;
      break;
    case "large": 
      $theu .= $this->gconf->VisaLogoLarge;
      break;
    }
    return($theu);
  }

  function GetAbsDefLogo($size) {         // no longer used !
    // used only by mobile app ( json export )
    $tmp = $this->GetDefLogo($size);
    $theu = "http://".$_SERVER['SERVER_NAME'].$tmp;
    return($theu);
  }
  

  //===============================================
  // Static methods
  //===============================================
  
  static function getURLSimple($gconf) {
    
    $uobj = new URL($gconf);
    return($uobj);
  }
  
  static function getURLByInfo($gconf,$pdinf) {
    $uobj = new URL($gconf);
    $uobj->InitByInfo($pdinf);
    return($uobj);
  }
  
  // should replace getURLByInfo progressively 
  static function getURLByFolder($gconf,$fobj) {
    $uobj = new URL($gconf);
    $uobj->InitByInfo($fobj->_all_);
    return($uobj);
  }
  
  static function getURLByDID($gconf,$did) {
    
    $uobj = new URL($gconf);
    $uobj->InitByDID($did);
    return($uobj);
  }

  //===============================================
  // Content Display Parameters
  // needed for plugins display
  //===============================================

  function InitContentDisplayParameters() {
    if ( is_null($this->ContDisParam) ) {
        $this->ContDisParam = Content_Display_Parameters();
    }
  }
  
  //===============================================
  // external URLs
  //===============================================
  
  function GetMeetJitsi() {
    // meetjitsi no longer work inside frame/iframe !!
    $theu = $this->gconf->MeetJitSiUrl."/".$this->gconf->name.$this->dosinfo['did'];
    return($theu);
  }

  function GetFramaDate() {
    // force lang=fr to framadate      
    if ( isset($this->dosinfo['framadate']) ) {
      $theu = $this->gconf->FramaDateUrl."/adminstuds.php?poll=".$this->dosinfo['framadate']."&lang=fr";
    } else {
      $theu = "about:blank";
    }
     return($theu);
  }

  function GetInnerDate() {
    $ur1 = $this->GetFramaDate();
    $ur2 = $this->GetDosMethod("ExternUrl","XU=".base64_encode($ur1));
    return $this->AddChangeContent($ur2);
  }

  //===============================================
  // mailto URL
  //===============================================

  function GetMailto($subj, $body) {
    $theu = "mailto:?to=&subject=";
    $tmp1 = str_replace("\n","%0A",str_replace(" ","%20",$subj));
    $theu .= $tmp1;
    $theu .= "&body=";
    $tmp2 = str_replace("\n","%0A",str_replace(" ","%20",$body));
    $theu .= $tmp2;

    return($theu);
  }
  
  //===============================================
  // WOPI URL
  //===============================================

  function GetWOPI($fid) {
      $theu = sprintf($this->gconf->wopitpu, $_SERVER['SERVER_NAME'], $fid);
      return($theu);
  }
  
  //===============================================
  // internals
  //===============================================
  
  function GetBrowser() {
    
    $ua = $_SERVER['HTTP_USER_AGENT'];

    $retval = "";
    
    if ( stristr($ua, "edg") ) {
        // should we differentiate old edge (edge) from new 2020 edge (edg ) ?
        $retval = "edge";
        return($retval);
    }
    if ( stristr($ua, "chrome") ) {
      $retval = "chrome";
    }
    if ( stristr($ua, "firefox") ) {
      $retval = "firefox";
    }
    if ( stristr($ua, "msie") || stristr($ua, "trident") ) {
        $retval = "msie";
    }
    if ( stristr($ua, "safari") and $retval != "chrome" ) {
      $retval = "safari";
    }
    if ( stristr($ua, "opera") || stristr($ua, "presto") ) {
      $retval = "opera";
    }

    if ( stristr($ua, "android") ) {
      $retval = "andro_".$retval;
    }

    return($retval);
    
  }
   
  function GetFileType($file) {
    return strtolower(substr(strrchr($file,'.'),1)); 
  }

  function GetContentDisplayParameters($file) {
    
    if ( ! isset($this->ContDisParam) ) {
      echo "Content Display Parameters not initialized !";
      exit();
    }
    
    $fext = $this->GetFileType($file);
    
    $step1 = $this->GetBrowser()."|".$fext;
    if ( isset($this->ContDisParam[$step1]) ) {
      return $this->ContDisParam[$step1];
    }
    
    $step2 = $fext;
    if ( isset($this->ContDisParam[$step2]) ) {
      return $this->ContDisParam[$step2];
    }
    
    return $this->ContDisParam["default"];
  }

  //===============================================
  // admin urls
  //===============================================
  
  function GetAdmMethod($method,$qs="") {
    $theu = $this->gconf->TopAppUrl;
    // type wbx|rewrite  defined in Config
    $theu .= $this->gconf->csxAdmClass;
    $theu .= "/".$method;
    if ( $qs != "" ) {
      $theu .= "?".$qs;
    }
    return($theu);
  }

  function GetAdmMethodWithDID($method,$did) {
    return( $this->GetAdmMethod1Arg($method,$did) );
  }
  
  function GetAdmMethod1Arg($method,$arg) {
    $p1 = $this->GetAdmMethod($method);
    return($p1."/".$arg);
  }

  function GetAbsAdmMethod1Arg($method,$arg) {
    $tmp = $this->GetAdmMethod1Arg($method,$arg);
    $theu = "https://".$_SERVER['SERVER_NAME'].$tmp;
    return($theu);
  }
  
  function GetAbsAdmMethod($method) {
    $tmp = $this->GetAdmMethod($method);
    $theu = "https://".$_SERVER['SERVER_NAME'].$tmp;
    return($theu);
  }
  
  //===============================================
  // APIurls
  //===============================================
  
  function GetApiBaseUrl() {
      $theu = "http(s)://".$_SERVER['SERVER_NAME']."/api";
      return($theu);
  }
  

  //===============================================
  // method with DOSINFO as arguments
  // used by multifolder methods
  //===============================================
  
  function GetInnerDataWithDos($dosinfo, $file) {
      $this->InitByInfo($dosinfo);
      $this->InitContentDisplayParameters();
      return($this->GetInnerData($file));
  }
  
  function GetDosThumbWithDos($dosinfo, $file) {
      $this->InitByInfo($dosinfo);
      $this->InitContentDisplayParameters();
      return($this->GetDosThumb($file));
  }
  
  //===============================================
  // end
  //===============================================
  
}

