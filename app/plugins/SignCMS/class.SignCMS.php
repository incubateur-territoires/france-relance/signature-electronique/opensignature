<?php

//===============================================
// plugins to display unknown content 
// -> display just a message 
//===============================================

$topdir = dirname(dirname(dirname(__DIR__)));
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.PluginLib.php";
include_once $topdir."/app/src/class.Debug.php";

class SignCMS {

  //===============================================
  // init
  //===============================================

  var $pluginlib;

  function __construct($pluglib) { 
    $this->pluginlib = $pluglib;

    $this->debug = new Debug();
  }

  //===============================================
  // display
  //===============================================

  function Display() {

      //echo "<pre>\n";
      //print_r($this->pluginlib);
      //echo"</pre>\n";
      //exit(0);

      
      $urls = URL::GetURLByInfo($this->pluginlib->globalconf, $this->pluginlib->dosinfo);
      $tpl = new Savant3();
 
      $tpl->assign("URL", $urls );
      $tpl->assign("URL1", "https://www.openssl.org/" );
      $tpl->assign("URL2", $urls->GetDosDownload($this->pluginlib->filename) );

      $tmp = "app/plugins/SignCMS/tpl.SignCMS.html";
      $tpl->display($tmp);

  }

}
