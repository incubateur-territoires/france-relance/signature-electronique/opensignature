<?php

//===============================================
// plugins to display External Applications
//===============================================

$topdir = dirname(dirname(dirname(__DIR__)));
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.PluginLib.php";
include_once $topdir."/app/src/class.Debug.php";
include_once $topdir."/app/src/class.Data.php";

class ExternApp {

  //===============================================
  // init
  //===============================================

  var $pluginlib;
  var $xappinfo;

  function __construct($pluglib) { 
    $this->pluginlib = $pluglib;

    $this->debug = new Debug();
   }

  //===============================================
  // display
  //===============================================

  function Display() {

      //$this->debug->Debug2("ExternApp-Display", $this->pluginlib);
      //exit(0);
      $this->ParseXapp();

      
      $urls = URL::GetURLByInfo($this->pluginlib->globalconf, $this->pluginlib->dosinfo);
      $tpl = new Savant3();
      
      $tpl->assign("APPNAM",  basename($this->pluginlib->filename,".xapp"));

      if ( $this->xappinfo["URL"] == "generated" ) {
          $this->data = new Data($this->pluginlib->globalconf);
          $xappo = $this->data->GetXapp($this->xappinfo["CLASS"]);
          $url = $xappo->GenerateUrl($this->xappinfo,$this->pluginlib->lang);
          $tpl->assign("XURL",  $url);
      } else {
          $tpl->assign("XURL",  $this->xappinfo["URL"]);
      }
      
      if ( @$this->xappinfo["DispInNewTab"] ) {
          $tpl->assign("NEWTAB", 1);
      } else {
          $tpl->assign("NEWTAB", 0);
      }
      if (  @$this->xappinfo["CLASS"] == "MeetJitsi" ) {
          $tpl->display("app/plugins/ExternApp/tpl.ExternMiJiTab.html");
      } else {
          $tpl->display("app/plugins/ExternApp/tpl.ExternApp.html");
      }
      
  }
  
  function ParseXapp() {
      
      $filpath = $this->pluginlib->globalconf->AbsDataDir."/".$this->pluginlib->dosinfo["rdir"]."/".$this->pluginlib->filename;
      $this->xappinfo = parse_ini_file($filpath);

      //$this->debug->Debug2("ExternApp-Xinfo", $this->xappinfo);
      //exit(0);
  }
  
}
