<?php

//===============================================
// plugins to display LOOL iframe
//===============================================

$topdir = dirname(dirname(dirname(__DIR__)));
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.PluginLib.php";
include_once $topdir."/app/src/class.Debug.php";

class LOOL {

  //===============================================
  // init
  //===============================================

  var $pluginlib;

  function __construct($pluglib) { 
    $this->pluginlib = $pluglib;

    $this->debug = new Debug();
   }

  //===============================================
  // display
  //===============================================

  function Display() {

    //$this->debug->Debug2("plugin LOOL : thyself", $this);
    //exit(0);
 
    $urls = URL::GetURLByInfo($this->pluginlib->globalconf, $this->pluginlib->dosinfo);
    $urls->InitContentDisplayParameters();
    $tpl = new Savant3();

    $sts = stat($this->pluginlib->GetFilePath());

    $dlurl = $urls->GetDosDownload($this->pluginlib->filename);
    $wopiurl = $urls->GetWOPI($sts['ino']);
    
    $tpl->assign("DID", $this->pluginlib->dosinfo['did']);
    $tpl->assign("FILENAM", $this->pluginlib->filename);
    $tpl->assign("DLURL", $dlurl);
    $tpl->assign("LOOLURL", $this->pluginlib->globalconf->loolurl);
    $tpl->assign("WOPIURL", urlencode($wopiurl));

    $tpl->display("app/plugins/LOOL/tpl.LOOL.html");

  }

}
