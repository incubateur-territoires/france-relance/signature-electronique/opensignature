<?php

//===============================================
// plugins to display resized image 
// to fit in a page
//===============================================

$topdir = dirname(dirname(dirname(__DIR__)));
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.PluginLib.php";

class Image {

  //===============================================
  // init
  //===============================================

  var $pluginlib;

  function __construct($pluglib) { 
    $this->pluginlib = $pluglib;

   }

  //===============================================
  // display
  //===============================================

  function Display() {

    $urls = URL::GetURLByInfo($this->pluginlib->globalconf, $this->pluginlib->dosinfo);
    $urls->InitContentDisplayParameters();
    $tpl = new Savant3();

    $tpl->assign("FILENAM", $this->pluginlib->filename);
    $tpl->assign("URL", $urls);

    $picsiz = getimagesize($this->pluginlib->GetFilePath());
    $tpl->assign("IMGW", $picsiz[0]);
    $tpl->assign("IMGH", $picsiz[1]);

    $prevf = $this->pluginlib->GetNearFile("prev");
    if ( $prevf != "" ) {
      $tpl->assign("PREVURL", $urls->GetInnerData($prevf) );
    }
    $nextf = $this->pluginlib->GetNearFile("next");
    if ( $nextf != "" ) {
      $tpl->assign("NEXTURL", $urls->GetInnerData($nextf) );
    }
    $tpl->assign("IMGURL", $urls->GetRawDosData($this->pluginlib->filename));
    $tpl->assign("DLURL", $urls->GetDosDownload($this->pluginlib->filename));
    $tpl->display("app/plugins/Image/tpl.Image.html");

  }

}
