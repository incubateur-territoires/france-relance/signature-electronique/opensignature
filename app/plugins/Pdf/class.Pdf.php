<?php

//===============================================
// plugins to display pdf document 
//===============================================

$topdir = dirname(dirname(dirname(__DIR__)));
include_once $topdir."/lib/Savant3/Savant3.php";
include_once $topdir."/app/src/class.URL.php";
include_once $topdir."/app/src/class.PluginLib.php";
include_once $topdir."/app/src/class.Debug.php";

class Pdf {

  //===============================================
  // init
  //===============================================

  var $pluginlib;

  function __construct($pluglib) { 
    $this->pluginlib = $pluglib;

    $this->debug = new Debug();
   }

  //===============================================
  // display
  //===============================================

  function Display() {

    $urls = URL::GetURLByInfo($this->pluginlib->globalconf, $this->pluginlib->dosinfo);
    $tpl = new Savant3();

    $tpl->assign("PDFURL", $urls->GetRawDosData($this->pluginlib->filename));
    $tpl->display("app/plugins/Pdf/tpl.Pdf.html");

  }

}
