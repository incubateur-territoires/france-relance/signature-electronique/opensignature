<?php

function GetStatusInfo($elem) {

    switch(@$elem['signstatus']) {
    case 'asked':
    case 'delayed':
        $rv = Array( "sts1"=>"zign2",
                     "sts2"=>"signature st-mod2",
                     "sts3"=>"<span class='fr-badge os-badge fr-badge--warning'>en cours</span>" );
        break;
    case 'refused':
        $rv = Array( "sts1"=>"zign3",
                     "sts2"=>"signature st-mod3",
                     "sts3"=>"<span class='fr-badge os-badge fr-badge--error'>refusée</span>" );
        break;
    case 'done':
        if ( isset($elem['signpartial']) ) {
            $rv = Array( "sts1"=>"zign5",
                         "sts2"=>"signature st-mod6",
                         "sts3"=>"<span class='fr-badge os-badge fr-badge--info'>partielle</span>" );
        } else {
            $rv = Array( "sts1"=>"zign4",
                         "sts2"=>"signature st-mod5",
                         "sts3"=>"<span class='fr-badge os-badge fr-badge--success'>effectuée</span>" );
        }
        break;
    case 'draft':
        $rv = Array( "sts1"=>"zign1",
                     "sts2"=>"signature st-mod1",
                     "sts3"=>"<span class='fr-badge os-badge fr-badge--new'>en préparation</span>" );
        break;
    default:
        // should not happen,   force to draft !
        $rv = Array( "sts1"=>"zign1",
                     "sts2"=>"signature st-mod1",
                     "sts3"=>"<span class='fr-badge os-badge fr-badge--new'>en préparation</span>" );
    }
    
    return($rv);
}
