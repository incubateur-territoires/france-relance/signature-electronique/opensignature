<?php
include_once "config/Config.php";

//*************************************
//   Arguments
//*************************************

// escape all REQUEST vars
$GPCVARS = Array();
foreach($_REQUEST as $key => $val) {
    if ( is_array($val) ) {
        $tmp = Array();
        foreach($val as $k2 => $v2 ) {
            $tmp[$k2] = htmlspecialchars($v2);
        }
        $GPCVARS[$key] = $tmp;
    } else {
        $GPCVARS[$key] = htmlspecialchars($val);
    }
}

if ( isset($_FILES) ){
  $GPCVARS = array_merge($GPCVARS, $_FILES);
}

if ( isset($GPCVARS['PHP_AUTH_USER']) || isset($GPCVARS['PHP_AUTH_PW']) ) {
  // anti cracker protection
  unset($PHP_AUTH_USER);
  unset($PHP_AUTH_PW);
}

//
// Webbox : Get Action PATH_INFO escaped [/Class/Method/Arg0/Arg1/Arg2/Arg3]
//

if (@isset($_SERVER['PATH_INFO'])) {
  $liact0 = explode('/',$_SERVER['PATH_INFO']);
  $liact1 = Array();
  foreach($liact0 as $ind => $liel) $liact1[$ind] =  htmlspecialchars($liel); 
  @list($dumb,$class,$method,$arg[0],$arg[1],$arg[2],$arg[3]) = $liact1;
} else {
  print("url corrupted\n");
  exit (1);
}

//********************************************
// Webbox : arguments mapping for class/method
//********************************************

$argsmapper = array(
    "Doc" => array("DID"),
    "Doc/GetData" => array("DID", "FILENAM"),
    "Doc/DispSplittedContent" => array("DID", "FILENAM"),
    "Doc/DispSplittedHeader" => array("DID", "FILENAM"),
    "Doc/DispPlugin" => array("DID", "FILENAM"),
    "Doc/Download" => array("DID", "FILENAM"),
    "Doc/AddXapp1" => array("XCLASS", "DID"),
    "Doc/AddLOnew1" => array("TYPE", "DID"),
    "Doc/Sign1Step2" => array("DID", "SIGNPARAM"),
    "Doc/SignSMS" => array("DID","TEL","MEL", "NAM"),
    "Doc/SignCreateSMS" => array("DID","TEL","MEL", "NAM"),
    "Doc/SignAction" => array("DID","ACTION"),
    "Mgmt/View" => array("DID"),
    "Mgmt/CreAcc3" => array("UCODE"),
    "Mgmt/Attach" => array("DID", "MODE"),
    "Mgmt/UnCreate" => array("DID"),
    "Mgmt/UnCreateMgt" => array("DID"),
    "Mgmt/GetPubSign" => array("SIGNENGINE"),
    "Mgmt/MultiSignSMS" => array("TEL","NAM"),
    "Admin/View" => array("DID"),
    "Admin/D4U" => array("UID"),
    "Admin/U4D" => array("DID"),
    "Admin/CloseRes" => array("TAG1", "TAG2"),
    "API/Create" => array("TITLE"),
    "API/CreateWithExternID" => array("APPNUM","XAID","TITLE"),
    "API/Info" => array("DID"),
    "API/AddFile" => array("DID"),
    "API/DelFile" => array("DID","FILE"),
    "API/Del" => array("DID"),
    "API/FetchAll" => array("DID"),
    "API/FetchFile" => array("DID","FILE"),
    "API/SetMode" => array("DID","MODE"),
    "API/Ask4Visa" => array("DID","VIZOR","ASKER"),
    "API/Ask4Sign" => array("DID","SIGNOR","ASKER"),
    "Proof/Show" => array("REFPROOF"),
    "Proof/DownProof" => array("REFPROOF"),
    "Proof/DownTSA" => array("REFPROOF"),
    "Proof/DownCA" => array("REFPROOF"),
    "Proof/ShowProof" => array("REFPROOF"),
    "Proof/ShowTSA" => array("REFPROOF"),
);

if ( isset($argsmapper[$class."/".$method]) ) {
    $argtab = $argsmapper[$class."/".$method];
} else if ( isset($argsmapper[$class]) ) {
    $argtab = $argsmapper[$class];
} else {
    $argtab = array();
}

foreach ($argtab as $ind => $argi) {
   $GPCVARS[$argi] = $arg[$ind]; 
}

// extended DID
$p1 = substr(@$GPCVARS['DID'],0,32);
$p2 = substr(@$GPCVARS['DID'],32);
if ( $p2 != "" ) {
    $GPCVARS['DID'] = $p1;
    $GPCVARS['XID'] = $p2;
}

//***************************************
// Webbox : global config,  
//***************************************

$conf = new Config;

umask(0002);

//***************************************
// external app ID conversion
//***************************************
if ( preg_match("|@[0-9]*@.*|", @$GPCVARS['DID']) ) {
    include_once "app/src/class.Data.php";
    $tmpdata = new Data($conf);
    $GPCVARS['DID'] = $tmpdata->GetRealDID($GPCVARS['DID']);
}

//***************************************
// Webbox : require class and call Action
//***************************************

function ExceptionHandler($exception) {
    echo "<pre>\n";
    echo "Exception: ".$exception->getMessage()."\n";
    echo "     file: ".$exception->getFile()."\n";
    echo "     line: ".$exception->getLine()."\n";
    echo "</pre>\n";
}

set_exception_handler('ExceptionHandler');

if ($class && $method) {
    $classfile = "./app/src/class.".$class.".php";
    if ( file_exists($classfile) ) {
        include_once $classfile;

        $obj = new $class($conf);
        $obj->$method($GPCVARS);
            
        unset($classfile);
        unset($obj);
    } else {
        print ("class $classfile does not exists\n");
    }
} else {
    echo "missing class or method\n";
}


   
